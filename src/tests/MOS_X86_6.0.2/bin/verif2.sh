#!/bin/bash

# declaration des codes couleur
noir='\e[0;30m'
gris='\e[1;30m'
rougefonce='\e[0;31m'
rose='\e[1;31m'
vertfonce='\e[0;32m'
vertclair='\e[1;32m'
orange='\e[0;33m'
jaune='\e[1;33m'
bleufonce='\e[0;34m'
bleuclair='\e[1;34m'
violetfonce='\e[0;35m'
violetclair='\e[1;35m'
cyanfonce='\e[0;36m'
cyanclair='\e[1;36m'
grisclair='\e[0;37m'
blanc='\e[1;37m'
neutre='\e[0;m'
# **********************************

# je regarde l'argument est un repertoire
if [ -d $1 ]
then
	echo "(verif) je rentre debut de recherche des elements directory *********************"
for elt in `ls $1 ` 
do 
	if [ -d $1/$elt ]
	then
echo -e "${orange}--->  $1/$elt       ****${neutre}" >> result.txt
	echo "$1/$elt" 
	./verif2.sh $1/$elt >> result.txt 
	fi
done
# **********************************

for elt in `ls $1/*.xml`
do
        #on contrôsi elt est un fichier ordinaire
        if [ -f $elt ]
        then
 	       #Si c'est un fichier on l'affiche
		echo -e "(verif) fichier : $elt${neutre}" 

 		resultDict=$(xmllint $elt --schema ../properties/dict_cta.xsd --noout 2>&1)
		if [[ $resultDict = *validates ]] ; then
		    echo -e "${orange}(verif) ---> verif syntaxe avec dictionaire : ${vertclair}PASS${neutre}"
		    echo -e "${orange}(verif) ---> verif syntaxe avec dictionaire : ${vertclair}PASS${neutre}" >> result.txt

		else
		    echo -e "${orange}(verif) ---> verif syntaxe avec dictionaire : ${rougefonce}PASS${neutre}"
		    echo -e "${orange}(verif) ---> verif syntaxe avec dictionaire : ${vertclair}PASS${neutre}" >> result.txt
		fi

		echo -e "${orange}(verif) ---> lancement serveur        ****${neutre}" 
		./Serveur_OPCUA -d $elt  2>&1  &
		monPID="$!"
		echo -e "${orange}(verif) ---> pause de 2 secondes       ****${neutre}"
		sleep 2 
		echo -e "${orange}(verif) ---> lancement client       ****${neutre}"
		elt2=${elt%xml*}
		elt2="${elt2}ini"
		./client_test $elt2 
		if [ $? -eq 0 ] 
		then
			echo -e "${orange}(verif) fichier : $elt${neutre} :  ${vertclair}PASS${neutre}" >> result.txt
			echo -e "${orange}(verif) ---> resultat du test : ${vertclair}PASS${neutre}"
		else
			echo -e "${orange}(verif) fichier : $elt${neutre} : ${rougefonce}FAIL${neutre}" >> result.txt
			echo -e "${orange}(verif) ---> resultat du test : ${rougefonce}FAIL${neutre}"
			
		fi
		echo -e "${orange}(verif) ---> kill du server            ****${neutre}" 

		kill -s SIGINT $monPID
		sleep 1
		kill -9 $monPID
        fi
done

fi 
