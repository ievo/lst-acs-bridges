#!/bin/bash
echo "   ************************************************************"
echo "   *** script $0 $*"
echo "   ************************************************************"


m_port=$2
m_description=$4
m_racine="CMS"

echo "    (script : $0) ****************************************"
echo "    (script : $0) ******* verify if the server is started ****"
echo "    (script : $0) ****************************************"
PROCESS_NUM=$(ps aux | grep "MOS_Device" | grep "$m_description" | grep "$m_port" | wc -l )
	if [ $PROCESS_NUM -eq 1 ];
        then
        	echo "    (script : $0) : Error --> This OPPCUA server is already started !!!"
		exit 0
	else
		echo "    (script : $0) ****************************************"
		echo "    (script : $0) ******* start the OPCUA server        ****"
		echo "    (script : $0) ****************************************"

		startMOS.sh $m_description $m_port $m_racine

		echo "    (script : $0) ****************************************"
		echo "    (script : $0) ******* verify if the server is started now      ****"
		echo "    (script : $0) ****************************************"
		PROCESS_NUM=$(ps aux | grep "MOS_Device" | grep "$m_description" | grep "$m_port" | wc -l )
		if [ $PROCESS_NUM -eq 1 ];
        	then
			echo "    (script : $0) ******* The server is now started      ****"
			exit 0
		else
        		echo "    (script : $0) : Error --> Problem to Start this Server !!!"
			exit 1
		fi
        fi




echo "   ************************************************************"
echo ""
echo ""
echo ""


