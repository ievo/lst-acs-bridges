#!/bin/bash
echo "(Script $0) Find MOS_Device with Port=$1"
val=$(ps ax | grep "MOS_Device" | grep "$1" | wc -l)
if [ $val == '0' ]
then
  echo "No instance of MOS_Device with the port number=$1"
else
if [ $val == '1' ]
then
  echo "(Script $0) No instance of MOS_Device with the port number=$1 if you launch this script with MOS_Device program"
  echo "(Script $0) MOS_Device already exist with this port number=$1 if you launch this script standalone"
  val=`expr $val - 1 `
else
  val=`expr $val - 1 `
  echo "MOS_Device already exist with this port number=$1"
fi
fi
exit $val

