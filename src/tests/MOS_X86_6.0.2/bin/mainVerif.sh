#!/bin/bash
# ********************************************

noir='\e[0;30m'
gris='\e[1;30m'
rougefonce='\e[0;31m'
rose='\e[1;31m'
vertfonce='\e[0;32m'
vertclair='\e[1;32m'
orange='\e[0;33m'
jaune='\e[1;33m'
bleufonce='\e[0;34m'
bleuclair='\e[1;34m'
violetfonce='\e[0;35m'
violetclair='\e[1;35m'
cyanfonce='\e[0;36m'
cyanclair='\e[1;36m'
grisclair='\e[0;37m'
blanc='\e[1;37m'

neutre='\e[0;m'

rm -rf result.txt
echo -e "${orange}" >> result.txt
echo "(verif) **********************************" >> result.txt
echo "(verif) ******* lancement test        ****" >> result.txt
echo "(verif) **********************************">> result.txt

echo -e "${orange}"
echo "(verif) **********************************"
echo "(verif) ******* lancement test        ****"
echo "(verif) **********************************"

./verif2.sh $1
echo -e "${orange}(verif) ******* fin du test            ****${neutre}"
echo -e "${orange}(verif) ******* fin du test            ****${neutre}" >> result.txt

