 #!/bin/bash

# declaration des codes couleur
rougefonce='\e[0;31m'
vertclair='\e[1;32m'
orange='\e[0;33m'
neutre='\e[0;m'


if [ -d $1 ]
then
#	echo "je rentre debut de recherche des elements directory *********************"
for elt in `ls $1 2> NULL` 
do 
	if [ -d $1/$elt ]
	then
#	echo "$1/$elt" 
	./testServerOPCUA.sh $1/$elt
	fi
done

for elt in `ls $1/*.xml 2> NULL`
do
        #on contrôsi elt est un fichier ordinaire
        if [ -f $elt ]
        	then
        	#Si c'est un fichier on l'affiche


 		if [[ $elt = *.xml ]] ; then
			echo -e ""
			echo -e "${neutre}-------------------------------------------"
			echo -e "${neutre}-->lancement Serveur OPCUA $elt"
			./MOS_Server -i -d $elt > NULL &
			#./MOS_Server -i -d $elt &
			PID=$!

			echo $elt | sed -e "s/.xml/.ini/g" > temp.tmp
                        elt1=$(< temp.tmp)
                        rm temp.tmp
			
			echo -e "${neutre}-->Pause 3 secondes"
			sleep 3
			echo -e "${neutre}-->Fin de pause 3 secondes"

			
			echo -e "${neutre}-->lancement Client OPCUA $elt1"
			./client_test -d ${elt1}
			var=$?
			if [[ $var = 0 ]] ;  then
        	            echo -e "${orange}(verif) $elt : ${vertclair}PASS${neutre}"
        	            echo -e "(verif) $elt : PASS" >> result.txt
			else
        	            echo -e "${orange}(verif) $elt : ${rougefonce}FAIL${neutre}"
        	            echo -e "(verif) $elt : FAIL" >> result.txt
			fi
			echo -e "${neutre}-->fin  Client OPCUA $elt1"
			echo -e "${neutre}-->destruction  Serveur OPCUA $elt"
		 	kill -9 $PID	
			echo -e "${neutre}-->Pause 3 secondes"
			sleep 3
			echo -e "${neutre}-->Fin de pause 3 secondes"
			
 			if [[ $var = *validates ]] ; then
        	            echo -e "${orange}(verif) $elt : ${vertclair}PASS${neutre}"
        	            echo -e "(verif) $elt : PASS" >> result.txt
			fi
			if [[ $var = *error* ]] ; then
        	            echo -e "${orange}(verif) $elt : ${rougefonce}FAIL${neutre}"
        	            echo -e "(verif) $elt : FAIL" >> result.txt
			    echo "$var"
			fi
			echo -e "${neutre}-------------------------------------------"
			echo -e ""
        	fi
	fi
done


fi 
