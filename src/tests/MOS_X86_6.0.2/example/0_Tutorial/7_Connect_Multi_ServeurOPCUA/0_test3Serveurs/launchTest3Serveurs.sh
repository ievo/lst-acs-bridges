#!/bin/bash

m_port="4841"
m_repository="../example/0_Tutorial/7_Connect_Multi_ServeurOPCUA/0_test3Serveurs/"

shutdown() {
	echo "(script) ******************************"
        kill -2 $PID
	echo "(script) * wait 5 seconds ...."
        sleep 5      
	echo "(script) *   LaunchDNS End "
	echo "(script) *******************************"
       	exit 0
}

trap "shutdown" SIGINT SIGTERM
set -m

echo "(script) ****************************************"
echo "(script) ******* lancement ServeurA          ****"
echo "(script) ****************************************"
m_serveur = $m_repository + "serveurA.xml"
./MOS_Server -d $m_serveur -p 4851 &
PID=$!
echo "kill -9 " $PID >> ps.txt



echo "(script) ****************************************"
echo "(script) ******* lancement ServeurB          ****"
echo "(script) ****************************************"
m_serveur = $m_repository + "serveurB.xml"
./MOS_Server -d $m_serveur -p 4861 &
PID=$!
echo "kill -9 " $PID >> ps.txt


echo "(script) ***************************************************************"
echo "(script) ******* lancement ServeurC                                ****"
echo "(script) ******* c'est le serveur qui gere les 2 autres serveurs   ****"
echo "(script) ***************************************************************"
m_serveur = $m_repository + "serveurC.xml"
./MOS_Server -d $m_serveur &
PID=$!
echo "kill -9 " $PID >> ps.txt



sleep 3        
echo "(script) ****************************************************"
echo "(script) ******* ctrl C  to stop the test               ****"
echo "(script) ****************************************************"
while :                 
do
        read a
done

