#include "pluginsBase.h"
#include "squeletonPlugin_for_synchroneMethodCall.h"

using namespace std;

        
int SqueletonTest::get(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue) {
	int ret=0;
	printf("sonde jl SqueletonTest::get() --> boost::any()\n");
	return ret;
}
        
int SqueletonTest::set(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue){
	int ret=0;
	printf("sonde jl SqueletonTest::set() --> boost::any()\n");
	return ret;
}


// you can overwrite this method if you want but not mandatory because the class pluginsInterfaceImpl already implement it:)
// but becarefull, you have to call before doing  your bussiness, call the father method (the father class) ( PluginsInterfaceImpl::init())
//
// This method is automaticaly call by the program "MOS" after "MOS" server is launched but the "MOS" server is not really ready.
// So don't use this method in ordr to communicate with the "MOS" Server.
// you can use the afertStart() method if needed.
int SqueletonTest::init(const std::string& chaine) {
	int ret=0;

// Mandatory allways need 
	PluginsBase::init(chaine);

	printf("sonde jl SqueletonTest::SqueletonTest::init\n***********************************\n");
//
	return ret;
}

// you can overwrite this method if you want but not mandatory because the class pluginsInterfaceImpl already implement it:)
// but becarefull, you have to call before doing  your bussiness, call the father method (the father class) ( PluginsInterfaceImpl::afterStart())
//
// This method is automaticaly call by the program "MOS" after "MOS" server is launched and ready.
int SqueletonTest::afterStart() {
	int ret=0;

// Mandatory allways need 
	ret = PluginsBase::afterStart();
// 
	if(ret != -1) {
// here an example in order to call method as a client to the server 
// here call the method GetMonitoring() with 1 Input argument
// and print the Output Argument of the method

 		std::vector<std::string> *listElement=getListMonitoringRef();
 		for (std::vector<std::string>::iterator it = listElement->begin(); it != listElement->end();
                        it++) {
			printf("elementMonitoring = %s\n",it->c_str());
 		}
 		listElement=getListControlRef();
 		for (std::vector<std::string>::iterator it = listElement->begin(); it != listElement->end();
                        it++) {
			printf("elementControl = %s\n",it->c_str());
 		}/*
 		for (std::vector<std::string>::iterator it = m_listControl.begin(); it != m_listControl.end();
                        it++) {
			printf("elementControl = %s\n",it->c_str());
 		}*/
	}
	return ret;
}


int SqueletonTest::cmdAsynch(const std::string& command, int commandStringAck, const std::string& datapointName, int nameSpace, std::string& result) {
	// not use in this example
	int ret=0;
	result ="";
	return ret;
}


int SqueletonTest::close() {
	// here we do noting
	int ret=0;
	return ret;
}

int SqueletonTest::cmd(const std::string& l_chaine, int commandStringAck, std::string& result) {
	int ret = 0;
 	std::string chaine=l_chaine;
	printf("plugin : cmd with the instruction :%s\n",chaine.c_str());
        chaine +=  " ";
        std::string subChaine1 = chaine;
        std::string subChaine2 = chaine;
        int flag=1;
        std::string::size_type pos;
        while (flag) {
                subChaine1 = subChaine2;
                pos = subChaine2.find(' '); // find separator =' '
                if (pos == std::string::npos) // nothing to do ? -> exit
                        flag = 0;
                else {
                         subChaine1.erase(pos); // find the pair name:value
                         subChaine2.erase(0, pos + 1); // strore the rest of the string (example the arguments of the instrution)
                              if(subChaine1.compare("startAllDevice") ==0) {
					 userMethodStartAll(subChaine2); // name of your method who manage this action
                              }
                              if(subChaine1.compare("stopAllDevice") ==0) {
					 userMethodStopAll(); // name of your method who manage this action
                              }
                }
        }
	// example here do nothing but wait
	//sleep(3);

	return ret;
}

int SqueletonTest::userMethodStartAll(const std::string& argument) {
        int ret=0;
	std::string resultCall;
	std::vector<boost::any> callRequest;
        
	boost::any temp=atoi(argument.c_str());
	callRequest.push_back(temp);

	getDataAccessClientOPCUARef()->connect("opc.tcp:\\<yourComputer>:48011");
 	//getDataAccessClientOPCUARef()->callMethod("MOS_Server.Slave_device.start",2,callRequest,&resultCall);
 	getDataAccessClientOPCUARef()->callMethod("MOS_Server.Slave_device.start",2,callRequest,resultCall);
	getDataAccessClientOPCUARef()->disconnect();

	getDataAccessClientOPCUARef()->connect("opc.tcp:\\<yourComputer>:48012");
 	getDataAccessClientOPCUARef()->callMethod("MOS_Server.Slave_device.start",2,callRequest,resultCall);
	getDataAccessClientOPCUARef()->disconnect();

	getDataAccessClientOPCUARef()->connect("opc.tcp:\\<yourComputer>:48013");
 	getDataAccessClientOPCUARef()->callMethod("MOS_Server.Slave_device.start",2,callRequest,resultCall);
	getDataAccessClientOPCUARef()->disconnect();
        return ret;
}

int SqueletonTest::userMethodStopAll() {
        int ret=0;
        std::string resultCall;
	std::vector<boost::any> callRequest;

        boost::any temp;
	callRequest.push_back(temp);

        getDataAccessClientOPCUARef()->connect("opc.tcp:\\<yourComputer>:48011");
        getDataAccessClientOPCUARef()->callMethod("MOS_Server.Slave_device.stop",2,callRequest,resultCall);
        getDataAccessClientOPCUARef()->disconnect();

        getDataAccessClientOPCUARef()->connect("opc.tcp:\\<yourComputer>:48012");
        getDataAccessClientOPCUARef()->callMethod("MOS_Server.Slave_device.stop",2,callRequest,resultCall);
        getDataAccessClientOPCUARef()->disconnect();

        getDataAccessClientOPCUARef()->connect("opc.tcp:\\<yourComputer>:48013");
        getDataAccessClientOPCUARef()->callMethod("MOS_Server.Slave_device.stop",2,callRequest,resultCall);
        getDataAccessClientOPCUARef()->disconnect();

        return ret;
}

// becarefull :  allways need : allow to connect this Plugin with MOS 
extern "C" {
SqueletonTest *make_protocole1() {
	return new SqueletonTest();
}
}

