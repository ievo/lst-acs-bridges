#ifndef SQUELETONTEST_H_
#define SQUELETONTEST_H_

class PluginsBase;

class SqueletonTest: public PluginsBase {
public:
        int init(const std::string& chaine);
        int close();
        int cmd(const std::string& chaine, int commandStringAck,std::string& result);

// new virtual methods appears with the version 3.0 of MOS
	int afterStart();
	int cmdAsynch(const std::string& command, int commandStringAck, const std::string&  datapointName, int nameSpace, std::string& result);

// new virtual methods who replace the setAnay getAny methods  with the version 4.0 of MOS
   	int get(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue);
   	int set(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue);
private:

        int userMethodStartAll(const std::string& argument);
        int userMethodStopAll();

};
#endif //  SQUELETONTEST_H_
