#ifndef HVPS_Tracker_H_
#define HVPS_Tracker_H_ 

#include "pluginsBase.h"

class Quasar_Manager;
class Config;
class Quasar_Callback;

#define DEFINE_PROTO IPPROTO_TCP


class HVPS_Tracker: public PluginsBase {
public:
	HVPS_Tracker();
        int init(const std::string& chaine);
        int close();
        int cmd(const std::string& chaine, int commandStringAck,std::string& result);

// new virtual methods appears with the version 3.0 of MOS
	int afterStart();
	int cmdAsynch(const std::string& command, int commandStringAck, const std::string& datapointName, int nameSpace, std::string& result);

// new virtual methods replace olde get/set methods with the version 4.0 of MOS
        int get(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue);
        int set(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue);

private:
	void open(const std::string& chaine);
	int set(const std::string& chaine, int commandStringAck);
	int get(const std::string& chaine, int commandStringAck,std::string& result);
	char recvBuffer[8192];
	int m_nbCycle;
	int m_rampDelay;
	int m_stepDelay;
	float m_voltage;
	Quasar_Manager *m_quasar_manager;
	std::string m_quasar_address;
	Config *m_config;
                Quasar_Callback *my_callBack;
	int startChannel(const std::string &element);
	int stopChannel(const std::string &element);
	int setChannel(const std::string &element,const std::string &voltage,const std::string &ramp);

};
#endif //  HVPS_Tracker_H__
