#include <vector>
#include "MOS_callbackInterface.h"

class HVPS_Tracker;

class Quasar_Callback :  public MOS_CallbackInterface
{
public:
    Quasar_Callback();
    Quasar_Callback(HVPS_Tracker *hvTracker);
    void  infoDebug();
    void  dataChange(std::vector<std::string> listElements, std::vector<std::string> listValues);
    void abort();
    void SendL2(std::vector<std::string> listElements, std::vector<std::string> listValues);

private:
        HVPS_Tracker  *m_drive;
};

