#include "quasar_callback.h"
#include "pluginsBase.h"
#include "HVPS_Tracker.h"
#include <cstring>
#include <cstdlib>
#include <stdio.h>
#include <sstream>

int t;

Quasar_Callback::Quasar_Callback(){
                t = 0;
}

Quasar_Callback::Quasar_Callback(HVPS_Tracker *hvTracker){
                m_drive = hvTracker;
}

void Quasar_Callback::infoDebug() {
        printf("sonde jl debug infoDebug()\n");
}

void Quasar_Callback::abort() {
}

void  Quasar_Callback::dataChange(std::vector<std::string> listElements, std::vector<std::string> listValues) {
        SendL2(listElements,listValues);

        t+=1;
        bool element = (t%2);
        m_drive->getDataAccessClientOPCUARef()->setDatapoint("MOS_Server.HVPS_Tracker.Test.Test_v",2,element);
}


void Quasar_Callback::SendL2(std::vector<std::string> listElements, std::vector<std::string> listValues){
        int count=(int)listElements.size();
	std::string elementS;

        for (int i=0; i<count; i++ ) {
		if((listElements[i].find("V0Set") !=std::string::npos) ||
		   (listElements[i].find("V1Set") !=std::string::npos) ||
		   (listElements[i].find("I1Set") !=std::string::npos) ||
		   (listElements[i].find("I0Set") !=std::string::npos) ||
		   (listElements[i].find("IMon") !=std::string::npos) ||
		   (listElements[i].find("VMon") !=std::string::npos) ||
		   (listElements[i].find("TripExt") !=std::string::npos) ||
		   (listElements[i].find("PDWn") !=std::string::npos) ||
		   (listElements[i].find("RUp") !=std::string::npos) ||
		   (listElements[i].find("SVMax") !=std::string::npos) 
		)
 		{
			std::size_t pos = listElements[i].find("board");      // position of "live" in str
  			std::string str3 = listElements[i].substr (pos);  
		 	elementS = "MOS_Server.HVPS_Tracker.";
		 	elementS += str3;
			pos = listElements[i].find_last_of(".");      // position of "live" in str
  			str3 = listElements[i].substr (pos);  
		 	elementS += str3;
		 	elementS += "_v";
			float valueF=atof(listValues[i].c_str());
			m_drive->getDataAccessClientOPCUARef()->setDatapoint(elementS.c_str(),2,valueF);
		}
		else {
			if((listElements[i].find("Status") !=std::string::npos) ||
			   (listElements[i].find("Trip") !=std::string::npos) ||
			   (listElements[i].find("TripInt") !=std::string::npos) 
			)
			{
	                        std::size_t pos = listElements[i].find("board");      // position of "live" in str
	                        std::string str3 = listElements[i].substr (pos);
	                        elementS = "MOS_Server.HVPS_Tracker.";
	                        elementS += str3;
	                        pos = listElements[i].find_last_of(".");      // position of "live" in str
	                        str3 = listElements[i].substr (pos);
	                        elementS += str3;
	                        elementS += "_v";
	                        int  valueI=atoi(listValues[i].c_str());
	                        m_drive->getDataAccessClientOPCUARef()->setDatapoint(elementS.c_str(),2,valueI);
			}
		}
        }
}

