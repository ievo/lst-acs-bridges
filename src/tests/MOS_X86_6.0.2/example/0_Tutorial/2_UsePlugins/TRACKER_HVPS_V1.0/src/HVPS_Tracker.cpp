#include "Quasar_Manager.hh"
#include "HVPS_Tracker.h"
#include "quasar_callback.h"
#include "Config.h"

#define NB_MAX_MODULE 13
#define NB_MAX_CHANNEL 32
#define FIRST_INDEX_MODULE 0
#define FIRST_INDEX_CHANNEL 0


using namespace std;


HVPS_Tracker::HVPS_Tracker() {
	int status_PLC_Connection;
	m_config = Config::getInstance("./Quasar.xml");
	m_quasar_manager=NULL;
}

int HVPS_Tracker::get(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue) {
        int ret=0;
	std::string result;
        if(!chaine.empty()) {
		get(chaine,commandStringAck,result);
		tabValue.resize(1);
	
		float val;
		val = atof(result.c_str());
		(tabValue)[0] = val;

                if(string *pstr = boost::any_cast<string>(&(tabValue)[0])){
			(tabValue)[0]=(string) result.c_str();
                }
                else if (int *pi = boost::any_cast <int> (&(tabValue)[0]))
                {
			(tabValue)[0] = (int) atoi(result.c_str());
                }
                else if (float *pi = boost::any_cast <float> (&(tabValue)[0]))
                {
			(tabValue)[0] = (float) atof(result.c_str());
                }
                else if (double *pi = boost::any_cast <double> (&(tabValue)[0]))
                {
			(tabValue)[0] = (double) atof(result.c_str());
                }
                else if (short int *pi = boost::any_cast <short int> (&(tabValue)[0]))
                {
			(tabValue)[0] = (short int) atoi(result.c_str());
                }
                else if (long *pi = boost::any_cast <long> (&(tabValue)[0]))
                {
			(tabValue)[0] = (long) atol(result.c_str());
                }
                else if (bool *pi = boost::any_cast <bool> (&(tabValue)[0]))
                {
			(tabValue)[0]= (bool) atoi(result.c_str()) ;
                }
                else
                {
		}
	}
        return ret;
}

int HVPS_Tracker::get(const std::string& chaine, int commandStringAck,std::string& result) {
	int ret=0;
	printf("sonde jl HVPS_Tracker::get\n");
	result="test";
        return ret;
}

int HVPS_Tracker::set(const std::string& chaine, int commandStringAck) {
        int ret = 0;
	printf("sonde jl HVPS_Tracker::set\n");
        std::string result;
        return ret;
}

int HVPS_Tracker::set(const std::string& l_chaine,int commandStringAck, std::vector<boost::any>& tabValue){
        int ret=0;
	 std::string chaine=l_chaine;
        if(string *pstr = boost::any_cast<string>(&tabValue[0])){
                        chaine += " ";
                        chaine += *pstr;
                }
                else if (int *pi = boost::any_cast <int> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (float *pi = boost::any_cast <float> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (double *pi = boost::any_cast <double> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (short int *pi = boost::any_cast <short int> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (long *pi = boost::any_cast <long> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (bool *pi = boost::any_cast <bool> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else
                {
                        printf(" unknown type\n");
                }
	set(chaine,commandStringAck);
        return ret;
}


// you can overwrite this method if you want but not mandatory because the class pluginsInterfaceImpl already implement it:)
// but becarefull, you have to call before doing  your bussiness, call the father method (the father class) ( PluginsBase::init())
//
// This method is automaticaly call by the program "MOS" after "MOS" server is launched but the "MOS" server is not really ready.
// So don't use this method in ordr to communicate with the "MOS" Server.
// you can use the afertStart() method if needed.
int HVPS_Tracker::init(const std::string& l_chaine) {
	printf("sonde jl TCP_Plugin::init() avec chaine=%s\n",l_chaine.c_str());
// Mandatory allways need 
	PluginsBase::init(l_chaine);
//
	std::string chaine=l_chaine;
        int ret=0;
	chaine = chaine + " ";
        std::string::size_type pos;
        std::string valueString;
        std::string nameString;
        std::string subChaine1 = chaine;
        std::string subChaine2 = chaine;
        int flag=1;
        while (flag) {
                subChaine1 = subChaine2;
                pos = subChaine2.find(' '); // cherche separateur ' '
                if (pos == std::string::npos) // il n'y a plus : alors on sort 
                        flag = 0; 
                else {
                         subChaine1.erase(pos); // isole le binome name:value
                         subChaine2.erase(0, pos + 1); // reste

                        valueString = subChaine1;
                        nameString = subChaine1;
                        pos = valueString.find_first_of(':'); // caratere separateur name:value = ':'
                        if (pos != std::string::npos) {
				nameString.erase(pos); // isole dans le biname name
                                valueString.erase(0, pos + 1); // isole dans le binome value
                                // et maintenant on traite
                                if(nameString.compare("Address") ==0) {
					m_quasar_address= valueString.c_str();
                                }
			}
                }
        }
        return ret;
}

// you can overwrite this method if you want but not mandatory because the class pluginsInterfaceImpl already implement it:)
// but becarefull, you have to call before doing  your bussiness, call the father method (the father class) ( PluginsBase::afterStart())
//
// This method is automaticaly call by the program "MOS" after "MOS" server is launched and ready.
int HVPS_Tracker::afterStart() {
	int ret=0;
	std::string resultCall;
	std::string currentAz;
	char RacineElementCh[200];
	char RacineElementBoard[200];
	std::string element;
	int status_PLC_Connection;
	my_callBack=NULL;
// Mandatory allways need 
	ret = PluginsBase::afterStart();

 	m_quasar_manager= new Quasar_Manager(m_quasar_address,&status_PLC_Connection);
	std::vector<ListElement*>* tempListSubscribe = m_config->getListSubscribe_L1();
                
	my_callBack = new Quasar_Callback(this);
	if(m_quasar_manager){
		m_quasar_manager->subscribe(tempListSubscribe,my_callBack);
	}
	return ret;
}

int HVPS_Tracker::cmdAsynch(const std::string& command, int commandStringAck, const std::string& datapointName, int nameSpace, std::string& result) {
	int ret=0;

	return ret;
}


int HVPS_Tracker::close() {
        int ret=0;
        std::string retValue;
        return ret;
}

int HVPS_Tracker::startChannel(const std::string &element) {
	int ret=0;
	std::string elementFinal = element;
	elementFinal +=".POn";
	ret = m_quasar_manager->setIntVariable(elementFinal.c_str(),1);
	return ret;
}

int HVPS_Tracker::stopChannel(const std::string &element) {
        int ret=0;
        std::string elementFinal = element;
        elementFinal +=".POn";
        ret = m_quasar_manager->setIntVariable(elementFinal.c_str(),0);
        return ret;
}

int HVPS_Tracker::setChannel(const std::string &element,const std::string &voltage,const std::string &ramp) {
	int ret=0;
	int error=0;
	std::string elementFinal = element;
                                                
	elementFinal +=".I0Set";
	float voltageF= atof(voltage.c_str());
	ret = m_quasar_manager->setFloatVariable(elementFinal,voltageF);
	if(ret) error=1;

	elementFinal = element;
	elementFinal +=".I1Set";
	voltageF= atof(voltage.c_str());
	ret = m_quasar_manager->setFloatVariable(elementFinal,voltageF);
	if(ret) error=1;
	

        elementFinal = element;
        elementFinal +=".Pw";
        int  pwI= 10;
        ret = m_quasar_manager->setIntVariable(elementFinal,pwI);
        if(ret) error=1;

        elementFinal = element;
        elementFinal +=".RUp";
        float rampF= atof(ramp.c_str());
        ret = m_quasar_manager->setFloatVariable(elementFinal,rampF);
        if(ret) error=1;
	

        elementFinal = element;
        elementFinal +=".PDwn";
        int  pdnI= 10;
        ret = m_quasar_manager->setIntVariable(elementFinal,pdnI);
        if(ret) error=1;

	return error;
}

int HVPS_Tracker::cmd(const std::string& command, int commandStringAck, std::string& result) {
	int ret=0;
	int error=0;
	std::string result2="";
	result ="";
	std::string chaine = command + " ";
        std::string::size_type pos;
        std::string valueString;
        std::string nameString;
        std::string subChaine1 = chaine;
        std::string subChaine2 = chaine;
	int boardNumber=1;
	int channelNumber=0;
	std::string voltage;
	std::string ramp;
        int flag=1;
        while (flag) {
                subChaine1 = subChaine2;
                pos = subChaine2.find(' '); // cherche separateur ' '
                if (pos == std::string::npos) // il n'y a plus : alors on sort 
                        flag = 0; 
                else {
                         subChaine1.erase(pos); // isole le binome name:value
                         subChaine2.erase(0, pos + 1); // reste

                        valueString = subChaine1;
                        nameString = subChaine1;
                        pos = valueString.find_first_of(':'); // caratere separateur name:value = ':'
                        if (pos != std::string::npos) {
                                        nameString.erase(pos); // isole dans le biname name
                                        valueString.erase(0, pos + 1); // isole dans le binome value
                                        // et maintenant on traite
                                if(nameString.compare("board") ==0) {
					boardNumber = atoi(valueString.c_str());
                                }
                                if(nameString.compare("channel") ==0) {
					channelNumber = atoi(valueString.c_str());
                                }
                                if(nameString.compare("voltage") ==0) {
					voltage = valueString.c_str();
                                }
                                if(nameString.compare("ramp") ==0) {
					ramp = valueString.c_str();
                                }
                        } else {
                                //ret=1;
                        }
                }
	}
	char RacineElementCh[200];
	char RacineElementBoard[200];
        std::string element;

                               if((chaine.find("StartChannel")==0) ||
                                  (chaine.find("StopChannel")==0) ||
                                  (chaine.find("SetChannel")==0) 
			 	)
				{
					if(boardNumber==-1) {
                                        	for(int i=(FIRST_INDEX_MODULE); i<(NB_MAX_MODULE - FIRST_INDEX_MODULE); i++) {
                                        		sprintf(RacineElementBoard,"board%02d",i);
							if(channelNumber==-1) {
 					  			for(int j=(FIRST_INDEX_CHANNEL); j<(NB_MAX_CHANNEL - FIRST_INDEX_CHANNEL); j++) {
                                                			sprintf(RacineElementCh,"%s.channel%02d",RacineElementBoard,j);
                               						if(chaine.find("StartChannel")==0) startChannel(RacineElementCh);
                               						if(chaine.find("StopChannel")==0) stopChannel(RacineElementCh);
                               						if(chaine.find("SetChannel")==0) setChannel(RacineElementCh,voltage,ramp);
								}
							} 
							else {
                                                		sprintf(RacineElementCh,"%s.channel%02d",RacineElementBoard,channelNumber);
                               					if(chaine.find("StartChannel")==0) startChannel(RacineElementCh);
                               					if(chaine.find("StopChannel")==0) stopChannel(RacineElementCh);
                               					if(chaine.find("SetChannel")==0) setChannel(RacineElementCh,voltage,ramp);
							}
						} 
					} 
					else {
                                        	sprintf(RacineElementBoard,"board%02d",boardNumber);
						if(channelNumber==-1) {
							for(int j=(FIRST_INDEX_CHANNEL); j<(NB_MAX_CHANNEL - FIRST_INDEX_CHANNEL); j++) {
								sprintf(RacineElementCh,"%s.channel%02d",RacineElementBoard,j);
                               					if(chaine.find("StartChannel")==0) startChannel(RacineElementCh);
                               					if(chaine.find("StopChannel")==0) stopChannel(RacineElementCh);
                               					if(chaine.find("SetChannel")==0) setChannel(RacineElementCh,voltage,ramp);
							}
                                                } 
						else {
                                                                sprintf(RacineElementCh,"%s.channel%02d",RacineElementBoard,channelNumber);
                               					if(chaine.find("StartChannel")==0) startChannel(RacineElementCh);
                               					if(chaine.find("StopChannel")==0) stopChannel(RacineElementCh);
                               					if(chaine.find("SetChannel")==0) setChannel(RacineElementCh,voltage,ramp);
						}
					}
                                }

				return error;

}

// becarefull :  allways need : allow to connect this Plugin with MOS 
extern "C" {
HVPS_Tracker *make_protocole1() {
	return new HVPS_Tracker();
}
}

