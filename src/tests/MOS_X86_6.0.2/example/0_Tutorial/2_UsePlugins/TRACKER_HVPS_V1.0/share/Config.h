#ifndef Config_h
#define Config_h

#include "Config.h"
#include "string"
#include "vector"
#include "map"
#include <libxml/parser.h>
#include <libxml/xpath.h>

typedef std::vector<std::string> list_t;

class ListCommand {	
public:
	std::string name;
	int  nameSpace;
};

class ListElement {
public :
        std::string Name;
        std::string Description;
        std::string NameSpace;
        std::string NodeId;
        std::string Type;
        std::string Subscribe_L1;
        std::string Subscribe_L2;
        std::string Subscribe_TelescopeStatus;
        std::string Subscribe_AzimuthStatus;
        std::string Subscribe_ElevationStatus;
        std::string CB_drive;
        std::string CB_emergencyStop;
        std::string CB_gotoposition;
        std::string CB_gotoskyposition;
        std::string CB_tracking;
        std::string CB_stopmotion;
        std::string CB_park;
        std::string CB_unpark;
};

class Config {

 static Config *singleton;

 public:

    static Config *getInstance(const std::string& fileName);
    static Config *getInstance();
    static void kill();
    Config(const std::string& fileName);

    std::string getOpcUaRef();
    std::string getRootName();

    std::vector<ListElement*>* getListSubscribe_L1();
    std::vector<ListElement*>* getListSubscribe_L2();
    std::vector<ListElement*>* getListSubscribe_TelescopeStatus();
    std::vector<ListElement*>* getListSubscribe_AzimuthStatus();
    std::vector<ListElement*>* getListSubscribe_ElevationStatus();

    std::vector<ListElement*>* getListCB_drive();
    std::vector<ListElement*>* getListCB_emergencyStop();
    std::vector<ListElement*>* getListCB_gotoposition();
    std::vector<ListElement*>* getListCB_gotoskyposition();
    std::vector<ListElement*>* getListCB_tracking();
    std::vector<ListElement*>* getListCB_stopmotion();
    std::vector<ListElement*>* getListCB_park();
    std::vector<ListElement*>* getListCB_unpark();
    std::vector<ListElement*>* getListElements();
    ListElement *searchElement(const std::string& element);
    std::vector<std::string>* getListCommand(const std::string& element);

 private:
	std::string m_fileName;
	std::string m_OpcUaRef;
	std::string m_RootName;
	ListCommand m_ListStatus;

	std::vector<ListElement *>m_ListSubscribe_L1; 
	std::vector<ListElement *>m_ListSubscribe_L2; 
	std::vector<ListElement *>m_ListSubscribe_TelescopeStatus; 
	std::vector<ListElement *>m_ListSubscribe_AzimuthStatus; 
	std::vector<ListElement *>m_ListSubscribe_ElevationStatus; 
        std::vector<ListElement *>m_ListCB_drive;
        std::vector<ListElement *>m_ListCB_emergencyStop;
        std::vector<ListElement *>m_ListCB_gotoposition;
        std::vector<ListElement *>m_ListCB_gotoskyposition;
        std::vector<ListElement *>m_ListCB_tracking;
        std::vector<ListElement *>m_ListCB_stopmotion;
        std::vector<ListElement *>m_ListCB_park;
        std::vector<ListElement *>m_ListCB_unpark;

	std::vector<ListElement *>m_ListElements; 
	std::map<std::string, ListElement *>m_MapElements; 
	std::map<std::string,list_t> m_Commands;
	xmlNodePtr readFile(); 
	int readConfig();
};

#endif // Config_h

