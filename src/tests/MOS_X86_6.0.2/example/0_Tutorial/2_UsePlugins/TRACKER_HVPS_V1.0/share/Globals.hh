#ifndef GLOBALS2_H
#define GLOBALS2_H

#include <string>
#include <stdint.h>

class Globals
{
public:
    
    const static int AZ = 0;
    const static int EL = 1;
    
    const static int RETURN_OK = 1;
    const static int RETURN_WARNING = 0;
    const static int RETURN_ERROR = -1;
    const static int RETURN_CRITICAL = -2;
    
    const static int INIT_OK = 0;
    const static int INIT_DB_ERR = 1;
    const static int INIT_NO_CON = 2;
    
};

class TRACK_MODE
{
public:
    const static int MONITOR = 1; ///< The telescope is in none of the below states
    const static int REGPOS = 2; ///< The telescope is positioning between two targets
    const static int FASTPOS = 3; ///< The telescope is fast positioning between two targets
    const static int FINEPOS = 4; ///< The telescope is fine positioning to target
    const static int ONTRACK = 5; ///< The telescope is doing tracking
    const static int PARKOUT = 6; ///< The telescope is parking out
    const static int PARKIN = 7; ///< The telescope is parking in
};



#endif
