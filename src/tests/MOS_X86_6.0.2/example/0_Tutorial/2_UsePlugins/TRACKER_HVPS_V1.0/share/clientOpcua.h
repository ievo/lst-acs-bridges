#ifndef CLIENT_OPCUA_H
#define CLIENT_OPCUA_H

#include "uabase.h"
#include "uaclientsdk.h"
#include "uaargument.h"

#include "string"


using namespace UaClientSdk;

class Client_opcua : public UaSessionCallback
{
    UA_DISABLE_COPY(Client_opcua);
public:
    Client_opcua();
    virtual ~Client_opcua();

    // UaSessionCallback implementation ----------------------------------------------------
    virtual void connectionStatusChanged(OpcUa_UInt32 clientConnectionId, UaClient::ServerStatus serverStatus);
    // UaSessionCallback implementation ------------------------------------------------------

    // OPC UA service calls
    UaStatus connect(const std::string& serverUrl);
    UaStatus disconnect();
    UaStatus callMethod(std::string method,CallIn callRequest,std::string *resultCall);
    UaStatus getDatapoint(std::string object,UaVariant *resultElement);
    UaStatus setDatapoint(std::string object,UaVariant element);


private:
    UaSession*              m_pSession;
    UaClient::ServerStatus  m_serverStatus;
    void getMethodArguments(const UaNodeId& methodId, UaArguments& inputArguments, UaArguments& outputArguments);

};


#endif // NAMESERVEROPCUACLIENT_H

