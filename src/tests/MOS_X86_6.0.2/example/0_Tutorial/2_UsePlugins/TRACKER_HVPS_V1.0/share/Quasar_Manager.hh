#ifndef Quasar_Manager_H_
#define Quasar_Manager_H_

#include <vector>
#include <map>
#include <cstring>
#include <string>
#include "PLCSysDef.hh"
#include "lappThread.h"
#include <libxml/parser.h>
#include <libxml/xpath.h>
// test
//class PluginInterfaceClientOPCUA;
class DataAccessClientOPCUA;
class MOS_CallbackInterface;
class Config;
class ListElement;

using namespace std;
typedef std::vector<std::string> list_t;

class Quasar_Manager : public LAPPThread {
        static Quasar_Manager *singleton;  // pointeur vers le singleton
public:
 	static Quasar_Manager *getInstance();

	Quasar_Manager(std::string url,int *status);
	~Quasar_Manager();
    int subscribe(std::vector<ListElement*>* listSubscribe,MOS_CallbackInterface* MOS_callback);
    int close();
    void* run(void *params);

	int getIntVariable(std::string node);
	short int getInt16Variable(std::string node);
        bool getBoolVariable(std::string node);
	float getFloatVariable(std::string node);
	string getStringVariable(std::string node);

        int setIntVariable(std::string node,int value);
	int setStringVariable(std::string node,std::string value);
        int setFloatVariable(std::string node,float value);
        int setShortVariable(std::string node,short value);

private:
	vector<string> t_url;
	//vector<PluginInterfaceClientOPCUA*> m_plugins;
	vector<DataAccessClientOPCUA*> m_plugins;



        int  startRun();
        int m_stop;
        int m_pause;
	int m_forceStop;
        Config *m_config;

        vector<float> buffer1;
        vector<float> buffer2;
        vector<int> buffer3;
        int nbPoints;

        bool SimulisTracking;

        xmlNodePtr readFile(std::string filename);
        //td::vector<std::vector<std::string>> m_listGetStatus;
        //std::vector<std::vector<std::string>> m_listGetStatus;
	//map<string, std::vector<std::string>> m_listGetStatus;
	map<string, list_t> m_listGetStatus;

        int m_nameSpace;
        string m_rootName;
	int m_sizeBufferPLC;
	string m_url;
};

#endif //  Quasar_Manager_H_
