#ifndef GOTO_POSITION_THREAD_H_
#define GOTO_POSITION_THREAD_H_

#include <string>
#include "lappThread.h"


using namespace std;

class Config;

class GotoPositionThread : public LAPPThread {

public:
	GotoPositionThread();
	~GotoPositionThread();
    	void* run(void *params);
private:
        int m_stop;
        int m_pause;
        Config *m_config;
        string m_rootName;

};

#endif //  GOTO_POSITION_THREAD_H_
