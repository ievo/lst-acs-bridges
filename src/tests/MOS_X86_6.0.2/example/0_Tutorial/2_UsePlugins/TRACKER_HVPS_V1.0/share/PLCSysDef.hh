/**
 * \class PLC_SysDef
 *
 * PLC com definitions
 *
 * \author A. Fiasson
 *
 */

#ifndef _PLC_SYSDEF_
#define _PLC_SYSDEF_

#include <string>
#include <vector>
#include <map>
#include <sys/types.h>
#include <sys/dir.h>
#include <sys/param.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdlib.h>

#include "Globals.hh"

using namespace std;


class PLCSysDef
{
public:

    static const int pt_MaxProgramTablePartLength = 10000; ///< Maximum size of a tracktable part that can be sent to PLC
    
    class TRACKTABLE
    {
    public:
        static const int pt_MinProgramTableLength = 5;           ///< Minimum size of a tracktable sent to the PLC (s)
        static const int pt_MaxProgramTableLength = 500;         ///< Maximum size of a complete tracktable (s)
        static const int pt_LoadProgramTableLength = 400;        ///< the actual size we use for a complete tracktable (s)
    };

    
    class CMD_PT_DATA_STRUCT
    {
    public:
        time_t Time;
        float Pos[2];
        float getPos(int id){
            if(id<2)return Pos[id];
            else{
                std::cout << "Wrong table index" << std::endl;
                return 0;
            }
        };
        time_t getTime(){return Time;};
        CMD_PT_DATA_STRUCT(float PosX, float PosY, time_t time){
            Pos[0]=PosX;
            Pos[1]=PosY;
            Time = time;
        };
        CMD_PT_DATA_STRUCT(){return;};

    };
    
    class CMD_PT_LOAD_TABLE_STRUCT
    {
    public:
        int CmdId;
        int SequenceLength;
        time_t StartTime;
        vector<CMD_PT_DATA_STRUCT> Data;
    };
    
    class CMD_POINTING
    {
    public:
        int CmdId;
        int SpeedMode;
        time_t StartTime;
        float Angle;
        int Axis;
        CMD_PT_DATA_STRUCT Data;
        
        CMD_POINTING(int axis,float deg,int speedmode){
            Axis = axis;
            Angle = deg;
            SpeedMode = speedmode;
            return;
        };
    };
  
  class CMD_PT_STATUS_STRUCT
    {
        public:
	// rajout jlp  pour veiter le reste
	map <string, string> m_status;
	int Print(bool verbose=false){
		cout << endl;
		cout << "=========================================================" << endl;
		cout << "                         PLC STATUS" << endl;
  		for (map<std::string, string>::iterator it=m_status.begin(); it!=m_status.end(); ++it) {
 			cout << it->first.c_str() << " \t " << 	it->second.c_str()<< endl;
        	}
 		cout << "=========================================================\t " << endl;
		cout << endl;
		return 1;
	};

    };
};
#endif
