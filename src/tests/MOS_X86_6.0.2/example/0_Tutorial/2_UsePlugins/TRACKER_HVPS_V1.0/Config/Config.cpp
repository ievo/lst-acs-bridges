#include "Config.h"
#include <iostream>


Config *Config::singleton = NULL;


Config *Config::getInstance(const std::string& fileName) {
        if (!singleton) {
                singleton = new Config(fileName);
        }
        return singleton;
}

Config *Config::getInstance() {
        return singleton;
}

void Config::kill() {
        if (NULL != singleton) {
                delete singleton;
                singleton = NULL;
        }
}


Config::Config(const std::string& fileName)
{
//	m_ListSubscriptionGotoposition.clear();
	m_fileName = fileName;

 	ListElement* tempElement=new ListElement();
/*	tempElement->description = "";
	tempElement->subscribe ="";
	tempElement->subscribeL2 ="";
        tempElement->subscribeGotoposition="y";
       	tempElement->subscribeGotoSkyposition="";
       	tempElement->subscribeEmergencyStop="";
       	tempElement->subscribeStopMotion="";
       	tempElement->subscribeStartTracking="";
	tempElement->type = "int16";
	tempElement->name = "GoToAxisPosition_Axis";
	tempElement->nameSpace = 7;
	tempElement->nodeId = "DB_Inputs_Telescope.GoToAxisPosition.Axis";
	//m_ListSubscriptionGotoposition.push_back(tempElement);
	//m_ListElements.push_back(tempElement);

 	ListElement* tempElement1=new ListElement();
	tempElement1->description = "";
	tempElement1->subscribe ="";
	tempElement1->subscribeL2 ="";
        tempElement1->subscribeGotoposition="";
       	tempElement1->subscribeGotoSkyposition="";
       	tempElement1->subscribeEmergencyStop="";
       	tempElement1->subscribeStopMotion="";
       	tempElement1->subscribeStartTracking="";
	tempElement1->type = "int16";
	tempElement1->name = "GoToAxisPosition_Speedmode";
	tempElement1->nameSpace = 7;
        tempElement1->nodeId = "DB_Inputs_Telescope.GoToAxisPosition.Speedmode";
	m_ListSubscriptionGotoposition.push_back(tempElement1);
	m_ListElements.push_back(tempElement1);
	 m_MapElements[tempElement1->name]=tempElement1;

	m_ListSubscriptionGotoposition.push_back(tempElement);
	m_ListElements.push_back(tempElement);
	 m_MapElements[tempElement->name]=tempElement;

        m_ListSubscriptionGotoSkyposition.push_back(tempElement);
*/

	readConfig();
}


xmlNodePtr Config::readFile() {
        xmlDocPtr doc=NULL;
        xmlNodePtr ret_racine;
        xmlNodePtr racine;

        doc = xmlParseFile(m_fileName.c_str());
        racine = xmlDocGetRootElement(doc);
        ret_racine = xmlDocGetRootElement(doc);
        if (racine == NULL) {
                   fprintf(stderr, "Document XML vierge\n");
                                         xmlFreeDoc(doc);
        }
        return ret_racine;
}

int Config::readConfig() {
        int ret=0;
         xmlNodePtr n2,n3,n4;
        std::string res3,res2,res;
        std::string tempName;
        std::string tempDescription;
        std::string tempNameSpace;
        std::string tempNodeId;
        std::string tempType;
        std::string tempSubscribe_L1;
        std::string tempSubscribe_L2;
        std::string tempSubscribe_TelescopeStatus;
        std::string tempSubscribe_AzimuthStatus;
        std::string tempSubscribe_ElevationStatus;
        std::string tempCB_drive;
        std::string tempCB_emergencyStop;
        std::string tempCB_gotoposition;
        std::string tempCB_gotoskyposition;
        std::string tempCB_tracking;
        std::string tempCB_stopmotion;
        std::string tempCB_park;
        std::string tempCB_unpark;

        xmlNodePtr element = readFile();

        std::vector<std::string> my_element;
        my_element.resize(2);
	if(element!=NULL) {
                for (n2 = element->children; n2 != NULL; n2 = n2->next) {
                                res2 = (char *) n2->name;
                                if (res2.compare("Elements") == 0) {
                                        for (n3 = n2->children; n3 != NULL; n3 = n3->next) {
                                                res = (char *) n3->name;
                                                if (res.compare("Element") == 0) {
							tempName="";
							tempDescription="";
							tempNameSpace="";
							tempNodeId="";
							tempType="";
							tempSubscribe_L1="";
							tempSubscribe_L2="";
							tempSubscribe_TelescopeStatus="";
							tempSubscribe_AzimuthStatus="";
							tempSubscribe_ElevationStatus="";
							tempCB_drive="";
							tempCB_emergencyStop="";
							tempCB_gotoposition="";
							tempCB_gotoskyposition="";
							tempCB_tracking="";
							tempCB_stopmotion="";
							tempCB_park="";
							tempCB_unpark="";
                                                        for (n4 = n3->children; n4 != NULL; n4 = n4->next) {
                                                                res = (char *) n4->name;
                                                                if (res.compare("Name") == 0) {
                                                                tempName = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("Description") == 0) {
                                                                tempDescription = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("NameSpace") == 0) {
                                                                tempNameSpace = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("NodeId") == 0) {
                                                                tempNodeId = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("Type") == 0) {
                                                                tempType = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("Subscribe_L1") == 0) {
                                                                tempSubscribe_L1 = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("Subscribe_L2") == 0) {
                                                                tempSubscribe_L2 = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("Subscribe_TelescopeStatus") == 0) {
                                                                tempSubscribe_TelescopeStatus = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("Subscribe_AzimuthStatus") == 0) {
                                                                tempSubscribe_AzimuthStatus = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("Subscribe_ElevationStatus") == 0) {
                                                                tempSubscribe_ElevationStatus = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("CB_drive") == 0) {
                                                                tempCB_drive = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("CB_emergencyStop") == 0) {
                                                                tempCB_emergencyStop = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("CB_gotoposition") == 0) {
                                                                tempCB_gotoposition = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("CB_gotoskyposition") == 0) {
                                                                tempCB_gotoskyposition = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("CB_tracking") == 0) {
                                                                tempCB_tracking = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("CB_stopmotion") == 0) {
                                                                tempCB_stopmotion = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("CB_park") == 0) {
                                                                tempCB_park = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                                if (res.compare("CB_unpark") == 0) {
                                                                tempCB_unpark = ((char *) xmlNodeGetContent(n4));
                                                                }
                                                        }
							if(tempNodeId.size()>0) {
								ListElement* tempElement=new ListElement();
								tempElement->Name = tempName;
								tempElement->Description = tempDescription;
								tempElement->NameSpace = tempNameSpace;
								tempElement->NodeId = tempNodeId;
								tempElement->Type = tempType;
								tempElement->Subscribe_L1 = tempSubscribe_L1;
								tempElement->Subscribe_L2 = tempSubscribe_L2;
								tempElement->Subscribe_TelescopeStatus = tempSubscribe_TelescopeStatus;
								tempElement->Subscribe_AzimuthStatus = tempSubscribe_AzimuthStatus;
								tempElement->Subscribe_ElevationStatus = tempSubscribe_ElevationStatus;
								tempElement->CB_drive = tempCB_drive;
								tempElement->CB_emergencyStop = tempCB_emergencyStop;
								tempElement->CB_gotoposition = tempCB_gotoposition;
								tempElement->CB_gotoskyposition = tempCB_gotoskyposition;
								tempElement->CB_tracking = tempCB_tracking;
								tempElement->CB_stopmotion = tempCB_stopmotion;
								tempElement->CB_park = tempCB_park;
								tempElement->CB_unpark = tempCB_unpark;
								m_ListElements.push_back(tempElement);
								m_MapElements[tempName]=tempElement;
								if(tempSubscribe_L1.compare("y")==0) 
									m_ListSubscribe_L1.push_back(tempElement);
								if(tempSubscribe_L2.compare("y")==0) 
									m_ListSubscribe_L2.push_back(tempElement);
								if(tempSubscribe_TelescopeStatus.compare("y")==0) 
									m_ListSubscribe_TelescopeStatus.push_back(tempElement);
								if(tempSubscribe_AzimuthStatus.compare("y")==0) 
									m_ListSubscribe_AzimuthStatus.push_back(tempElement);
								if(tempSubscribe_ElevationStatus.compare("y")==0) 
									m_ListSubscribe_ElevationStatus.push_back(tempElement);
								if(tempCB_drive.compare("y")==0) 
									m_ListCB_drive.push_back(tempElement);
								if(tempCB_emergencyStop.compare("y")==0) 
									m_ListCB_emergencyStop.push_back(tempElement);
								if(tempCB_gotoposition.compare("y")==0) 
								{
									m_ListCB_gotoposition.push_back(tempElement);
								}
								if(tempCB_gotoskyposition.compare("y")==0) 
									m_ListCB_gotoskyposition.push_back(tempElement);
								if(tempCB_tracking.compare("y")==0) 
									m_ListCB_tracking.push_back(tempElement);
								if(tempCB_stopmotion.compare("y")==0) 
									m_ListCB_stopmotion.push_back(tempElement);
								if(tempCB_park.compare("y")==0) 
									m_ListCB_park.push_back(tempElement);
								if(tempCB_unpark.compare("y")==0) 
									m_ListCB_unpark.push_back(tempElement);
							}
                                                }
                                        }
                                }

                               if (res2.compare("GeneralInfo") == 0) {

                                        for (n3 = n2->children; n3 != NULL; n3 = n3->next) {
                                                res = (char *) n3->name;
 						if (res.compare("OPCUA_Address") == 0) {
							m_OpcUaRef = ((char *) xmlNodeGetContent(n3));
						}
 						if (res.compare("RootName") == 0) {
							m_RootName = ((char *) xmlNodeGetContent(n3));
						}
					}
				}
                                if (res2.compare("Commands") == 0) {
					for (n3 = n2->children; n3 != NULL; n3 = n3->next) {
                                                res = (char *) n3->name;
						std::vector<std::string> tempTabCommand;
                                                        for (n4 = n3->children; n4 != NULL; n4 = n4->next) {
                                                                res2 = (char *) n4->name;
                                                                if (res2.compare("Name") == 0) {
									tempTabCommand.push_back((char *) xmlNodeGetContent(n4));
                                                                }
                                                        }
                                                        m_Commands[res]=tempTabCommand;
                                        }
                                }


                }
	} else {
		ret=1;
	}
	return ret;
}

std::string  Config::getOpcUaRef()
{
	return m_OpcUaRef;
}

std::string  Config::getRootName()
{
	return m_RootName;
}


std::vector<ListElement*>* Config::getListSubscribe_L1()
{
	return &m_ListSubscribe_L1;
}
std::vector<ListElement*>* Config::getListSubscribe_L2()
{
	return &m_ListSubscribe_L2;
}
std::vector<ListElement*>* Config::getListSubscribe_TelescopeStatus()
{
	return &m_ListSubscribe_TelescopeStatus;
}
std::vector<ListElement*>* Config::getListSubscribe_AzimuthStatus()
{
	return &m_ListSubscribe_AzimuthStatus;
}
std::vector<ListElement*>* Config::getListSubscribe_ElevationStatus()
{
	return &m_ListSubscribe_ElevationStatus;
}

std::vector<ListElement*>* Config::getListCB_drive()
{
        return &m_ListCB_drive;
}
std::vector<ListElement*>* Config::getListCB_emergencyStop()
{
        return &m_ListCB_emergencyStop;
}
std::vector<ListElement*>* Config::getListCB_gotoposition()
{
        return &m_ListCB_gotoposition;
}
std::vector<ListElement*>* Config::getListCB_gotoskyposition()
{
        return &m_ListCB_gotoskyposition;
}
std::vector<ListElement*>* Config::getListCB_tracking()
{
        return &m_ListCB_tracking;
}
std::vector<ListElement*>* Config::getListCB_stopmotion()
{
        return &m_ListCB_stopmotion;
}
std::vector<ListElement*>* Config::getListCB_park()
{
        return &m_ListCB_park;
}
std::vector<ListElement*>* Config::getListCB_unpark()
{
        return &m_ListCB_unpark;
}


std::vector<ListElement*>*  Config::getListElements()
{
	return &m_ListElements;
}

std::vector<std::string>* Config::getListCommand(const std::string& element)
{
	return &m_Commands[element];
}


ListElement* Config::searchElement(const std::string& element)
{
	return m_MapElements[element];
}

