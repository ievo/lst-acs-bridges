#include "gotoPositionThread.hh"
#include "dynamicLoader.h"
#include "Config.h"


#include <iostream>
#include <fstream>

#define API_LIB_PATH "/MOS_X86_4.0.2/lib/libDataAccessClientOPCUA.so";
#define MAX_PATH 1023
#define SIZE_BUFFER 1000



GotoPositionThread::GotoPositionThread() {
        int ret=0;
        m_config = NULL;
        m_config = Config::getInstance();
        if(m_config) {
                m_rootName = m_config->getRootName() + ".";
        }
        std::string m_pluginFile=API_LIB_PATH;

        char* pPath;
        pPath = getenv ("MOS_PATH");
        if (pPath!=NULL) {
                 m_pluginFile = pPath;
                 m_pluginFile += "/../lib/libDataAccessClientOPCUA.so";
        }

        std::string m_pluginClass="ptr_Plugin";
        DynamicLoader *m_pluginsLoader;
        DataAccessClientOPCUA *my_plugins;
	m_nameSpace=7; // par defaut sur PLC d'ino

        m_pluginsLoader = new DynamicLoader(m_pluginFile, m_pluginClass);
                //my_plugins = m_pluginsLoader->loadAllSenderReceiverPlugins();
                my_plugins = m_pluginsLoader->load();
                if(my_plugins==NULL) {
                        ret = 1;
                }
                 else {
			int cpt=0;
			int flag=0;
			do {
				ret = my_plugins->connect(url);
				flag= ret;
				if(cpt==3) flag=0;
				cpt++;
			}
			while(flag==-1);
			if(ret!=-1) {
				m_plugins.push_back(my_plugins);
				m_pause = true;
        			m_forceStop=0;
        			m_stop = false;
				SimulisTracking = true;
				startRun();
			}
		}
		*status = ret;
}

GotoPositionThread::~GotoPositionThread() {
 	m_pause = false;
        m_stop = true;
        wait();
}


// Get Variable Id 
std::string GotoPositionThread::getVariableId(std::string varname){
	std::string varid = m_rootName;
	varid += varname;
	return varid;
}

int GotoPositionThread::startTracking(PLCSysDef::CMD_PT_LOAD_TABLE_STRUCT loadlist)
{
	int ret=0;
	m_forceStop=0;


        cout << "ici GotoPositionThread::start" <<endl;
	nbPoints= loadlist.Data.size();
	buffer1.resize(nbPoints);
	buffer2.resize(nbPoints);
	buffer3.resize(nbPoints);
	for (int i=0; i<nbPoints; i++ )
        {
                         buffer1[i] = loadlist.Data[i].Pos[1]; // Warning AltAz -> AzEl
                         buffer2[i] = loadlist.Data[i].Pos[0]; // Warning AltAz -> AzEl
                         buffer3[i] = loadlist.Data[i].Time;
//			cout << "GotoPositionThread " << buffer1[i] << " " << buffer2[i] << " " << buffer3[i] << endl; 
        }

	struct tm * tmp = gmtime(&loadlist.StartTime);
#ifdef NOTSIMU
        int start_year = tmp->tm_year;
        int start_month = tmp->tm_mon;
        int start_mday = tmp->tm_mday;
        int start_hour = tmp->tm_hour;
        int start_min = tmp->tm_min;
        int start_sec = tmp->tm_sec;
        int start_msec = 0;
	// Define To for the tracking
        std::string nodeStartTrackingTimeY="PLC Drive.CPU 317T-2 DP.DB_Time_0_Tracking_In.Year";
        std::string nodeStartTrackingTimeM="PLC Drive.CPU 317T-2 DP.DB_Time_0_Tracking_In.Month";
        std::string nodeStartTrackingTimeD="PLC Drive.CPU 317T-2 DP.DB_Time_0_Tracking_In.Day";
        std::string nodeStartTrackingTimeh="PLC Drive.CPU 317T-2 DP.DB_Time_0_Tracking_In.Hour";
        std::string nodeStartTrackingTimem="PLC Drive.CPU 317T-2 DP.DB_Time_0_Tracking_In.Minutes";
        std::string nodeStartTrackingTimes="PLC Drive.CPU 317T-2 DP.DB_Time_0_Tracking_In.Secondes";
        std::string nodeStartTrackingTimems="PLC Drive.CPU 317T-2 DP.DB_Time_0_Tracking_In.MilliSecondes";
	m_plugins[0]->setDatapoint(nodeStartTrackingTimeY.c_str(),m_nameSpace,start_year);
	m_plugins[0]->setDatapoint(nodeStartTrackingTimeM.c_str(),m_nameSpace,start_month);
	m_plugins[0]->setDatapoint(nodeStartTrackingTimeD.c_str(),m_nameSpace,start_mday);
	m_plugins[0]->setDatapoint(nodeStartTrackingTimeh.c_str(),m_nameSpace,start_hour);
	m_plugins[0]->setDatapoint(nodeStartTrackingTimem.c_str(),m_nameSpace,start_min);
	m_plugins[0]->setDatapoint(nodeStartTrackingTimes.c_str(),m_nameSpace,start_sec);
	m_plugins[0]->setDatapoint(nodeStartTrackingTimems.c_str(),m_nameSpace,start_msec);
#endif	 
	std::cout << "[PLCManager] SIMU send T=" << tmp->tm_year<<"-"<<tmp->tm_mon<<"-"<<tmp->tm_mday
            << " " << tmp->tm_hour<<":"<<tmp->tm_min << ":" << tmp->tm_sec << std::endl;

	m_pause = false;
	ret =Globals::RETURN_OK;
   return ret;
}

int GotoPositionThread::gotopos(PLCSysDef::CMD_POINTING pt)
{
        int ret=0;
        std::string node_cmd="PLC Drive.CPU 317T-2 DP.DB_Inputs_Telescope.CMD_GoToPosition_StartCmd";
        std::string node_target="PLC Drive.CPU 317T-2 DP.DB_Inputs_Telescope.CMD_GoToPosition_StartCmd";
        std::string node_axisid="PLC Drive.CPU 317T-2 DP.DB_Inputs_Telescope.CMD_GoToPosition_StartCmd";
        std::string node_speedmode="PLC Drive.CPU 317T-2 DP.DB_Inputs_Telescope.CMD_GoToPosition_StartCmd";
#ifdef NOTSIMU
	m_plugins[0]->setDatapoint(node_target.c_str(),m_nameSpace,pt.angle);
	m_plugins[0]->setDatapoint(node_axisid.c_str(),m_nameSpace,pt.axis);
	m_plugins[0]->setDatapoint(node_speedmode.c_str(),m_nameSpace,pt.speedmode);
	m_plugins[0]->setDatapoint(node_cmd.c_str(),m_nameSpace,1);
#endif
	ret = Globals::RETURN_OK;
        return ret;
}

int GotoPositionThread::gotopos(PLCSysDef::CMD_POINTING Alt,PLCSysDef::CMD_POINTING Az,bool followtarget)
{
        int ret=0;
        std::string node_cmd="PLC Drive.CPU 317T-2 DP.DB_Inputs_Telescope.CMD_GoToPosition_StartCmd";
        std::string node_Az="PLC Drive.CPU 317T-2 DP.DB_Inputs_Telescope.CMD_GoToPosition_???";
        std::string node_Alt="PLC Drive.CPU 317T-2 DP.DB_Inputs_Telescope.CMD_GoToPosition_???";
        std::string node_Speedmode="PLC Drive.CPU 317T-2 DP.DB_Inputs_Telescope.CMD_GoToPosition_???";
        std::string node_FollowTarget="PLC Drive.CPU 317T-2 DP.DB_Inputs_Telescope.CMD_GoToPosition_???";
#ifdef NOTSIMU
	m_plugins[0]->setDatapoint(node_Az.c_str(),m_nameSpace,Alt.angle);
	m_plugins[0]->setDatapoint(node_Alt.c_str(),m_nameSpace,Az.angle);
	m_plugins[0]->setDatapoint(node_Speedmode.c_str(),m_nameSpace,Alt.speedmode);
	m_plugins[0]->setDatapoint(node_FollowTracking.c_str(),m_nameSpace,followtarget);
	m_plugins[0]->setDatapoint(node_cmd.c_str(),m_nameSpace,1);
#endif
	ret = Globals::RETURN_OK;
        return ret;
}

/*int GotoPositionThread::subscribe(std::vector<ListElement*>* listSubscribe,MOS_CallbackInterface* MOS_callback) {
        int ret=Globals::RETURN_OK;
        std::vector<std::string> m_elements;
        std::vector<int> m_namespace;
        int count= listSubscribe->size();
        m_elements.resize(count);
        m_namespace.resize(count);
        for (int i=0; i<count; i++ ) {
                m_elements[i] = m_rootName + (*listSubscribe)[i]->nodeId;
                m_namespace[i] = (*listSubscribe)[i]->nameSpace;
        }
        m_plugins[0]->subscribe(m_elements,m_namespace,MOS_callback);
        return ret;
}*/

void *GotoPositionThread::run(void *params) {
	bool elementBool;
        //UaVariant element;
        std::string nodeBufferFlag="PLC Drive.CPU 317T-2 DP.DB_Tracking_In.Flag_Array_Treated1";
        std::string nodeBuffer1=   "PLC Drive.CPU 317T-2 DP.DB_Tracking_In.Az";
        std::string nodeBuffer2=   "PLC Drive.CPU 317T-2 DP.DB_Tracking_In.El";
        std::string nodeBuffer3=   "PLC Drive.CPU 317T-2 DP.DB_Tracking_In.Temps";
        std::string nodeStartPointingFlag="PLC Drive.CPU 317T-2 DP.DB_Inputs_Telescope.CMD_Start_Tracking";
	int etat=1;
        while (m_stop == false) {
                if (m_pause == false) {
			if(etat==1) {
				etat=2;
			}
			if(etat==2) {
				//printf("zonde jl GotoPositionThread::run etat=2\n");
#ifdef NOTSIMU       	
		                m_plugins[0]->getDatapoint(nodeBufferFlag,m_nameSpace,elementBool);
#endif
		                //element.toBool(valBoolean);
        		        if(elementBool==0) {
        		                etat=3;
                		} else {
                        		usleep(1000000);
	        	        }
		        }
		}
		 else {
		//		printf("zonde jl GotoPositionThread::run ici2 pause=%d\n",m_pause);
                	usleep(1000000);
		}
        }
}


int  GotoPositionThread::startRun() {
        int ret=0;
        start(NULL);
	ret =Globals::RETURN_OK;
        return ret;
}
