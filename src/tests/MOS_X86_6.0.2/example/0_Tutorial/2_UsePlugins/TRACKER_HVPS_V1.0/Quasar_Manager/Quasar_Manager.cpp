#include "Quasar_Manager.hh"
//#include "pluginsloader.h"
#include "dynamicLoader.h"
#include "Config.h"



#include <iostream>
#include <fstream>
//#define API_LIB_PATH "/MOS/lib/libPluginAPIClientOPCUA.so";
#define API_LIB_PATH "/MOS_X86_4.0.2/lib/libDataAccessClientOPCUA.so";
#define MAX_PATH 1023
#define SIZE_BUFFER 300

#define NOTSIMU

Quasar_Manager *Quasar_Manager::singleton = NULL;

Quasar_Manager *Quasar_Manager::getInstance() {
        return singleton;
}

Quasar_Manager::Quasar_Manager(std::string url,int *status) {
	m_url = url;
	singleton= this;
	m_sizeBufferPLC=SIZE_BUFFER;
        int ret=0;
        m_config = NULL;
        m_config = Config::getInstance();
        if(m_config) {
                m_rootName = m_config->getRootName() + ".";
        }
        std::string m_pluginFile=API_LIB_PATH;

        char* pPath;
        pPath = getenv ("MOS_PATH");
        if (pPath!=NULL) {
                 m_pluginFile = pPath;
                 m_pluginFile += "/../lib/libDataAccessClientOPCUA.so";
        }

        std::string m_pluginClass="ptr_Plugin";
        DynamicLoader *m_pluginsLoader;
        //PluginInterfaceClientOPCUA *my_plugins;
        DataAccessClientOPCUA *my_plugins;
	m_nameSpace=2; // par defaut sur PLC d'ino

        m_pluginsLoader = new DynamicLoader(m_pluginFile, m_pluginClass);
                //my_plugins = m_pluginsLoader->loadAllSenderReceiverPlugins();
                my_plugins = m_pluginsLoader->load();
                if(my_plugins==NULL) {
                        ret = 1;
                }
                 else {
			int cpt=0;
			int flag=0;
			do {
				ret = my_plugins->connect(url);
				flag= ret;
				if(cpt==3) flag=0;
				cpt++;
			}
			while(flag==-1);
			if(ret!=-1) {
				m_plugins.push_back(my_plugins);
				m_pause = true;
        			m_forceStop=0;
        			m_stop = false;
				SimulisTracking = true;
				startRun();
			}
		}
		*status = ret;
}

Quasar_Manager::~Quasar_Manager() {
 	m_pause = false;
        m_stop = true;
        wait();
	//nnif(opcc) delete opcc;
}



int Quasar_Manager::close()
{
        int ret=0;
 	for (vector<DataAccessClientOPCUA *>::iterator it=m_plugins.begin(); it!=m_plugins.end(); ++it) {
                if(*it) {
                 ret = (*it)->disconnect();
                }
                else ret=-1;
                printf("\tsonde jl ici pluginClient->Close()\n");
        }
        return ret;

 	m_pause = true;
	ret =Globals::RETURN_OK;
        return ret;
}


int Quasar_Manager::setShortVariable(std::string node,short value){
        node = m_rootName + node;
        int element=0;
#ifdef NOTSIMU
        m_plugins[0]->setDatapoint(node,m_nameSpace,value);
#endif
        return element;
}

int Quasar_Manager::setIntVariable(std::string node,int value){
        node = m_rootName + node;
        int element=0;
#ifdef NOTSIMU
        m_plugins[0]->setDatapoint(node,m_nameSpace,value);
#endif
        return element;
}

int Quasar_Manager::setStringVariable(std::string node,std::string value){
        node = m_rootName + node;
        int element=0;
#ifdef NOTSIMU
        element = m_plugins[0]->setDatapoint(node,m_nameSpace,value);
	//printf("sonde jl element = %d\n",element);
#endif
        return element;
}

int Quasar_Manager::setFloatVariable(std::string node,float value){
        node = m_rootName + node;
        int element=0;
#ifdef NOTSIMU
        element = m_plugins[0]->setDatapoint(node,m_nameSpace,value);
        //printf("sonde jl element = %d\n",element);
#endif
        return element;
}


short int Quasar_Manager::getInt16Variable(std::string node){
	string finalnode = "";
 	short int element;
        ListElement* tempElement=NULL;
	if(m_config) tempElement = m_config->searchElement(node);
#ifdef NOTSIMU 
 	finalnode= m_rootName + tempElement->NodeId;
	m_plugins[0]->getDatapoint(finalnode,m_nameSpace,element);
#endif
	return element;
}

int Quasar_Manager::getIntVariable(std::string node){
	string finalnode = "";
 	int element;
        ListElement* tempElement=NULL;
	if(m_config) tempElement = m_config->searchElement(node);
#ifdef NOTSIMU 
 	finalnode= m_rootName + tempElement->NodeId;
	m_plugins[0]->getDatapoint(finalnode,m_nameSpace,element);
#endif
	return element;
}

bool Quasar_Manager::getBoolVariable(std::string node){
	string finalnode="";
 	bool elementBool;
        ListElement* tempElement=NULL;
	if(m_config) tempElement = m_config->searchElement(node);
#ifdef NOTSIMU 
 	finalnode= m_rootName + tempElement->NodeId;
	m_plugins[0]->getDatapoint(finalnode,m_nameSpace,elementBool);
#endif
	return elementBool;
}

float Quasar_Manager::getFloatVariable(std::string node){
	string finalnode="";
        bool elementFloat;
        ListElement* tempElement=NULL;
	if(m_config) tempElement = m_config->searchElement(node);
#ifdef NOTSIMU 
 	finalnode= m_rootName + tempElement->NodeId;
        m_plugins[0]->getDatapoint(finalnode,m_nameSpace,elementFloat);
#endif
        return elementFloat;
}
string  Quasar_Manager::getStringVariable(std::string node){
        string finalnode = "";
        ListElement* tempElement=NULL;
        string elementString;
	if(m_config) tempElement = m_config->searchElement(node);
#ifdef NOTSIMU 
 	finalnode= m_rootName + tempElement->NodeId;
        m_plugins[0]->getDatapoint(finalnode,m_nameSpace,elementString);
#endif
        return elementString;
}

int Quasar_Manager::subscribe(std::vector<ListElement*>* listSubscribe,MOS_CallbackInterface* MOS_callback) {
        int ret=Globals::RETURN_OK;
        std::vector<std::string> m_elements;
        std::vector<int> m_namespace;
        int count= listSubscribe->size();
        m_elements.resize(count);
        m_namespace.resize(count);

        for (int i=0; i<count; i++ ) {
                m_elements[i] = m_rootName + (*listSubscribe)[i]->NodeId;
                m_namespace[i] = atoi((*listSubscribe)[i]->NameSpace.c_str());
        }
	if(MOS_callback==NULL) printf("sonde jl pb NULL\n");
        m_plugins[0]->subscribe(m_elements,m_namespace,MOS_callback);
        return ret;
}

void *Quasar_Manager::run(void *params) {
        vector<float> tempArray;
        vector<int> tempArrayI;
	bool elementBool;
        tempArray.resize(m_sizeBufferPLC);
        tempArrayI.resize(m_sizeBufferPLC);
        int  first=1;
        int ptrCounter=0;

	int etat=1;
        while (m_stop == false) {
                if (m_pause == false) {
                        usleep(100000);
		}
		 else {
                	usleep(1000000);
		}
        }
}

int  Quasar_Manager::startRun() {
        int ret=0;
        start(NULL);
	ret =Globals::RETURN_OK;
        return ret;
}

xmlNodePtr Quasar_Manager::readFile(std::string filename) {
        xmlDocPtr doc=NULL;
        xmlNodePtr ret_racine;
        xmlNodePtr racine;

        char *pszFind;
        std::string path;
        char pathCh[MAX_PATH];
        std::string filename2=filename;

                doc = xmlParseFile(filename2.c_str());
                strncpy(pathCh, filename2.c_str(), MAX_PATH);
                pszFind = strrchr(pathCh, '/');
                if (pszFind) {
                        *pszFind = 0; // cut off appname
                }

        racine = xmlDocGetRootElement(doc);
        ret_racine = xmlDocGetRootElement(doc);
        if (racine == NULL) {
                   fprintf(stderr, "Document XML vierge\n");
                                         xmlFreeDoc(doc);
        }
        return ret_racine;
}

