#include "PLC_Manager.hh"
#include "Config.h"

#include <iostream> 

using namespace std;

int main(int argc, char* argv[])
{   int ret=0;
    int status;
    map<string, string> my_status;
    Config *m_config = Config::getInstance("./PLC.xml");

    PLC_Manager * plc_manager;
    cout << "************************************************ " << endl;
    cout << "**** test Unitaire  de la librairie PLC_Manager  " << endl;
    cout << "************************************************ " << endl;
    cout << " step 1 : Connection vers le PLC d'ino " << endl;
    plc_manager = new PLC_Manager("opc.tcp://134.158.99.176:4845",&status);
    cout << "\tresultat :  status= " << status << endl;
    if(plc_manager) {

    	cout << "************************************************ " << endl;
    	cout << " step 2 : lancement method init() " << endl;
  	ret = plc_manager->init();
    	cout << "\tresultat :  status= " << status << endl;

    	cout << "************************************************ " << endl;
    	cout << " step 3 : lancement method getStatus " << endl;
	ret = plc_manager->getStatus(&my_status);
    	cout << "\tresultat :  status= " << status << endl;
 	for (std::map<string,string>::iterator it=my_status.begin(); it!=my_status.end(); ++it)
    		std::cout << it->first << " => " << it->second << '\n';
    	//PrintStatus(true);

    	cout << "************************************************ " << endl;
    	cout << " step 4 : lancement method emergencyStop " << endl;
    	ret = plc_manager->emergencyStop();
    	cout << "\tresultat :  status= " << status << endl;

    	cout << "************************************************ " << endl;
    	cout << " step 5 : lancement method stopMotion " << endl;
    	ret = plc_manager->stopMotion();
    	cout << "\tresultat :  status= " << status << endl;

    	cout << "************************************************ " << endl;
    	cout << " step 6 : lancement method get datapoint float " << endl;
	float valFloat=0;
      	valFloat = plc_manager->getFloatVariable("Drive_azimuth_current_position");
    	cout << "\tresultat :  val= " << valFloat << endl;

    	cout << "************************************************ " << endl;
    	cout << " step 7 : lancement method get datapoint int " << endl;
	short  valInt=0;
      	valInt = plc_manager->getInt16Variable("DB_Inputs_Telescope.Park_Out.Typ");
      	//valInt = plc_manager->getIntVariable("Azimuth_Status.Monitoring_Period_s");
    	cout << "\tresultat :  val= " << valInt << endl;
	short t=0;
 	plc_manager->setIntVariable("DB_Inputs_Telescope.Park_Out.Typ",t);
//[0]->setDatapoint("DB_Inputs_Telescope.Park_Out.Typ",7,t);


    	cout << "************************************************ " << endl;
    	cout << " step 8 : lancement method get datapoint string " << endl;
	string valString="";
      	valString = plc_manager->getFloatVariable("DB_Outputs_Telescope_2.Azimuth_Status.TimeStamp_Az_Current");
    	cout << "\tresultat :  val= " << valString << endl;

    	cout << "************************************************ " << endl;
    	cout << " step 9 : lancement method shutdown() " << endl;
    	ret = plc_manager->shutdown();
    	cout << "\tresultat :  status= " << status << endl;
    }

    cout << endl;
    cout << "************************************************ " << endl;
    cout << "**** fin test Unitaire  de la librairie PLC_Manager  " << endl;
    cout << "************************************************ " << endl;
    return ret;
}
