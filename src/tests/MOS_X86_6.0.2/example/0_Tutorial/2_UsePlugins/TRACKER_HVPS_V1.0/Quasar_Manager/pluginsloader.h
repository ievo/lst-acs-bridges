/**
 *******************************************************************************
 *
 * @file    cta_slc_plugins.h
 * @authors panazol@lapp.in2p3.fr
 * @date    25/04/2014
 * @version v1.0.2
 * @brief description :

 *
 *------------------------------------------------------------------------------
 *
 * @copyright Copyright � 2014, LAPP/CNRS
 *
 * @section LICENSE
 *
 *******************************************************************************
 */

#ifndef PLUGINSLOADER_H
#define PLUGINSLOADER_H

#include "./share/pluginInterfaceClientOPCUA.h"

/**
 * class PluginsLoader
 *
 * @authors panazol@lapp.in2p3.fr
 * @date creation :   25/04/2014
 * @date modification with description:
 *
 * @brief description of this function :
 *    This class is a launcher
 *    Allow to load the library file in your porject (plugin)
 */
class PluginsLoader {
	typedef PluginInterfaceClientOPCUA *(*maker_Plugins)();
public:
	// construction / destruction
	PluginsLoader(std::string mylibFile, std::string className);
	~PluginsLoader();
	PluginInterfaceClientOPCUA *loadAllSenderReceiverPlugins();

private:
	void *hndl;
	std::string m_myLibFile;
	std::string m_className;
	PluginInterfaceClientOPCUA *m_classRef;
};

#endif // PLUGINSLOADER
