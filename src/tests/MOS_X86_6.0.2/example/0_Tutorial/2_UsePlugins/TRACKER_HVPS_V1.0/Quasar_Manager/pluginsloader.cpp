/**
 *******************************************************************************
 *
 * @file    cta_slc_plugins.cpp
 * @authors panazol@lapp.in2p3.fr
 * @date
 * @Modif   04/02/2015
 * @version v1.0.2
 *     04/02/15 update the comment 
 * @brief description :
 *
 *
 * @details
 *  <full description...>
 *
 *------------------------------------------------------------------------------
 *
 * @copyright Copyright © 2014, LAPP/CNRS
 *
 *
 *******************************************************************************
 */
#include <stdio.h>
#include <iostream>
#include <dlfcn.h>
#include <string>
#include <vector>
#include "pluginsloader.h"

using namespace std;

PluginsLoader::PluginsLoader(std::string myLibFile, std::string className) {
	hndl = NULL;
	m_myLibFile = myLibFile;
	m_className = className;
}

PluginInterfaceClientOPCUA *PluginsLoader::loadAllSenderReceiverPlugins() {
	maker_Plugins pMaker;
	// Ouverture de la librairie
	std::string msg;

        msg = "open plugin : ";
	msg += m_myLibFile;
	
	hndl = dlopen(m_myLibFile.c_str(), RTLD_LAZY);
	if (hndl == NULL) {
        	msg = "dlopen : ";
		msg += dlerror();
		fprintf(stderr,"msg=%s\n",msg.c_str());
		return NULL;
	}
	// Chargement de la classe
	void *mkr = dlsym(hndl, m_className.c_str());
	if (mkr == NULL) {
        	msg = "dlsym : ";
		msg += dlerror();
		fprintf(stderr,"msg=%s\n",msg.c_str());
		return NULL;
	}

	pMaker = (maker_Plugins) mkr;
	m_classRef = pMaker();

	return m_classRef;
}

PluginsLoader::~PluginsLoader() {
	if (hndl != NULL)
		dlclose(hndl); 
}

