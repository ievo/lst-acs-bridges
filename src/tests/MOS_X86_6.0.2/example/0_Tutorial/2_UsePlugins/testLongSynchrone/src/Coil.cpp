#include "Coil.h"

using namespace std;

//Coil() : Socket("", 1, DEFINE_PROTO) {
Coil::Coil() {
	m_nbCycle = 1;
	m_socket = NULL;
	m_stepDelay = 1;
	m_testThread = NULL;
	m_rampDelay = 1;
	m_voltage = 1;
}

int Coil::get(const std::string& chaine, int commandStringAck,
		std::vector<boost::any>& tabValue) {
	int ret = 0;
	std::string result;
	//printf("sonde jl Coil::get()1 --> boost::any()\n");
	if (!chaine.empty()) {

		get(chaine, commandStringAck, result);
		tabValue.resize(1);

		float val;
		val = atof(result.c_str());
		(tabValue)[0] = val;

		if (string *pstr = boost::any_cast<string>(&(tabValue)[0])) {
			(tabValue)[0] = (string) result.c_str();
		} else if (int *pi = boost::any_cast<int>(&(tabValue)[0])) {
			(tabValue)[0] = (int) atoi(result.c_str());
		} else if (float *pi = boost::any_cast<float>(&(tabValue)[0])) {
			(tabValue)[0] = (float) atof(result.c_str());
		} else if (double *pi = boost::any_cast<double>(&(tabValue)[0])) {
			(tabValue)[0] = (double) atof(result.c_str());
		} else if (short int *pi = boost::any_cast<short int>(&(tabValue)[0])) {
			(tabValue)[0] = (short int) atoi(result.c_str());
		} else if (long *pi = boost::any_cast<long>(&(tabValue)[0])) {
			(tabValue)[0] = (long) atol(result.c_str());
		} else if (bool *pi = boost::any_cast<bool>(&(tabValue)[0])) {
			(tabValue)[0] = (bool) atoi(result.c_str());
		} else {
		}
	}
	return ret;
}

int Coil::get(const std::string& chaine, int commandStringAck,
		std::string& result) {
	int ret = 0;
	return ret;
}

int Coil::set(const std::string& chaine, int commandStringAck) {
	int ret = 0;
	return ret;
}

int Coil::set(const std::string& l_chaine, int commandStringAck,
		std::vector<boost::any>& tabValue) {
	int ret = 0;
	std::string chaine = l_chaine;
	//printf("sonde jl Coil::set() --> boost::any()\n");
	if (string *pstr = boost::any_cast<string>(&tabValue[0])) {
		chaine += " ";
		chaine += *pstr;
	} else if (int *pi = boost::any_cast<int>(&tabValue[0])) {
		chaine += " ";
		chaine += *pi;
	} else if (float *pi = boost::any_cast<float>(&tabValue[0])) {
		chaine += " ";
		chaine += *pi;
	} else if (double *pi = boost::any_cast<double>(&tabValue[0])) {
		chaine += " ";
		chaine += *pi;
	} else if (short int *pi = boost::any_cast<short int>(&tabValue[0])) {
		chaine += " ";
		chaine += *pi;
	} else if (long *pi = boost::any_cast<long>(&tabValue[0])) {
		chaine += " ";
		chaine += *pi;
	} else if (bool *pi = boost::any_cast<bool>(&tabValue[0])) {
		chaine += " ";
		chaine += *pi;
	} else {
		printf(" unknown type\n");
	}

	return ret;
}

// you can overwrite this method if you want but not mandatory because the class pluginsInterfaceImpl already implement it:)
// but becarefull, you have to call before doing  your bussiness, call the father method (the father class) ( PluginsBase::init())
//
// This method is automaticaly call by the program "MOS" after "MOS" server is launched but the "MOS" server is not really ready.
// So don't use this method in ordr to communicate with the "MOS" Server.
// you can use the afertStart() method if needed.
int Coil::init(const std::string& l_chaine) {
	printf("sonde jl TCP_Plugin::init() avec chaine=%s\n", l_chaine.c_str());
// Mandatory allways need 
	PluginsBase::init(l_chaine);
//
	int ret = 0;
	return ret;
}

// you can overwrite this method if you want but not mandatory because the class pluginsInterfaceImpl already implement it:)
// but becarefull, you have to call before doing  your bussiness, call the father method (the father class) ( PluginsBase::afterStart())
//
// This method is automatically call by the program "MOS" after "MOS" server is launched and ready.
int Coil::afterStart() {
	printf("sonde jl fin Coil::afterStart()\n");
	int ret = 0;

	printf("sonde jl debut Coil::afterStart()\n");
// Mandatory allways need 
	ret = PluginsBase::afterStart();
// 
	printf(
			"sonde jl apres PluginsBase::afterStart11111111 Coil::afterStart()ret=%d\n",
			ret);
	return ret;
}

int Coil::cmdAsynch(const std::string& command, int commandStringAck,
		const std::string& datapointName, int nameSpace, std::string& result) {
	int ret;
	return ret;
}
int Coil::cmd(const std::string& command, int commandStringAck,
		std::string& result) {
	// not use in this example
	int ret = 0;
	result = "";
	std::string chaine = command + " ";
	std::string::size_type pos;
	std::string valueString;
	std::string nameString;
	std::string subChaine1 = chaine;
	std::string subChaine2 = chaine;
	int flag = 1;
	while (1) {
		usleep(3000000);
	}
	return ret;
}

int Coil::cmdStartCycling(int nbCycle, int rampDelay, int stepDelay,
		float voltage) {
	int i, j;
	int ret = 0;
	printf("sonde jl  Coil::cmdStartCycling\n");
	for (i = 0; i < nbCycle; i++) {
		for (j = 0; j < rampDelay; j++) {
			usleep(1000000);
		}
		for (j = 0; j < stepDelay; j++) {
			usleep(1000000);
		}
		for (j = 0; j < rampDelay; j++) {
			usleep(1000000);
		}
	}
	return ret;
}

int Coil::cmdStopCycling() {
	usleep(3000000);

}

int Coil::close() {
	int ret = 0;
	std::string retValue;
	return ret;
}

// becarefull :  allways need : allow to connect this Plugin with MOS 
extern "C" {
Coil *make_protocole1() {
	return new Coil();
}
}

