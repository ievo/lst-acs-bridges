#ifndef COMMONTYPES_H

#define COMMONTYPES_H

#ifdef __cplusplus
extern "C" {
#endif

 #include <stdint.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <semaphore.h>

#define CTRL_HEADER 0xAAAAAAAA
#define CTRL_COMMAND_LAUNCH_SENDING_TYPE1 0x00000001

#define CTRL_COMMAND_STOP_SENDING 0x10000001
#define CTRL_COMMAND_RESET_SENDING 0x10000011
#define CTRL_COMMAND_QUIT 0x10000111


typedef struct 
{
  uint32_t header;
  uint32_t command;
}tCtrlCommand;

typedef struct 
{
  unsigned port;
  struct timeval timeOut;
  char addressIP[16];
}tSocketConfiguration;

typedef enum 
  {
    EVT_TYPE_1,  // full transmission
    EVT_TYPE_2   // partial transmission
  } tEventType;

typedef enum
  {
    SLC_TYPE_1,
    SLC_TYPE_2,
    SLC_TYPE_3,
    SLC_TYPE_4,
    SLC_TYPE_5
  } tSlowControlType;


typedef struct 
{
  uint16_t headerFlag;
  uint16_t reserve;
  uint32_t eventNumber;
  uint32_t time1;
  uint32_t time2;
}tEventHeader;

typedef struct 
{
  uint32_t headerFlag;
  uint32_t trameId;
  uint32_t reserve;
  uint32_t eventNumber;
  uint32_t time1;
  uint32_t time2;
  uint32_t bloc[10];
  uint32_t headerFlag2;
}tSlowControlHeader;

typedef struct 
{
  unsigned coucou;
}tEventsAttributes;

typedef enum 
  {
    IDLE,
    INITIALIZATION,
    SENDING,
    STOP,
    EXIT
  } tApplicationState;

typedef enum 
  {
    NO_ERROR,
    ERROR_1
  } tErrorCode;

typedef struct 
{
  unsigned taskId;
  int socket;
  int sendPort;
  tApplicationState * applicationState;
  char daqClientIpAddress[30];
  int daqClientPort;
  int nodeId;
  sem_t * pTimerSem;
}tTaskSendDataParameters;


#ifdef __cplusplus
}
#endif

#endif
