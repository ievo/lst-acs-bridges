/**
 *******************************************************************************
 *
 * @file    plugin_tcp.cpp
 * @authors panazol@lapp.in2p3.fr
 * @date    19/09/2014
 * @version v1.0.2
 * @brief description :
 *     plugin for the communication with tcp socket
 *     Used by the MOS application
 *     Need to implement the abstract virtual class PluginsInterface
 *
 * @details
 *  <full description...>
 *
 *------------------------------------------------------------------------------
 *
 * @copyright Copyright © 2014, LAPP/CNRS
 *
 *
 *******************************************************************************
 */
#include "tcp_plugin.h"

#define DEFINE_PROTO IPPROTO_TCP

using namespace std;

TCP_Plugin::TCP_Plugin() : PluginsInterface(), Socket("", 1, DEFINE_PROTO) {
}


/**
* Plugin interface function
*
* Pure virtual function defined in the PluginInterface class
*
* \param[in]    chaine : argument for the initialisation of the communication
* 			argument = name:value
*			separation beetween argument = ' '
*
* \return       return the status of this function
*
*               : 0 = no error
*
*               : -1 = error
*
* This function parse the string 'chaine' and find all the arguments for the initialisation of the socket
* (address, port, connectio,validity)
* the the initalisation open the socket and the communication
*/
int TCP_Plugin::init(const std::string& l_chaine) {
	std::string chaine=l_chaine;
        int ret=0;
	chaine = chaine + " ";
        std::string::size_type pos;
        std::string valueString;
        std::string nameString;
        std::string subChaine1 = chaine;
        std::string subChaine2 = chaine;
	printf("sonde jl TCP_Plugin::init() avec chaine=%s\n",chaine.c_str());
        int flag=1;
        while (flag) {
                subChaine1 = subChaine2;
                pos = subChaine2.find(' '); // cherche separateur ' '
                if (pos == std::string::npos) // il n'y a plus : alors on sort 
                        flag = 0; 
                else {
                         subChaine1.erase(pos); // isole le binome name:value
                         subChaine2.erase(0, pos + 1); // reste

                        valueString = subChaine1;
                        nameString = subChaine1;
                        pos = valueString.find_first_of(':'); // caratere separateur name:value = ':'
                        if (pos != std::string::npos) {
                                        nameString.erase(pos); // isole dans le biname name
                                        valueString.erase(0, pos + 1); // isole dans le binome value
                                        // et maintenant on traite
                                if(nameString.compare("Address") ==0) {
                                         setAddress(valueString.c_str());
					//printf("sonde jl debug dans plugin Address =%s\n",valueString.c_str());
                                }
                                if(nameString.compare("Port") ==0) {
                                        setPort(atoi(valueString.c_str()));
					//printf("sonde jl debug dans plugin Port =%s\n",valueString.c_str());
                                }
                                if(nameString.compare("Connection") ==0)  {
                                        setMode(valueString);
					//printf("sonde jl debug dans plugin Connection =%s\n",valueString.c_str());
                                }
                                if(nameString.compare("Validity") ==0) {
                                        setValidity(valueString.c_str());
					//printf("sonde jl debug dans plugin validity =%s\n",valueString.c_str());
                                }

                        } else {
                                //ret=1;
                        }
                }
        }
        if(getValidity()) {              
          ret = Sopen();
	}
        return ret;
}

/**
* Plugin interface function
*
* Pure virtual function defined in the PluginInterface class
*
* \param[in]    chaine(string) : instruction and argument define in the MOS
*                       separation beetween argument = ' '
* \param[in]    commandStringAck(int) : 
*
*               : 0 = the device not answer after the write command
*
*               : 1 = The device answer so it's necessary to read the device
* \param[out]    result(string *) : 
*
*               : if commandStringAck=1, put the result of the reading of the device int the string 'result'
*
*
* \return       return the status of this function
*
*               : 0 = no error
*
*               : -1 = error
*
* This function is called by the MOS when a OPCUA method is called by a client.(not for  set/get methods)
* The MOS application put into the string 'chaine' the instruction for the device
*/
int TCP_Plugin::cmd(const std::string& chaine, int commandStringAck,std::string& result) {
	int ret=0;
	std::string result2="";
	//char chaine2[2048];
	//sprintf(chaine2,"%s",chaine.c_str());
#ifdef DEBUG
	printf("sonde jl TCP_Plugin::cmd %s",chaine2);
#endif
	
	ret = Swrite(chaine.c_str());
	//ret = Swrite(chaine2);
	if((!ret) && (commandStringAck) ) {
	  	ret = Sread(recvBuffer);
        	if(ret>0) {
                        result=recvBuffer;
		}
        }
	else {
		result=result2;
	}
	result=result2;
	
	return ret;
}

/**
* internal function
*
*
* \param[in]    chaine(string) : instruction and argument define in the MOS
*                       separation beetween argument = ' '
* \param[in]    commandStringAck(int) :
*
*               : 0 = the device not answer after the write command
*
*               : 1 = The device answer so it's necessary to read the device
*
* \return       return the status of this function
*
*               : 0 = no error
*
*               : -1 = error
*
* This function is called by the opthers function 'get'.
* This function write the instruction to the device
*/
int TCP_Plugin::set(const std::string& chaine, int commandStringAck) {        
	int ret = 0;     
	std::string result;
        ret = Swrite(chaine.c_str());
	if((!ret) && (commandStringAck) ) {
                ret = Sread(recvBuffer);
                if(ret>0) {
                        result=recvBuffer;
                }
        }
	return ret;
}


int TCP_Plugin::get(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue) {
        int ret=0;
	std::string result;
        if(!chaine.empty()) {

	get(chaine,commandStringAck,result);
	tabValue.resize(1);
	
	float val;
	val = atof(result.c_str());
	(tabValue)[0] = val;

                if(string *pstr = boost::any_cast<string>(&(tabValue)[0])){
			(tabValue)[0]=(string) result.c_str();
                }
                else if (int *pi = boost::any_cast <int> (&(tabValue)[0]))
                {
			(tabValue)[0] = (int) atoi(result.c_str());
                }
                else if (float *pi = boost::any_cast <float> (&(tabValue)[0]))
                {
			(tabValue)[0] = (float) atof(result.c_str());
                }
                else if (double *pi = boost::any_cast <double> (&(tabValue)[0]))
                {
			(tabValue)[0] = (double) atof(result.c_str());
                }
                else if (short int *pi = boost::any_cast <short int> (&(tabValue)[0]))
                {
			(tabValue)[0] = (short int) atoi(result.c_str());
                }
                else if (long *pi = boost::any_cast <long> (&(tabValue)[0]))
                {
			(tabValue)[0] = (long) atol(result.c_str());
                }
                else if (bool *pi = boost::any_cast <bool> (&(tabValue)[0]))
                {
			(tabValue)[0]= (bool) atoi(result.c_str()) ;
                }
                else
                {
		}
	}
        return ret;
}

int TCP_Plugin::set(const std::string& l_chaine,int commandStringAck, std::vector<boost::any>& tabValue){
        int ret=0;
	 std::string chaine=l_chaine;
        if(string *pstr = boost::any_cast<string>(&tabValue[0])){
                        chaine += " ";
                        chaine += *pstr;
                }
                else if (int *pi = boost::any_cast <int> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (float *pi = boost::any_cast <float> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (double *pi = boost::any_cast <double> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (short int *pi = boost::any_cast <short int> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (long *pi = boost::any_cast <long> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (bool *pi = boost::any_cast <bool> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else
                {
                        printf(" unknown type\n");
                }

        printf("sonde jl SqueletonTest::set() --> boost::any() avec chaine=%s\n",chaine.c_str());
	set(chaine,commandStringAck);


        return ret;
}

/**
* Plugin interface function
*
* Pure virtual function defined in the PluginInterface class
*
* \param[in]    chaine(string) : instruction and argument define in the MOS
*                       separation beetween argument = ' '
* \param[in]    commandStringAck(int) :
*
*               : 0 = the device not answer after the write command
*
*               : 1 = The device answer so it's necessary to read the device
* \param[out]   result (string* ) :
*		the result of the reading of the device.
*		this string  'result' will be store in the variable of the datapoint 
*
* \return       return the status of this function
*
*               : 0 = no error
*
*               : -1 = error
*
* This function is called by the MOS when a OPCUA method get is called by a client.
* This specific function 'get' is called because the variable of the datapoint in MOS is a define as a 'string'. 
* if the string 'chaine' containt an instruction, we write the instruction to the device
* if the string 'chaine' is empty  we just read the device
* so we call the get() function and store the result into the good 'type' here 'string' 
*/
int TCP_Plugin::get(const std::string& chaine, int commandStringAck,std::string& result) {
	int ret=0;

		if(chaine.size()!=0) {
                        ret = Swrite(chaine.c_str());
                        usleep(100000);
                }
                if(!ret) {
                        ret = Sread(recvBuffer);
                        if(commandStringAck) {
                                if(ret>0) {
                                        result= recvBuffer;
                                }
                        }
                }
        return ret;
}

/**
* Plugin interface function
*
* Pure virtual function defined in the PluginInterface class
*
* \return       return the status of this function
*
*               : 0 = no error
*
*               : -1 = error
*
* This function is called by the MOS at the end of  MOS application.
*/
int TCP_Plugin::close() {
	int ret=0;
        std::string retValue;

	ret = Sclose();
	return ret;
}

extern "C" {
TCP_Plugin *ptr_Plugin() {
	return new TCP_Plugin();
}
}

