project(SN_Coil_testInfinySynchrone)
cmake_minimum_required(VERSION 2.8)
SET(CMAKE_DEBUG_POSTFIX "d")

    
MESSAGE( STATUS "" )
MESSAGE( STATUS "==============================================" )
MESSAGE( STATUS "Now configuring serveur_OPCUA CMakeLists.txt" )
MESSAGE( STATUS "==============================================" )

    include_directories(${CMAKE_CURRENT_SOURCE_DIR})
    include_directories(${CMAKE_CURRENT_SOURCE_DIR}/share)
    include_directories(${CMAKE_CURRENT_SOURCE_DIR}/share/socket)
    include_directories(${MOS_PATH}/include/MOS_Plugin)
    link_directories(${MOS_PATH}/lib)




IF (UNIX)
    INCLUDE_DIRECTORIES()
    ADD_DEFINITIONS(-Wall)
    SET(PLATTFORM_LIBS pthread rt dl)
ENDIF (UNIX)


# create library
add_library(
         ${PROJECT_NAME}
        SHARED
        Coil.cpp
        )

#TARGET_LINK_LIBRARIES(${PROJECT_NAME} PluginImpl)
TARGET_LINK_LIBRARIES(${PROJECT_NAME} PluginBase)
set_target_properties( PROPERTIES DEBUG_POSTFIX "d")

# configure output path
INSTALL(TARGETS  ${PROJECT_NAME}
        RUNTIME DESTINATION ../bin
        LIBRARY DESTINATION ../plugins
        ARCHIVE DESTINATION ../lib
)
