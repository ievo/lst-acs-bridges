#ifndef COIL_H_
#define COIL_H_

#include "socketTools.h"
#include "pluginsBase.h"

//class PluginsBase;
class CoilThread;
class Socket;

#define DEFINE_PROTO IPPROTO_TCP

class Coil: public PluginsBase {
public:
	Coil();
	~Coil();
	int init(const std::string& chaine);
	int close();
	int cmd(const std::string& chaine, int commandStringAck,
			std::string& result);

// new virtual methods appears with the version 3.0 of MOS
	int afterStart();
	int cmdAsynch(const std::string& command, int commandStringAck,
			const std::string& datapointName, int nameSpace,
			std::string& result);

// new virtual methods replace olde get/set methods with the version 4.0 of MOS
	int get(const std::string& chaine, int commandStringAck,
			std::vector<boost::any>& tabValue);
	int set(const std::string& chaine, int commandStringAck,
			std::vector<boost::any>& tabValue);

private:
	CoilThread* m_testThread;
	Socket *m_socket;
	void open(const std::string& chaine);
	int set(const std::string& chaine, int commandStringAck);
	int get(const std::string& chaine, int commandStringAck,
			std::string& result);
	char recvBuffer[8192];
	int m_nbCycle;
	int m_rampDelay;
	int m_stepDelay;
	float m_voltage;

};
#endif //  COIL_H_
