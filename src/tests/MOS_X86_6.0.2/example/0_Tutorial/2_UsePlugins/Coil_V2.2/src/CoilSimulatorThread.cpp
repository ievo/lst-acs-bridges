/******************************************************************************
 **
 **
 ** Copyright (C) LAPP. CNRS
 **
 ** Project: C++ OPCUA generique
 **
 ** Description:
 **
 ** Author : Panazol Jean Luc
 ******************************************************************************/

#include "CoilSimulatorThread.h"
#include "dataAccessClientOPCUA.h"
#include "socketSimulator.h"

CoilThread::CoilThread(DataAccessClientOPCUA* dataAccessClientOPCUA) {
	m_stepDelay = 1;
	m_raspberryController=NULL;
	nbCycle= 1;
	m_voltage=1;
	m_nameSpace=2;
	m_rampDelay=1;
	m_nbCycle=1;

	m_nameSpaceStop =0;
	m_pause = false;
	m_stop = false;
	m_command = 0;
	m_cmdStartCycling = 0;
	m_cmdStopCycling = 0;
	m_dataAccessClientOPCUA = dataAccessClientOPCUA;
	m_socket = SocketSimulator::getInstance();
	first = 1;
	printf("sonde jl CoilThread::CoilThread\n");
}

CoilThread::~CoilThread() {
	m_pause = true;
	m_stop = true;
	wait();
}

void CoilThread::pause() {
	m_pause = true;
}

void CoilThread::resume() {
	m_pause = false;
}

int CoilThread::stop() {
	int ret = 0;
	m_stop = true;
	return ret;
}

int CoilThread::cmdStopCycling(std::string datapointName, int nameSpace) {
	int ret = 0;
	printf("***************** sonde jl CoilThread::cmdStopCycling() debut \n");
	m_datapointNameStop = datapointName;
	m_nameSpaceStop = nameSpace;
	m_cmdStopCycling = 1;
	return ret;
}

int CoilThread::cmdStartCycling(std::string datapointName, int nameSpace,
		int nbCycle, int rampDelay, int stepDelay, float voltage) {
	int ret = 0;
	m_cmdStartCycling = 1;
	m_datapointName = datapointName;
	m_nameSpace = nameSpace;
	m_nbCycle = nbCycle;
	m_rampDelay = rampDelay;
	m_stepDelay = stepDelay;
	m_voltage = voltage;
	return ret;
}

void *CoilThread::run(void *params) {
	int cpt = 0;
	std::string temString;
	std::string tempValue;
	int t = 0;
	float val = 0;
	int cptState = 0;
	int state = 0;
	int delay = 0;
	float range_V;
	nbCycle = 0;
	while (m_stop == false) {
		if (m_pause == false) {
			usleep(500000);
			cpt++;
				//printf("sonde jl state=%d\n", state);
			if (m_cmdStopCycling == 1) {
				m_cmdStartCycling = 0;
				m_cmdStopCycling = 0;
				nbCycle = 0;
				first = 1;
				state = 0;
				cptState = 0;
				delay = 0;
				val = 0;
				std::string temString;
				temString = m_datapointName + "._Done";
				m_dataAccessClientOPCUA->setDatapoint(temString, m_nameSpace,
						true);
				temString = m_datapointName + "._Error";
				m_dataAccessClientOPCUA->setDatapoint(temString, m_nameSpace,
						true);
				temString = m_datapointName + "._InProgress";
				m_dataAccessClientOPCUA->setDatapoint(temString, m_nameSpace,
						false);

				temString = m_datapointNameStop + "._Done";
				m_dataAccessClientOPCUA->setDatapoint(temString,
						m_nameSpaceStop, true);
				temString = m_datapointNameStop + "._InProgress";
				m_dataAccessClientOPCUA->setDatapoint(temString,
						m_nameSpaceStop, false);
			}
			if (m_cmdStartCycling == 1) {
				if (first) {
					printf("sonde jl first=1\n");
					delay = m_rampDelay;
					range_V = m_voltage / (m_rampDelay*2);
					first = 0;
					nbCycle = 0;
				}
				printf("sonde jl ici debug delay=%d nbCycle=%d m_nbCycle=%d m_stepDelay=%d m_rampDelay=%d\n",delay,nbCycle,m_nbCycle,m_stepDelay,m_rampDelay);
				temString = m_datapointName + "._InProgressBar";
				t = (cptState);
				m_dataAccessClientOPCUA->setDatapoint(temString, m_nameSpace,
						t);
				printf("sonde jl state=%d\n", state);
				if (nbCycle == 0) {
					temString = m_datapointName + "._Done";
					m_dataAccessClientOPCUA->setDatapoint(temString,
							m_nameSpace, false);
					temString = m_datapointName + "._InProgress";
					m_dataAccessClientOPCUA->setDatapoint(temString,
							m_nameSpace, true);
				}

				if (nbCycle == m_nbCycle) {
					m_cmdStartCycling = 0;
					temString = m_datapointName + "._Done";
					m_dataAccessClientOPCUA->setDatapoint(temString,
							m_nameSpace, true);
					temString = m_datapointName + "._InProgress";
					m_dataAccessClientOPCUA->setDatapoint(temString,
							m_nameSpace, false);
					nbCycle = 0;
					first = 1;

				} else {
					if (state == 0) {
						cptState++;
						std::string teststring = "SOURce:VOLTage ";
						val += range_V;

						char charString[20];
						sprintf(charString, "%f", val);
						teststring += charString;
						teststring += "\n";
	printf("sonde jl Swrite avec %s\n",teststring.c_str());
	printf("sonde jl cptState= %d\n",cptState);
						m_socket->Swrite(teststring.c_str());
						if (cptState == (delay*2)) {
							state = 1;
							delay += m_stepDelay;
						}
					}
					if (state == 1) {
						cptState++;
						if (cptState == (delay*2)) {
							state = 2;
							delay += m_rampDelay;
						}
					}
					if (state == 2) {
						cptState++;
						std::string teststring = "SOURce:VOLTage ";
						val -= range_V;
						char charString[20];
						sprintf(charString, "%f", val);
						teststring += charString;
						teststring += "\n";
	printf("sonde jl Swrite avec %s\n",teststring.c_str());
						m_socket->Swrite(teststring.c_str());

						if (cptState == (delay*2)) {
							state = 3;
							delay += m_stepDelay;
						}
					}
					if (state == 3) {
						cptState++;
						if (cptState == (delay*2)) {
							state = 0;
							cptState = 0;
							nbCycle++;
							delay = m_rampDelay;
						}
					}
				}
			}

		}
	}
	return NULL;
}

int CoilThread::startRun() {
	int ret = 0;

	printf("plugin CoilThread startRun\n");
	start(NULL);
	return ret;
}

