typedef unsigned char Byte;

#include "socket.h"

#include "commonTypes.h"

#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

#include <sys/ioctl.h>

#define SIZE_EVT_TYPE_1 1024
#define MAX_CLIENTS 	100
#define NBERR 	1

Socket *Socket::singleton = NULL;


/** get the Singleton instance reference or create unique instance */
Socket *Socket::getInstance(std::string address, int port, int protocol) {
        if (!singleton) {
                singleton = new Socket(address,port,protocol);
        }
        return singleton;
}

/** get the Singleton instance reference or create unique instance */
Socket *Socket::getInstance() {
        return singleton;
}


/** Kill the singleton instance */
void Socket::kill() {
        if (NULL != singleton) {
                delete singleton;
                singleton = NULL;
        }
}


/*int closeSocket(int socketId) {
	int ret=0;
	close(socketId);

	return 0;
}*/


Socket::Socket(std::string address, int port, int protocol) {
	m_address = address;
	m_port = port;
	m_protocol = protocol;
	m_status = 0;
	m_NbErreur=0;
	socketIdClient=0;

	char buf[32];

	if (address.compare("localhost") == 0) {
		gethostname(buf, sizeof buf);
		hostent* localHost = gethostbyname("134.158.96.192");
		char* localIP = inet_ntoa(*(struct in_addr *) *localHost->h_addr_list);
		m_address = localIP;
	}
}

Socket::~Socket() {
	if (m_connection_t == SOCKET_PERMANENT)
		Sclose();
}

int Socket::Swrite(std::string buf) {
	char sendBuffer[2048];
	unsigned int length;
	int n;
	int ret=0;
	struct sockaddr_in addr_local;

	bzero(&addr_local, sizeof(struct sockaddr_in));
	length = sizeof(struct sockaddr_in);
	addr_local.sin_family = AF_INET;
	addr_local.sin_port = htons(m_port);
	addr_local.sin_addr.s_addr = inet_addr(m_address.c_str());

	// 12/09/14 bug sprintf(sendBuffer, "%s %d", buf.c_str(), socketIdCtrl);
	sprintf(sendBuffer, "%s", buf.c_str());

	if (m_protocol == IPPROTO_UDP) {
printf("sonde jl 14/02/2017 Socket::Swrite() m_protocol==IPPROTO_UDP\n");
		n = sendto(socketIdCtrl, sendBuffer, strlen(sendBuffer), 0,
				(const struct sockaddr *) &addr_local, length);
		if (n < 0) {
			printf("(Socket) sendto failed! Error: \n");
			return -1;
		}
	} else {
printf("sonde jl 14/02/2017 Socket::Swrite() m_protocol==IPPROTO_TCP\n");
		if (m_clientServer_t == SERVER) {
printf("sonde jl 14/02/2017 Socket::Swrite() m_clientServer_t == SERVER\n");
printf("sonde jl 14/02/2017 Socket::Swrite() m_status == %d\n",m_status);
                        if(m_status<0) Saccept();
                        if(m_status>=0) {
printf("sonde jl 14/02/2017 Socket::Swrite() sendBuffer == %s\n",sendBuffer);
                                if (send(socketIdClient, sendBuffer, strlen(sendBuffer), 0) < 0) {
                                      	printf("(Socket) send failed! Error: \n");
                                        m_status=-1;
                                        return -1;
                                }
                                        else m_status =0;
                        }

                }else {
printf("sonde jl 14/02/2017 Socket::Swrite() m_clientServer_t == CLIENT\n");
printf("sonde jl 14/02/2017 Socket::Swrite() m_status == %d\n",m_status);
                        if(m_status<0) {
					if(m_NbErreur<NBERR) {
                                        	ret = Sopen();
						if(ret==0) m_NbErreur=0;
					}
                        }
printf("sonde jl 14/02/2017 Socket::Swrite() sendBuffer == %s\n",sendBuffer);
                        if (send(socketIdCtrl, sendBuffer, strlen(sendBuffer), 0) < 0) {
                                 printf("(Socket) send failed! Error: \n");
                                m_status=-1;
                                close(socketIdCtrl);

                                return -1;
                        }
                }
	}
	return 0;
}

int Socket::Sread(char *buf) {
	int n = 0;


	struct sockaddr_in cin;
	memset(&cin, 0, sizeof(cin));
	socklen_t sinsize = sizeof cin;
	if (m_protocol == IPPROTO_UDP) {
		if ((n = recvfrom(socketIdCtrl, buf, SIZE_EVT_TYPE_1 - 1, 0,
				(struct sockaddr *) &cin, &sinsize)) < 0) {
			n = 0;
			goto ERROR;
		} else {
			(buf)[n] = 0;
		}
		(buf)[n] = 0;
	} else {
		if (m_clientServer_t == SERVER) {
                        if(socketIdClient<=0) Saccept();
                        if(socketIdClient>0) {
				n = recv(socketIdClient, buf, SIZE_EVT_TYPE_1 - 1, 0);
				if(n==0) {
	                                n = 0;
					socketIdClient=0;
	                                goto ERROR;
				}
				if(n==-1) {
	                                //printf("sonde jl Socket::Sread avec recv n=%d\n", n);
	                                n = 0;
	                                goto ERROR;
	                        } else {
	                                (buf)[n] = 0;
	                                //printf("sonde jl Socket::Sread avec %s\n",buf);
	                        }
                        }

                }else {
			if ((n = recv(socketIdCtrl, buf, SIZE_EVT_TYPE_1 - 1, 0)) < 0) {
				n = 0;
				goto ERROR;
			} else {
				(buf)[n] = 0;
			}
		}
	}
	ERROR: ;
	//printf("sonde jl Socket::Sread end n=%d\n", n);
	return n;
}

 void Socket::setAddress(std::string address) {
	m_address= address;
}

        
void Socket::setPort(int port){
	m_port = port;
}

        
void Socket::setValidity(std::string validity){
	if(validity.compare("permanent")==0)
		m_connection_t=1;
	else
		m_connection_t=0;
}

int Socket::getValidity() {
	return m_connection_t;
}

void Socket::setMode(std::string connection){
	if(connection.compare("server")==0)
 		m_clientServer_t = SERVER;        
	else        
		m_clientServer_t = CLIENT;
}



int Socket::Saccept() {
        int ret=0;
 /* new client */
        struct sockaddr_in csin; // = { 0 };
        memset(&csin, 0, sizeof(csin));
        // 12/09/14 modif car erreur dans SL6 mais fonctionnait dans fedora size_t sinsize = sizeof csin;
 	socklen_t sinsize = sizeof csin;

         socketIdClient = accept(socketIdCtrl, (struct sockaddr *)&csin, &sinsize);
        if(socketIdClient==-1) m_status =0;
        else m_status=1;

        //printf("sonde jl Socket::Saccept socketIdClient=%d\n",socketIdClient);

        return ret;
}

int Socket::Sopen() {
                                printf("(Socket) Socket::Sopen()\n");
        int ret=0;
        value = 0;
        // ctrl socket
        struct timeval timeOut;
        //tErrorCode errorCode = NO_ERROR;
        socketIdCtrl = 0;
        timeOut.tv_sec = 0;
        timeOut.tv_usec = 5000;
        int yes = 1;
        struct sockaddr_in sin; // = { 0 };
        memset(&sin, 0, sizeof(sin));
        if (m_protocol == IPPROTO_UDP) {
                /* UDP so SOCK_DGRAM */
                //printf("UDP so SOCK_DGRAM \n");
                socketIdCtrl = socket(AF_INET, SOCK_DGRAM, 0);
        } else {
                /* TCP so SOCK_STREAM */
                printf(" TCP so SOCK_STREAM \n");
                socketIdCtrl = socket(AF_INET, SOCK_STREAM, 0);
        }
       if (setsockopt(socketIdCtrl, SOL_SOCKET, SO_RCVTIMEO, (void *) (&(timeOut)),
                        sizeof(timeOut)) == -1) {
		ret =-1;
		m_NbErreur++;
                printf("(Socket) Error socket timeout\n");
                goto ERROR;
        }

        if (m_protocol == IPPROTO_UDP) {

                if (setsockopt(socketIdCtrl, SOL_SOCKET, SO_REUSEADDR, &yes,
                                sizeof(yes)) == -1) {
                        ret=-1;
			m_NbErreur++;
                        printf("(Socket) error bind\n");
                        goto ERROR;
                }
        }

        if (socketIdCtrl == -1) {
                ret=-1;
		m_NbErreur++;
                printf("(Socket) error socket\n");
                goto ERROR;
        }
        sin.sin_addr.s_addr = inet_addr(m_address.c_str());
        sin.sin_port = htons(m_port);
        sin.sin_family = AF_INET;

       if (m_clientServer_t == SERVER) {
                if (bind(socketIdCtrl, (struct sockaddr*) &sin, sizeof sin) == -1) {
                        ret=-1;
			m_NbErreur++;
                        printf("(Socket) error bind\n");
                        goto ERROR;
                }
                if (m_protocol == IPPROTO_TCP) {
                        Saccept();
                        if (listen(socketIdCtrl, MAX_CLIENTS) == -1) {
                                ret=-1;
				m_NbErreur++;
                                printf("(Socket) error connect \n");
                        }
                }
        } else {
                printf("(Socket) (m_clientServer_t ==Client\n");
                if (m_protocol == IPPROTO_TCP) {
                printf("(Socket) m_protocol == IPPROTO_TCP\n");
			printf("sonde jl socketIdCtrl=%d \n",socketIdCtrl);
			printf("sonde jl m_address=%s \n",m_address.c_str());
                        if (connect(socketIdCtrl, (struct sockaddr *) &sin,
                                        sizeof(struct sockaddr)) == -1) {
                                printf("(Socket) error connect \n");
				m_NbErreur++;
                                m_status =-1;
                                ret=-2;
                        }
                        else m_status=0;
                }

        }
        ERROR:
        return ret;
}

int Socket::Sclose() {
	int ret=0;
	close(socketIdCtrl);
	//closeSocket(socketIdCtrl);
	return ret;
}

int Socket::Sclose(int sock) {
	int ret=0;
	//closeSocket(sock);
	close(sock);
	return ret;
}

