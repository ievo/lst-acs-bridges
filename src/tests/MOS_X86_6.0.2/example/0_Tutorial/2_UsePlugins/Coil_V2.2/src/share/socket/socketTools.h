#ifndef SOCKETTOOLS_H

#define SOCKETTOOLS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "commonTypes.h"

#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h> 
#include <unistd.h>
#include <time.h>

  tErrorCode  openSocketClient(tSocketConfiguration socketConfiguration, int * socketId,int protocol);
  tErrorCode  openSocket(tSocketConfiguration socketConfiguration, int * socketId);
  tErrorCode closeSocket(int socketId);


#ifdef __cplusplus
}
#endif

#endif
