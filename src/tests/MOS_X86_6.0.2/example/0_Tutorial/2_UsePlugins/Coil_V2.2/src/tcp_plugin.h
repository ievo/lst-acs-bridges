/**
 *******************************************************************************
 *
 * @file    plugin_tcp.h
 * @authors panazol@lapp.in2p3.fr
 * @date    10/09/2014
 * @version v1.0.2
 * @brief description :
 *     plugin for the communication with tcp socket
 *     Used by the MOS application
 *
 *
 * @details
 *  <full description...>
 *
 *------------------------------------------------------------------------------
 *
 * @copyright Copyright © 2014, LAPP/CNRS
 *
 *
 *******************************************************************************
 */

#ifndef TCP_PLUGIN_H_
#define TCP_PLUGIN_H_

#include <iostream>
#include <vector>
#include <cstring>
#include <cstdlib>


#include "cta_slc_pluginsInterface.h"
#include "socketTools.h"
#include "socket.h"

/**
 * class TCP_Plugin:
 *
 * @authors panazol@lapp.in2p3.fr
 * @date creation :   19/09/2014
 * @date modification with description:
 *
 * @brief description of this function :
 *    This class open/read/write/close the communication with a socket (TCP)
 */
class TCP_Plugin: public PluginsInterface, public Socket {
public:
	TCP_Plugin();

        int close();
        int init(const std::string& chaine);

        int cmd(const std::string& chaine, int commandStringAck, std::string& result);

        int set(const std::string& chaine,int commandStringAck) ;
        int get(const std::string& chaine, int commandStringAck,std::string& result);
        int set(const std::string& command, int commandStringAck, std::vector<boost::any>& value);
        int get(const std::string& command, int commandStringAck, std::vector<boost::any>& res);

        int afterStart() {return 1;};
        int cmdAsynch(const std::string& command, int commandStringAck, const std::string& datapointName, int namesSpace, std::string& result) {return 1;};
 
private:
	void open(const std::string& chaine);
	char recvBuffer[8192];

};

typedef TCP_Plugin *(*maker_protocole1)();
#endif //  TCP_Plugin_H_
