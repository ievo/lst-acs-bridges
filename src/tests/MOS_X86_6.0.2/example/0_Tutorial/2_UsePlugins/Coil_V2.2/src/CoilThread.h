/******************************************************************************
 **
 **
 ** Copyright (C) LAPP. CNRS
 **
 ** Project: C++ OPCUA generique
 **
 ** Description:
 **
 ** Author : Panazol Jean Luc
 ******************************************************************************/

#ifndef __COILTHREAD_H__
#define __COILTHREAD_H__

#include "string"
#include "lappThread.h"

typedef unsigned char Byte;

class DataAccessClientOPCUA;
class Socket;

class CoilThread: public LAPPThread {
public:
	CoilThread(DataAccessClientOPCUA* m_dataAccessClientOPCUA);
	~CoilThread();
	void* run(void *params);

	int  stop();
	void pause();
	void resume();
	int  startRun();
	int cmdStartCycling( std::string datapointName, int nameSpace,int nbCycle, int rampDelay, int stepDelay, float voltage);
	int cmdStopCycling( std::string datapointName, int nameSpace);
private:
	CoilThread* m_raspberryController;
	int m_stop;
	int m_pause;
	int m_command;
	int m_cmdStopCycling;
	int m_cmdStartCycling;
	int m_nameSpace;
	int m_nameSpaceStop;
	int m_nbCycle;
        int m_rampDelay;
        int m_stepDelay;
	float m_voltage;
	int nbCycle;
	int first;

	std::string m_datapointName;
	std::string m_datapointNameStop;
        DataAccessClientOPCUA* m_dataAccessClientOPCUA;
	Socket *m_socket;
};

#endif // __COILTHREAD_H__
