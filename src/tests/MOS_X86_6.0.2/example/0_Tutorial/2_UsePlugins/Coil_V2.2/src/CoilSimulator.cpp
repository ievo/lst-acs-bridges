#include "CoilSimulatorThread.h"
#include "CoilSimulator.h"
#include "socketSimulator.h"

using namespace std;


//Coil() : Socket("", 1, DEFINE_PROTO) {
Coil::Coil() {
printf("sonde jl debut constructeur Coil version 2\n");
	m_socket = SocketSimulator::getInstance("", 1, DEFINE_PROTO);

	m_stepDelay = 1;
	m_testThread=NULL;
	m_voltage=1;
	m_rampDelay=1;
	m_nbCycle=1;

printf("sonde jl fin constructeur Coil\n");
}

Coil::~Coil() {};


int Coil::get(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue) {
        int ret=0;
	std::string result;
        //printf("sonde jl Coil::get() debut\n");
//        printf("sonde jl Coil::get()1 --> boost::any() commandStringAck=%di chaine=*%s*\n",commandStringAck,chaine.c_str());
        if(!chaine.empty()) {
	std::string chaine2=chaine;
	chaine2 += "\n";
	get(chaine2,commandStringAck,result);
	tabValue.resize(1);
        //printf("sonde jl Coil::get() result=*%s*\n",result.c_str());
	
	float val;
	val = atof(result.c_str());
	(tabValue)[0] = val;

                if(string *pstr = boost::any_cast<string>(&(tabValue)[0])){
			(tabValue)[0]=(string) result.c_str();
                }
                else if (int *pi = boost::any_cast <int> (&(tabValue)[0]))
                {
			(tabValue)[0] = (int) atoi(result.c_str());
                }
                else if (float *pi = boost::any_cast <float> (&(tabValue)[0]))
                {
			(tabValue)[0] = (float) atof(result.c_str());
                }
                else if (double *pi = boost::any_cast <double> (&(tabValue)[0]))
                {
			(tabValue)[0] = (double) atof(result.c_str());
                }
                else if (short int *pi = boost::any_cast <short int> (&(tabValue)[0]))
                {
			(tabValue)[0] = (short int) atoi(result.c_str());
                }
                else if (long *pi = boost::any_cast <long> (&(tabValue)[0]))
                {
			(tabValue)[0] = (long) atol(result.c_str());
                }
                else if (bool *pi = boost::any_cast <bool> (&(tabValue)[0]))
                {
			(tabValue)[0]= (bool) atoi(result.c_str()) ;
                }
                else
                {
		}
	}
        return ret;
}

int Coil::get(const std::string& chaine, int commandStringAck,std::string& result) {
	int ret=0;
        //printf("sonde jl Coil::get() chaine=%s commandStringAck=%d\n",chaine.c_str(),commandStringAck);

		if(chaine.size()!=0) {
                        ret = m_socket->Swrite(chaine.c_str());
                        usleep(100000);
                }
                if(!ret) {
        		sprintf(recvBuffer,"test");
                        ret = m_socket->Sread(recvBuffer);
        		//printf("sonde jl idebug version 11/06/18 Coil::get() chaine=%s commandStringAck=%d\n",recvBuffer,commandStringAck);
                        if(commandStringAck) {
                                if(ret>0) {
                                        result= recvBuffer;
                                }
                        }
                }

        return ret;
}

int Coil::set(const std::string& chaine, int commandStringAck) {
        int ret = 0;
        std::string result;
        ret = m_socket->Swrite(chaine.c_str());
        if((!ret) && (commandStringAck) ) {
                ret = m_socket->Sread(recvBuffer);
                if(ret>0) {
                        result=recvBuffer;
                }
        }
        return ret;
}

int Coil::set(const std::string& l_chaine,int commandStringAck, std::vector<boost::any>& tabValue){
        int ret=0;
	 std::string chaine=l_chaine;
        //printf("sonde jl Coil::set() --> boost::any() l_chaine=%s\n",l_chaine.c_str());
        if(string *pstr = boost::any_cast<string>(&tabValue[0])){
                        chaine += " ";
                        chaine += *pstr;
                }
                else if (int *pi = boost::any_cast <int> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (float *pi = boost::any_cast <float> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (double *pi = boost::any_cast <double> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (short int *pi = boost::any_cast <short int> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (long *pi = boost::any_cast <long> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else if (bool *pi = boost::any_cast <bool> (&tabValue[0]))
                {
                        chaine += " ";
                        chaine += *pi;
                }
                else
                {
                        printf(" unknown type\n");
                }

	set(chaine,commandStringAck);


        return ret;
}


// you can overwrite this method if you want but not mandatory because the class pluginsInterfaceImpl already implement it:)
// but becarefull, you have to call before doing  your bussiness, call the father method (the father class) ( PluginsBase::init())
//
// This method is automaticaly call by the program "MOS" after "MOS" server is launched but the "MOS" server is not really ready.
// So don't use this method in ordr to communicate with the "MOS" Server.
// you can use the afertStart() method if needed.
int Coil::init(const std::string& l_chaine) {
	printf("sonde jl TCP_Plugin::init() avec chaine=%s\n",l_chaine.c_str());
// Mandatory allways need 
	PluginsBase::init(l_chaine);
//
printf("sonde jl debut Coil::init() version 05/03/2017\n");
	std::string chaine=l_chaine;
        int ret=0;
	chaine = chaine + " ";
        std::string::size_type pos;
        std::string valueString;
        std::string nameString;
        std::string subChaine1 = chaine;
        std::string subChaine2 = chaine;
	printf("sonde jl TCP_Plugin::init() avec chaine=%s\n",chaine.c_str());
        int flag=1;
        while (flag) {
                subChaine1 = subChaine2;
                pos = subChaine2.find(' '); // cherche separateur ' '
                if (pos == std::string::npos) // il n'y a plus : alors on sort 
                        flag = 0; 
                else {
                         subChaine1.erase(pos); // isole le binome name:value
                         subChaine2.erase(0, pos + 1); // reste

                        valueString = subChaine1;
                        nameString = subChaine1;
                        pos = valueString.find_first_of(':'); // caratere separateur name:value = ':'
                        if (pos != std::string::npos) {
                                        nameString.erase(pos); // isole dans le biname name
                                        valueString.erase(0, pos + 1); // isole dans le binome value
                                        // et maintenant on traite
                                if(nameString.compare("Address") ==0) {
                                         m_socket->setAddress(valueString.c_str());
					//printf("sonde jl debug dans plugin Address =%s\n",valueString.c_str());
                                }
                                if(nameString.compare("Port") ==0) {
                                        m_socket->setPort(atoi(valueString.c_str()));
					//printf("sonde jl debug dans plugin Port =%s\n",valueString.c_str());
                                }
                                if(nameString.compare("Connection") ==0)  {
                                         m_socket->setMode(valueString);
					//printf("sonde jl debug dans plugin Connection =%s\n",valueString.c_str());
                                }
                                if(nameString.compare("Validity") ==0) {
                                         m_socket->setValidity(valueString.c_str());
					//printf("sonde jl debug dans plugin validity =%s\n",valueString.c_str());
                                }

                        } else {
                                //ret=1;
                        }
                }
        }

	printf("sonde jl TCP_Plugin::init() 1 avec chaine=%s\n",chaine.c_str());
        if( m_socket->getValidity()) {              
	printf("sonde jl TCP_Plugin::init() 2 avec chaine=%s\n",chaine.c_str());
          ret = m_socket->Sopen();
	}

	printf("sonde jl TCP_Plugin::init() test 05/03/2017 fin avec chaine=%s\n",chaine.c_str());
        return ret;
}

// you can overwrite this method if you want but not mandatory because the class pluginsInterfaceImpl already implement it:)
// but becarefull, you have to call before doing  your bussiness, call the father method (the father class) ( PluginsBase::afterStart())
//
// This method is automaticaly call by the program "MOS" after "MOS" server is launched and ready.
int Coil::afterStart() {
printf("sonde jl debut Coil::afterStart()\n");
	int ret=0;

// Mandatory allways need 
	ret = PluginsBase::afterStart();
// 
printf("sonde jl apres PluginsBase::afterStart11111111 Coil::afterStart()ret=%d\n",ret);
//	if(ret != -1) {
// here an example in order to call method as a client to the server 
// here call the method GetMonitoring() with 1 Input argument
// and print the Output Argument of the method
	        std::string resultCall;

		m_testThread = new CoilThread(getDataAccessClientOPCUARef());
                ret = m_testThread->startRun();
//	}
printf("sonde jl fin Coil::afterStart()\n");
	return ret;
}

int Coil::cmdAsynch(const std::string& command, int commandStringAck, const std::string& datapointName, int nameSpace, std::string& result) {
	// not use in this example
	int ret=0;
	result ="";
	std::string chaine = command + " ";
        std::string::size_type pos;
        std::string valueString;
        std::string nameString;
        std::string subChaine1 = chaine;
        std::string subChaine2 = chaine;
        int flag=1;

        while (flag) {
                subChaine1 = subChaine2;
                pos = subChaine2.find(' '); // cherche separateur ' '
                if (pos == std::string::npos) // il n'y a plus : alors on sort 
                        flag = 0; 
                else {
                         subChaine1.erase(pos); // isole le binome name:value
                         subChaine2.erase(0, pos + 1); // reste

                        valueString = subChaine1;
                        nameString = subChaine1;
                        pos = valueString.find_first_of(':'); // caratere separateur name:value = ':'
                        if (pos != std::string::npos) {
                                        nameString.erase(pos); // isole dans le biname name
                                        valueString.erase(0, pos + 1); // isole dans le binome value
                                        // et maintenant on traite
                                if(nameString.compare("rampDelay") ==0) {
					m_rampDelay = atoi(valueString.c_str());
                                }
                                if(nameString.compare("stepDelay") ==0) {
					m_stepDelay = atoi(valueString.c_str());
                                }
                                if(nameString.compare("nbCycle") ==0)  {
					m_nbCycle = atoi(valueString.c_str());
                                }
                                if(nameString.compare("voltage") ==0)  {
					m_voltage = atof(valueString.c_str());
                                }
                        } else {
                                //ret=1;
                        }
                }
	}
	printf("sonde jl TCP_Plugin::cmd %s\n",command.c_str());
	if(command.find("StartCycling")==0) {
                 m_testThread->cmdStartCycling(datapointName,nameSpace,m_nbCycle, m_rampDelay, m_stepDelay,m_voltage);
        }
         if(command.compare("StopCycling")==0) {
                 m_testThread->cmdStopCycling(datapointName,nameSpace);
        }

	return ret;
}


int Coil::close() {
        int ret=0;
        std::string retValue;

        ret = m_socket->Sclose();
        return ret;
}

int Coil::cmd(const std::string& chaine, int commandStringAck, std::string& result) {
	int ret=0;
	std::string result2="";
	//char chaine2[2048];
	//sprintf(chaine2,"%s",chaine.c_str());
	printf("sonde jl TCP_Plugin::cmd %s",chaine.c_str());
#ifdef DEBUG
	printf("sonde jl TCP_Plugin::cmd %s",chaine.c_str());
#endif
	ret = m_socket->Swrite(chaine.c_str());
	//ret = Swrite(chaine2);
	if((!ret) && (commandStringAck) ) {
	  	ret = m_socket->Sread(recvBuffer);
        	if(ret>0) {
                        result=recvBuffer;
		}
        }
	else {
		result=result2;
	}
	result=result2;
	
	return ret;
}


// becarefull :  allways need : allow to connect this Plugin with MOS 
extern "C" {
Coil *make_protocole1() {
	return new Coil();
}
}

