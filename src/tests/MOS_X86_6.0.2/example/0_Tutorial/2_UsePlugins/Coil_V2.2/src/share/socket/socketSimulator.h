/******************************************************************************

 ******************************************************************************/
#ifndef __SOCKETSIMULATOR_H__
#define __SOCKETSIMULATOR_H__

#include "string"

#define CLIENT 0
#define SERVER 1
#define SOCKET_PERMANENT 1
#define SOCKET_TEMPORAIRE 0

class SocketSimulator {
 	static SocketSimulator *singleton;  // pointeur vers le singleton
public:
	SocketSimulator(std::string address, int port, int protocol);
	~SocketSimulator();
	static void kill();
	static SocketSimulator *getInstance(std::string address, int port, int protocol);
        static SocketSimulator *getInstance();

	int Sopen();
	int Saccept();
	int Sclose();
	int Sclose(int sock);
	int Swrite(std::string buf);
	int Sread(char *buf);
public:
	int value;
	int socketIdCtrl;
	int socketIdClient;

	void setAddress(std::string address);
	void setPort(int port);
	void setValidity(std::string validity);
	int  getValidity();
	void setMode(std::string connection);
	float m_simul;
	float m_current;
	float m_voltage;
private:
	std::string m_address;
	int m_port;
	int m_timeOut_sec;
	int m_timeOut_usec;
	int m_clientServer_t;
	int m_connection_t;
	int m_protocol;
	int m_status;
	int m_NbErreur;
	int m_laststatus;
};
#endif  // end SOCKET_H
