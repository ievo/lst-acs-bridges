#include "pluginsBase.h"
#include "test_asynchroneThread.h"
#include "squeletonPlugin_for_asynchroneMethodCall.h"

using namespace std;


int SqueletonTest::get(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue) {
        int ret=0;
        printf("sonde jl SqueletonTest::get() --> boost::any()\n");
        return ret;
}

int SqueletonTest::set(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue){
        int ret=0;
        printf("sonde jl SqueletonTest::set() --> boost::any()\n");
        return ret;
}


// you can overwrite this method if you want but not mandatory because the class pluginsInterfaceImpl already implement it:)
// but becarefull, you have to call before doing  your bussiness, call the father method (the father class) ( PluginsBase::init())
//
// This method is automaticaly call by the program "MOS" after "MOS" server is launched but the "MOS" server is not really ready.
// So don't use this method in ordr to communicate with the "MOS" Server.
// you can use the afertStart() method if needed.
int SqueletonTest::init(const std::string& chaine) {
	int ret=0;

// Mandatory allways need 
	PluginsBase::init(chaine);
//
	return ret;
}

// you can overwrite this method if you want but not mandatory because the class pluginsInterfaceImpl already implement it:)
// but becarefull, you have to call before doing  your bussiness, call the father method (the father class) ( PluginsBase::afterStart())
//
// This method is automaticaly call by the program "MOS" after "MOS" server is launched and ready.
int SqueletonTest::afterStart() {
	int ret=0;
// Mandatory allways need 
	ret = PluginsBase::afterStart();
// here an example in order to call method as a client to the server 
// here call the method GetMonitoring() with 1 Input argument
// and print the Output Argument of the method
	        std::string resultCall;
		m_testThread = new TestAsynchroneThread(getDataAccessClientOPCUARef());
                ret = m_testThread->startRun();
	return ret;
}

int SqueletonTest::cmdAsynch(const std::string& command, int commandStringAck, const std::string& datapointName, int nameSpace, std::string& result) {
	// not use in this example
	int ret=0;
	result ="";
	printf("sonde jl SqueletonTest::cmdAsynch\n");
	if(command.compare("CloseShutters")==0) {
                 m_testThread->cmdCloseShutter(datapointName,nameSpace);
        }
         if(command.compare("OpenShutters")==0) {
                 m_testThread->cmdOpenShutter(datapointName,nameSpace);
        }

	return ret;
}

int SqueletonTest::close() {
	// here we do noting
	int ret=0;
	return ret;
}

int SqueletonTest::cmd(const std::string& chaine, int commandStringAck, std::string& result) {
	int ret = 0;
	result="";
	return ret;
}


// becarefull :  allways need : allow to connect this Plugin with MOS 
extern "C" {
SqueletonTest *make_protocole1() {
	return new SqueletonTest();
}
}

