/******************************************************************************
 **
 **
 ** Copyright (C) LAPP. CNRS
 **
 ** Project: C++ OPCUA generique
 **
 ** Description:
 **
 ** Author : Panazol Jean Luc
 ******************************************************************************/

#ifndef __TESTASYNCHRONECONTROLLER_H__
#define __TESTASYNCHRONECONTROLLER_H__

#include "string"
#include "lappThread.h"

typedef unsigned char Byte;

class DataAccessClientOPCUA;

class TestAsynchroneThread: public LAPPThread {
public:
	TestAsynchroneThread(DataAccessClientOPCUA* m_dataAccessClientOPCUA);
	~TestAsynchroneThread();
	void* run(void *params);

	int  stop();
	void pause();
	void resume();
	int  startRun();
	int cmdCloseShutter( std::string datapointName, int nameSpace);
	int cmdOpenShutter( std::string datapointName, int nameSpace);
private:
	int m_stop;
	int m_pause;
	int m_command;
	int m_cmdCloseShutter;
	int m_cmdOpenShutter;
	int m_nameSpace;
	std::string m_datapointName;
        DataAccessClientOPCUA* m_dataAccessClientOPCUA;
};

#endif //__TESTASYNCHRONECONTROLLER_H__ 
