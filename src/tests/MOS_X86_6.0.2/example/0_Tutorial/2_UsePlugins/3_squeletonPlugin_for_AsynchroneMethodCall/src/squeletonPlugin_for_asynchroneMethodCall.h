#ifndef SQUELETONTEST_H_
#define SQUELETONTEST_H_

class PluginsBase;
class TestAsynchroneThread;

class SqueletonTest: public PluginsBase {
public:
        int init(const std::string& chaine);
        int close();
        int cmd(const std::string& chaine, int commandStringAck,std::string& result);

// new virtual methods appears with the version 3.0 of MOS
	int afterStart();
	int cmdAsynch(const std::string& command, int commandStringAck, const std::string& datapointName, int nameSpace, std::string& result);

// new virtual methods replace olde get/set methods with the version 4.0 of MOS
        int get(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue);
        int set(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue);

private:
        TestAsynchroneThread* m_testThread;

};
#endif //  SQUELETONTEST_H_
