/******************************************************************************
 **
 **
 ** Copyright (C) LAPP. CNRS
 **
 ** Project: C++ OPCUA generique
 **
 ** Description:
 **
 ** Author : Panazol Jean Luc
 ******************************************************************************/

#ifndef __LASERSTARTTHREAD_H__
#define __LASERSTARTTHREAD_H__

#include "string"
#include "lappThread.h"

typedef unsigned char Byte;

class DataAccessClientOPCUA;

class LaserStartThread: public LAPPThread {
public:
	LaserStartThread(DataAccessClientOPCUA* m_dataAccessClientOPCUA);
	~LaserStartThread();
	void* run(void *params);

	int  stop();
	void pause();
	void resume();
	int  startRun();
	int cmdStartLaser( std::string datapointName, int nameSpace);
	int cmdStopLaser();
private:
	int m_stop;
	int m_pause;
	int m_command;
	int m_cmdStartLaser;
	int m_cmdStopLaser;
	int m_nameSpace;
	std::string m_datapointName;
        DataAccessClientOPCUA* m_dataAccessClientOPCUA;
};

#endif //__LASERSTARTTHREAD_H__ 
