#ifndef SKELETON_H_
#define SKELETON_H_

#include <iostream>
#include "map"
#include "vector"

class PluginsBase;
class Weatherboard;
class Laser;
class LaserStartThread;


//class skeleton: public PluginsInterface {
class skeleton: public PluginsBase {
public:
	skeleton() {compteurTest=0;};
	int close();
 	int init(const std::string& chaine);
//
//      int init(const std::string& value);

	int cmd(const std::string& chaine, int commandStringAck, std::string& result);
//
//      int cmd(const std::string& value, int commandStringAck, std::string& result);

	// new virtual methods appears with the version 3.0 of MOS
	int afterStart();
        int cmdAsynch(const std::string& command, int commandStringAck, const std::string& datapointName, int nameSpace, std::string& result);
        //int cmdAsynch(const std::string& command, int commandStringAck, const std::string& datapointName, int nameSpace, std::string& result){};
	//int cmdAsynch(std::string command, int commandStringAck, std::string datapointName, int nameSpace, std::string *result) {return 0;};

	// new virtual methods who replace the setAnay getAny methods  with the version 4.0 of MOS
        int get(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue);
        int set(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue);
//
//        int get(const std::string& value,int commandStringAck, std::vector<boost::any>& tabValue);
//        int set(const std::string& value,int commandStringAck, std::vector<boost::any>& tabValue);
private:
	Weatherboard *m_Weatherboard;
        Laser *m_Laser;	
// add 05/04/2017
//        int relayON();
//        int relayOFF();
        int relay(int flag);
        int laser(int flag);
        int laserStart(int freq, float duty,int cycles);
// modifica per skeleton.cpp_old
// ///       int wheel(std::string val1,std::string val2);
//int wheel1(std::string val1);
//int wheel2(std::string val2);
int wheel(std::string val1,std::string val2);
        int adc1(int flag);        
        
        void Setup();
        int m_fd1;
        int m_fd2;
        int m_fd3;
        int m_fd4;
	int compteurTest;
                
        LaserStartThread* m_testThread;

};

typedef skeleton *(*maker_protocole1)();
#endif //  skeleton_H_
