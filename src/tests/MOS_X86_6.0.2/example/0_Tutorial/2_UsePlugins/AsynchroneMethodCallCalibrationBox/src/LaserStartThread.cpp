/******************************************************************************
 **
 **
 ** Copyright (C) LAPP. CNRS
 **
 ** Project: C++ OPCUA generique
 **
 ** Description:
 **
 ** Author : Panazol Jean Luc
 ******************************************************************************/

#include "LaserStartThread.h"
#include "dataAccessClientOPCUA.h"


LaserStartThread::LaserStartThread(DataAccessClientOPCUA* dataAccessClientOPCUA) {
	m_pause = false;
	m_stop = false;
	m_command=0;
	m_cmdStartLaser=0;
	m_cmdStopLaser=0;
	m_dataAccessClientOPCUA = dataAccessClientOPCUA;
}

LaserStartThread::~LaserStartThread() {
	m_pause = true;
	m_stop = true;
	wait();
}

void LaserStartThread::pause() {
	m_pause = true;
}

void LaserStartThread::resume() {
	m_pause = false;
}

int LaserStartThread::stop() {
	int ret=0;
	m_stop = true;
	return ret;
}

int LaserStartThread::cmdStartLaser( std::string datapointName, int nameSpace) {
        int ret=0;
     printf("sonde jl (plugin) LaserStartThread::cmdStartLaser\n"); 
        m_cmdStartLaser = true;
	m_datapointName = datapointName;
	m_nameSpace = nameSpace;
        return ret;
}

int LaserStartThread::cmdStopLaser() {
        int ret=0;
        m_cmdStopLaser = true;
        return ret;
}


void *LaserStartThread::run(void *params) {
// this method run all the time after calling the method startRun() -> in your squeletonPlugin_for_asynchroneMethodCall.cpp file )
// you can stop with m_stop=true with calling the method stop() (when the program finish)
// or make a pause with m_pause=true with calling the methods pause() and resume()
// you implement here your methods who take a long time to execute
// Here an example with to methods closeShutter and openShutter
	std::string temString;
	std::string tempValue;
	int t=0;
	while (m_stop == false) {
		if (m_pause == false) {
			printf("sonde jl ici dans la boucle\n");
			usleep(1000000);
			if(m_cmdStartLaser==1) {
				// inform that the command is in progress 
				temString = m_datapointName + "._InProgressBar";
				t=1;
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,t);
				// inform that the command is done
				temString = m_datapointName + "._InProgress";	
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,true);

				// ****************** here put the code for closeShutter who take a long time to execute ***************

 				// you can put the outputs arguments in this place to inform the server
					temString = m_datapointName + "._OutputArguments._Val_Retour";
					tempValue="laser starting ";
          				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,tempValue);

// here boucle

        int ic;
        int idone=0;
	int cycles=1000;
          				
	temString = "MOS_Server.CalibBox.Temperature_sensor.Temperature_sensor_v";
	int nameSpace=2;
	float valueTp;
        for (ic=0;ic<cycles;++ic) {
		printf("cycles=%d\n",ic);
		usleep(100000);
                ++idone;
		if(ic %10) {
          		m_dataAccessClientOPCUA->getDatapoint(temString,nameSpace,valueTp);
			printf("val=%f\n",valueTp);
	
			if(valueTp>50) {
				printf("Error val=%f>10\n",valueTp);
				// inform that the command is done
				temString = m_datapointName + "._Error";	
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,true);
				// inform that the command is done
				temString = m_datapointName + "._ErrorCode";	
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,2);
				// inform that the command is done
				temString = m_datapointName + "._ErrorMsg";	
				std::string stringMsg = "temperature too high";
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,stringMsg);
				break;
			}
                        if(m_cmdStopLaser==1) {
				temString = m_datapointName + "._Error";	
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,true);
				temString = m_datapointName + "._ErrorCode";	
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,1);
				temString = m_datapointName + "._ErrorMsg";	
				std::string stringMsg = "Operator stop the Laser";
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,stringMsg);
                                m_cmdStopLaser=0;
				break;
			}
		}
	}
					
				// inform that the command is done
				temString = m_datapointName + "._Done";	
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,true);

				// inform that the command is done
				temString = m_datapointName + "._InProgress";	
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,false);

				// inform that the command is not in progress 
				temString = m_datapointName + "._InProgressBar";
				t=0;
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,t);

				// reset the command
                                m_cmdStartLaser=0;
			}

                        if(m_cmdStopLaser==1) {
				// inform that the command is in progress 
				temString = m_datapointName + "._InProgressBar";
				t=1;
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,t);

				// ****************** here put the code for closeShutter who take a long time to execute ***************

 				// you can put the outputs arguments in this place to inform the server
					temString = m_datapointName + "._OutputArguments._Val_Retour";
					tempValue="Laser stoping ";
                                        m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,tempValue);
					
				// inform that the command is done
				temString = m_datapointName + "._Done";	
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,true);

				// inform that the command is not in progress 
				temString = m_datapointName + "._InProgressBar";
				t=0;
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,t);

				// reset the command
                                m_cmdStopLaser=0;
                        }
		}
	}
	return NULL;
}

int  LaserStartThread::startRun() {
	int ret=0;
	start(NULL);
	return ret;
}

