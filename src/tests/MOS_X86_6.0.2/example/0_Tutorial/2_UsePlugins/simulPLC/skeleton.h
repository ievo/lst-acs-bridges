#ifndef SKELETON_H_
#define SKELETON_H_

#include <iostream>
#include "map"
#include "vector"

class PluginsBase;
class Weatherboard;
class Laser;
class LaserStartThread;

//class skeleton: public PluginsInterface {
//

#include "lappThread.h"

class SetDatapointThread: public LAPPThread {
public:
        SetDatapointThread(DataAccessClientOPCUA* dataAccessClientOPCUA,std::string datapointName,int nameSpace) {
		m_dataAccessClientOPCUA = dataAccessClientOPCUA;
		m_datapointName = datapointName;
		m_nameSpace = nameSpace;
		s = "{1,2,3,4,5,6}";
		/*char ch[10];
		for(int i=0; i<300; i++) {
			m_f.push_back(10+i);
			sprintf(ch,"%f",10+i);
			s=s+ch;
			s=s+",";
		} 
		s=s+"}";*/
		printf("s=%s\n",s.c_str());
		//for(int i=0; i<300; i++)
		//	printf(" %f",m_f[i]);
		start(NULL);
	};
        ~SetDatapointThread(){};
        void* run(void *params) {
                        std::string temString = m_datapointName;
			
                        m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,m_f);
	};
private:
        int m_nameSpace;
        std::string m_datapointName;
        DataAccessClientOPCUA* m_dataAccessClientOPCUA;
	std::vector<float> m_f; 
	std::string s; 

};


class skeleton: public PluginsBase {
public:
	skeleton() {};
	int close();
 	int init(const std::string& chaine);
//
//      int init(const std::string& value);

	int cmd(const std::string& chaine, int commandStringAck, std::string& result);
//
//      int cmd(const std::string& value, int commandStringAck, std::string& result);

	// new virtual methods appears with the version 3.0 of MOS
	int afterStart();
        int cmdAsynch(const std::string& command, int commandStringAck, const std::string& datapointName, int nameSpace, std::string& result);
	//int cmdAsynch(std::string command, int commandStringAck, std::string datapointName, int nameSpace, std::string *result) {return 0;};

	// new virtual methods who replace the setAnay getAny methods  with the version 4.0 of MOS
        int get(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue);
        int set(const std::string& chaine,int commandStringAck, std::vector<boost::any>& tabValue);
//
//        int get(const std::string& value,int commandStringAck, std::vector<boost::any>& tabValue);
//        int set(const std::string& value,int commandStringAck, std::vector<boost::any>& tabValue);
private:
	Weatherboard *m_Weatherboard;
        Laser *m_Laser;	
// add 05/04/2017
//        int relayON();
//        int relayOFF();
        int relay(int flag);
        int laser(int flag);
        int laserStart(int freq, float duty,int cycles);
        int lasercheckstatus(int flag);
        char * lasererrorsreg1(int flag);
// modifica per skeleton.cpp_old
// ///       int wheel(std::string val1,std::string val2);
	int setwheel(int value1,int value2);
	int wheel1(int val1);
	int wheel2(int val2);
        int adc1(int flag);        
        
        void Setup();
        int m_fd1;
        int m_fd2;
        int m_fd3;
        int m_fd4;
        int posw1;
        int posw2;
	int relayon;
//	int lstatus;
	LaserStartThread* m_testThread;
};

typedef skeleton *(*maker_protocole1)();
#endif //  skeleton_H_
