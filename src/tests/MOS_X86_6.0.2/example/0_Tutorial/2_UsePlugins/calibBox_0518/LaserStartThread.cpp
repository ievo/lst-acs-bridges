/******************************************************************************
 **
 **
 ** Copyright (C) LAPP. CNRS
 **
 ** Project: C++ OPCUA generique
 **
 ** Description:
 **
 ** Author : Panazol Jean Luc
 ******************************************************************************/

#include "LaserStartThread.h"
#include "dataAccessClientOPCUA.h"
//#include <wiringPi.h>
//#include <wiringSerial.h>
#define PULSE  0  // pin number
#define TTLDAQ 1  // pin long TTL DA

LaserStartThread::LaserStartThread(DataAccessClientOPCUA* dataAccessClientOPCUA) {
	m_pause = false;
	m_stop = false;
	m_command=0;
	m_cmdStartLaser=0;
	m_cmdStopLaser=0;
	m_dataAccessClientOPCUA = dataAccessClientOPCUA;
}

LaserStartThread::~LaserStartThread() {
	m_pause = true;
	m_stop = true;
	wait();
}

void LaserStartThread::pause() {
	m_pause = true;
}

void LaserStartThread::resume() {
	m_pause = false;
}

int LaserStartThread::stop() {
	int ret=0;
	m_stop = true;
	return ret;
}

int LaserStartThread::cmdStartLaser( std::string datapointName, int nameSpace) {
        int ret=0;
     printf("sonde jl (plugin) LaserStartThread::cmdStartLaser\n"); 
        m_cmdStartLaser = true;
	m_datapointName = datapointName;
	m_nameSpace = nameSpace;
        return ret;
}

int LaserStartThread::cmdStopLaser() {
        int ret=0;
        m_cmdStopLaser = true;
        return ret;
}


void *LaserStartThread::run(void *params) {
// this method run all the time after calling the method startRun() -> in your squeletonPlugin_for_asynchroneMethodCall.cpp file )
// you can stop with m_stop=true with calling the method stop() (when the program finish)
// or make a pause with m_pause=true with calling the methods pause() and resume()
// you implement here your methods who take a long time to execute
// Here an example with to methods closeShutter and openShutter
	std::string temString;
	std::string tempValue;
	int t=0;
	while (m_stop == false) {
		if (m_pause == false) {
			printf("sonde jl ici dans la boucle\n");
			usleep(1000000);
			if(m_cmdStartLaser==1) {
				// inform that the command is in progress 
				temString = m_datapointName + "._InProgressBar";
				t=1;
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,t);
				// inform that the command is done
				temString = m_datapointName + "._InProgress";	
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,true);
///program code entire
int freq=1000;
float duty=0.5;
int cycles=10000;
        int uptime = 1000000*(duty/freq);

        printf ("Odroid test with %d cycles at %d Hz on WiringPi pin %3d  - duty cycle = %5.1f \n",cycles,freq,PULSE,duty) ;

        //Initialize the woromgPi ports
        /*if (wiringPiSetup () == -1) {
                printf(" WiringPiSetup error - stop !\n");
                break ;
        }*/


        // set priority of process
        //piHiPri(60) ;
        //sleep (1) ; //?

        //set the PULSE pin as output
        //pinMode (PULSE, OUTPUT);
        //pinMode (TTLDAQ, OUTPUT);
        //printf(" Print TTLDAQ and OUTPUT %5d %5d \n",TTLDAQ,OUTPUT);


        // here send once the TTL to DAQ to Set up
        //digitalWrite (TTLDAQ, HIGH) ;
        //delayMicroseconds (2000) ;
        //printf(" Print TTLDAQ and High %5d %5d \n",TTLDAQ,HIGH);
usleep (10);

        // from rate calculate downtime for cycle
         int totaltime=1000000/freq;
        int downtime = totaltime - uptime;
        float factor=0.97;

        if (freq<=100) {
              factor =0.997;
        }
        else if (freq>100 && freq<=500) {
                factor =0.986;
          }
        else if (freq>500 && freq<=1000) {
               factor =0.976;
        }
        else if (freq>1000 && freq<=1500) {
              factor =0.966;
        }
        else if (freq>1500 && freq<=2000) {
                factor =0.953;
        }
        else {
           factor =0.94;
        }
// microseconds delay - scale by 3.5% because of large offset
        int down = downtime*factor;
        int up = uptime*factor;
        //printf(" Scaled uptime   = %d usec \n",up);
         //printf(" Scaled downtime = %d usec \n",down);
        int ic;
        int idone=0;
//        float valT = m_Weatherboard->getT();
//        float valH = m_Weatherboard->getH();
        for (ic=0;ic<cycles;++ic) {
 			if(m_cmdStopLaser==1) {
                                temString = m_datapointName + "._Error";        
                                m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,true);
                                temString = m_datapointName + "._ErrorCode";    
                                m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,1);
                                temString = m_datapointName + "._ErrorMsg";     
                                std::string stringMsg = "Operator stop the Laser";
                                m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,stringMsg);
                                m_cmdStopLaser=0;
                                break;
                        }
  //         if (valH/1000 < 85 && valT < 45){
                //digitalWrite (PULSE, HIGH) ;        // On
                // microseconds delay
                //printf(" Print PULSE and High %5d %5d \n",PULSE,HIGH);
                //delayMicroseconds (up) ;
                //printf(" Print delay ms  %5d  \n",up);
                //digitalWrite (PULSE, LOW) ; // Off
                //printf(" Print PULSE and low %5d  %5d  \n",PULSE,LOW);
            //microseconds delay - scaled 
                  //  delayMicroseconds (down) ;
                //printf(" Print dealy ms %5d  \n",down);
                    ++idone;
                printf(" Time for %5d loops execution  \n",idone);
               // }
    //         else
      //    printf("DANGER!!! Temperature= %.2f or R.H.=%.2f outside safety boundaries...  \n",valT, valH/1000);
       }

        printf(" Time for %5d loops execution  \n",idone);
          // here send once the TTL to DAQ finish
 //digitalWrite (TTLDAQ, HIGH) ;
   //     printf(" Print TTLDAQ and Low %5d %5d \n",TTLDAQ,HIGH);
     //   delayMicroseconds (2000) ;
       //  digitalWrite (TTLDAQ, LOW);
       // printf(" Print TTLDAQ and Low %5d %5d \n",TTLDAQ,LOW);
				// ****************** here put the code for closeShutter who take a long time to execute ***************

 				// you can put the outputs arguments in this place to inform the server
					temString = m_datapointName + "._OutputArguments._Val_Retour";
					tempValue="laser starting ";
          				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,tempValue);

				// inform that the command is done
				temString = m_datapointName + "._Done";	
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,true);

				// inform that the command is done
				temString = m_datapointName + "._InProgress";	
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,false);

				// inform that the command is not in progress 
				temString = m_datapointName + "._InProgressBar";
				t=0;
				m_dataAccessClientOPCUA->setDatapoint(temString,m_nameSpace,t);

				// reset the command
                                m_cmdStartLaser=0;
			}
		}
	}
	return NULL;
}

int  LaserStartThread::startRun() {
	int ret=0;
	start(NULL);
	return ret;
}

