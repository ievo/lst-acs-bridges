#include "pluginsBase.h"
//#include "mcp3008Spi.h"
#include <errno.h> 
#include <fstream>
#include <sys/time.h>
#include <ctime>
#include <iostream>
//#include "weatherboard.h"
//#include "Laser.h"
#include "skeleton.h"
#include "LaserStartThread.h"
#include <stdio.h>
#include <stdint.h> 
#include <stdlib.h> 
#include <cstring>
#include <errno.h> //error output
#include <string>
#include <cstdlib>
//#include <wiringPi.h>
//#include <wiringSerial.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h> 
#include <ctype.h>
//#define CYCLES  100000  // number of cycles to do
//#define dutycycle 0.1 // duty  cycle defined here (just modify 0.5 to have different duty cicly, range 0.1-0.9)
#define PULSE  0  // pin number
#define TTLDAQ 1  // pin long TTL DA
#define TTLREL 4 // pin for TTL relay SiPM
using namespace std;



int skeleton::afterStart() {
        int ret=0;
        ret = PluginsBase::afterStart();

        std::string resultCall;
        m_testThread = new LaserStartThread(getDataAccessClientOPCUARef());
        ret = m_testThread->startRun();
        return ret;
}



int skeleton::cmdAsynch(const std::string& command, int commandStringAck, const std::string& datapointName, int nameSpace, std::string& result) {
        int ret=0;
        result ="";
     printf("sonde jl skeleton::cmdAsynch %s\n",command.c_str());
        if(command.compare("start")==0) {
                 m_testThread->cmdStartLaser(datapointName,nameSpace);
        }
        if(command.find("SetLaser") != command.npos) {
                std::string value= command;
                unsigned long index =value.find(" ");
                if(index != value.npos ) {
                        std::string value1 = value.substr(index+1,value.size()-index-1);
                        printf("setLaser cmd choosen is %s \n",value1.c_str());
			int cmd=atoi(value1.c_str());
			if(cmd<2) {
                        	ret = laser(atoi(value1.c_str()));
        			SetDatapointThread *m_SetDatapointThread= new SetDatapointThread(getDataAccessClientOPCUARef(),datapointName,nameSpace);

			}
			else {
                 		m_testThread->cmdStartLaser(datapointName,nameSpace);
			}
		}
	}
        return ret;
}



int skeleton::get(const std::string& chaine, int commandStringAck, std::vector<boost::any>&tabValue) {
        int ret = 0;
        char buf[1024];
        printf("\n(Plugin) :  skeleton::get(): %s\n",chaine.c_str());
	DataAccessClientOPCUA* my_ref=getDataAccessClientOPCUARef();
		sprintf(buf,"2"); // for test
	if(chaine.compare("getT") == 0) {
        	float valT;// = m_Weatherboard->getT();
        	if (valT>25) m_testThread->cmdStopLaser();
                printf("skeleton::get() ValT=%.2f\n",valT);
		sprintf(buf,"%.2f",valT); // for test
	}
        if(chaine.compare("getH") == 0) {
                float valH; // = m_Weatherboard->getH();
                printf("skeleton::get() ValH=%.2f\n",valH/1000);
                sprintf(buf,"%.2f",valH/1000); // for test^M
        }
        if(chaine.compare("getP") == 0) {
                float valP; // = m_Weatherboard->getP();
                printf("skeleton::get() ValP=%.2f\n",valP/100.0);
                sprintf(buf,"%.2f",valP/100.0); // for test^M
        }

        if(chaine.find("WheelPosition") == 0) {
               std::string value= chaine;
               std::string value1;
               std::string value2;
               unsigned long index =value.find(" ");
               if(index != value.npos ) {
                       value1 = value.substr(index+1,1);
                        printf("wheel1 position choosen is %s \n",value1.c_str());
                        int index = atoi(value1.c_str());
                        if (index == 1){
        		       printf("skeleton::get() posw1=%d\n",posw1);
	                	sprintf(buf,"%d",posw1); // for test^M
 
                        }
			if (index == 2){
                               printf("skeleton::get() posw2=%d\n",posw2);
                                sprintf(buf,"%d",posw2); // for test^M

                        }
       		}
        }
        if(chaine.find("lasercheckstatus") == 0) {
                ret=lasercheckstatus(0);
                printf("skeleton::get() Laser Status = %d\n", ret);
                sprintf(buf,"%d",ret); // for test^M
        }
        if(chaine.find("lasererrorsreg1") == 0) {
         //       char erreg1[10];
         //       int eret[10] = { 0 };
              char* erreg1=lasererrorsreg1(0);
          //      strcpy( erreg1,eret );
             //  char erreg1[6]="ciaon";
                
                printf("skeleton::get() Laser Errors Reg1 = %s\n", erreg1);
                sprintf(buf,"%s",erreg1); // for test^M
        }
/*
      if(chaine.find("RelayStatus") == 0) {
                relayon=0;
                printf("skeleton::get() Relay Status = %d\n",relayon);
                sprintf(buf,"%d",relayon); // for test^M
        }*/
        	if (int *pi = boost::any_cast <int> (&(tabValue)[0])) {
				*pi=atoi(buf);
		}else if (short *pi = boost::any_cast <short> (&(tabValue)[0])){
				*pi=atoi(buf);
		}else if (long *pi = boost::any_cast <long> (&(tabValue)[0])){
				*pi=atol(buf);
		}else if (double *pi = boost::any_cast <double> (&(tabValue)[0])){
				*pi=atof(buf);
		}else if (float *pi = boost::any_cast <float> (&(tabValue)[0])){
	                        *pi =  atof(buf);
		}else if (bool *pi = boost::any_cast <bool> (&(tabValue)[0])){
	                        *pi =  atoi(buf);
		}else if(string *pstr = boost::any_cast<string>(&(tabValue)[0])){
	                        *pstr = buf;
		}
        return ret;
}
// add 05/04/2017 
void skeleton::Setup(){
// configuration for the wheel1 device
  char device1[]= "/dev/ttyUSB2"; // wheel2 connected to USB3 
  unsigned long baud1 = 115200;

// configuration for the relay evice
  char device2[]= "/dev/ttyUSB0"; // relay connected to USB0 
  unsigned long baud2 = 9600;

// Laser ON/OFF
// configuration for the Laser evice
  char device3[]= "/dev/ttyUSB1"; // relay connected to USB0 
  unsigned long baud3 = 19200;

// configuration for the wheel1 device
  char device4[]= "/dev/ttyUSB3"; // wheel1 connected to USB3
  unsigned long baud4 = 115200;

  printf("%s \n", "Rasp Startup!");
//  fflush(stdout);

  // comment for the test 
  /*
// Open the USB device wheel 2
m_fd1=serialOpen(device1,baud1); 
// Open the USB device relay 
m_fd2=serialOpen(device2,baud2); 
// Open The USB device Laser
m_fd3=serialOpen(device3,baud3);
// Open The USB device Wheel 1
m_fd4=serialOpen(device4,baud4);
*/
/*
  printf("m_fd2 response = %d \n",m_fd2);
  if (m_fd2 < 0){
    printf ("Unable to open device /dev/ttyUSB0 (Relay): %s\n", strerror (error)) ;
    exit(1); //error
  }
*/
};

int skeleton::setwheel(int value1, int value2){
                        char pstring[5]="pos=";
                        char cr[2]="\r";
                        char buffer[8];
                         strcpy(buffer,pstring);
                         sprintf(buffer,"%s%d",buffer,value1);                         
                         strcat(buffer,cr);
                        //       here send buffer by usb to device
                         printf("buffer --> %s\n",buffer);
                         //serialPuts (m_fd4,buffer);
//              
                         strcpy(buffer,pstring);
                         sprintf(buffer,"%s%d",buffer,value2);
                         strcat(buffer,cr);
                        //       here send buffer by usb to device
                         printf("buffer --> %s\n",buffer);
                         //serialPuts (m_fd1,buffer);
                         return 0;
};



int skeleton::wheel1(int value1) {
                        char pstring[5]="pos=";
                        char cr[2]="\r";
                        char buffer[8];
                         strcpy(buffer,pstring);
                         sprintf(buffer,"%s%d",buffer,value1);
                         strcat(buffer,cr);
                        //       here send buffer by usb to device
                         printf("buffer --> %s\n",buffer);
                         //serialPuts (m_fd4,buffer);
                         return 0;
};
int skeleton::wheel2(int value2) {
                        char pstring[5]="pos=";
                        char cr[2]="\r";
                        char buffer[8];
                         strcpy(buffer,pstring);
			sprintf(buffer,"%s%d",buffer,value2);                         
			strcat(buffer,cr);
                        //       here send buffer by usb to device
                         printf("buffer --> %s\n",buffer);
                         //serialPuts (m_fd1,buffer);
                         return 0;
};



int skeleton::adc1(int flag) {
/*
 if(flag==0) {
int ret=0;
   mcp3008Spi a2d("/dev/spidev0.0", SPI_MODE_0, 1000000, 8);
    int max = 1000000;
        int a2dVal = 0;
    int a2dChannel = 0;
        unsigned char data[3];
        ofstream outputFile("/home/odroid/Desktop/spi-adc/mcp3008/prove_opc/TestADCTime.dat");
timeval start, stop;
double elapsedTime;
for (int i=1;i<max; i++)
    {
gettimeofday(&start, NULL);
        data[0] = 1;  //  first byte transmitted -> start bit
        data[1] = 0b10000000 |( ((a2dChannel & 7) << 4)); // second byte transmitted -> (SGL/DIF = 1, D2=D1=D0=0)
        data[2] = 0; // third byte transmitted....don't care
     // sleep(1);
        a2d.spiWriteRead(data, sizeof(data) );

        a2dVal = 0;
                a2dVal = (data[1]<< 8) & 0b1100000000; //merge data[1] & data[2] to get result
                a2dVal |=  (data[2] & 0xff);
                if (a2dVal>2)
                  {
        outputFile <<i<<" "<< a2dVal<<" "<< elapsedTime<<endl;
gettimeofday(&stop, NULL);
elapsedTime = (stop.tv_usec - start.tv_usec);
i++;              }
  //      i--;
    }
//auto end = std::chrono::steady_clock::now();
//auto elapse = std::chrono::duration_cast<std::chrono::microseconds>(end-start);
    outputFile.close();
    return ret;
};
*/
};

int skeleton::laser(int flag) {
                std::string realInstruction;
		int ret=0;
                if(flag==2) {
                  float duty=0.5;
                  int freq = 1000;
                  int cycles= 10000;
                  ret= laserStart(freq,duty,cycles);
                 }
                else{
                // and parse the argument of the instruction 
		if(flag==0) {
               		realInstruction = ("SSSD 0\r");
		}
		if(flag==1) {
               		realInstruction = ("SSSD 1\r");
		}
                usleep(1000);
                printf("Laser instruction is %s \n",realInstruction.c_str());
                usleep(1000);
                //serialPuts(m_fd3,realInstruction.c_str());
                usleep(1000);
//                serialClose(m_fd3);
		}		
		return ret;
};

char * skeleton::lasererrorsreg1(int flag){
unsigned char newStr;

    char allchar[36];
    int iplace=0;
    int imax = 24;
    char newChar;
/*
if (flag==0) {
    usleep(1000);

    serialPuts(m_fd3,"GSER\r");

    usleep(50000); //waiting time, serial bus is slow

        // here get response    

    for (int ic=0;ic<imax;ic++) {
     if(serialDataAvail(m_fd3)) {
        newChar = serialGetchar(m_fd3);
//       printf("%d %x \n",iplace,newChar);
        allchar[iplace]=newChar;
        iplace++;
      fflush(stdout);
     } else {
        break;
     }
    }
if(iplace>imax) iplace=imax;

//printf("newStr=%s\n", newStr);
if(char *substr = strchr(allchar, 'R') ){
int substringLength = 9; // 13-4=9 (bigger string size) - (new string size) 
printf("substr=%s\n", substr+2);
printf("allchar=%s\n", allchar);
//printf("largeSrt=%s\n", largeSrt);
char newStr[9];
//char *newStr = malloc(sizeof(char) * substringLength + 1);// keep memory free to new string
//char newStr=sizeof(char) * substringLength + 1;
strncpy(newStr, substr+2,substringLength);  // copy only 9 characters 
newStr[substringLength] = '\0'; // close the new string with final character

printf(" \n");

 usleep(5000);

        printf(" Laser Error Reg1  = %s \n",newStr);
        // and close laser serial

        usleep(1000);
 //       serialClose(fdl);
return newStr;
} else{
        return "Not Reg1 ready";
}
}
*/
}
int skeleton::lasercheckstatus(int flag){
unsigned int lstatus;
   // here read response
    char allchar[10];
    int iplace=0;
    int imax = 8;
    char newChar;
/*
	if (flag==0) {
    	usleep(10000);

    	serialPuts(m_fd3,"GSSD\r");

    	usleep(500000); //waiting time, serial bus is slow

        // here get response    

    		for (int ic=0;ic<imax;ic++) {
     			if(serialDataAvail(m_fd3)) {
        		newChar = serialGetchar(m_fd3);
//       printf("%d %x \n",iplace,newChar);
        		allchar[iplace]=newChar;
        		iplace++;
		        fflush(stdout);
     			} else {
        		break;
    	 		}
    		}

    	if(iplace>imax) iplace=imax;
*/

/*
	for (int ik=0;ik<iplace;ik++) {
        printf(" %d %c \n",ik,allchar[ik]);
   	}
*/
/*
	int i;
	int strLength = strlen(allchar);
	int resultPosCtr = 0;
	char varres[10];

	for(i = 0; i < strLength; i++){
    		if(isdigit(allchar[i])){
         	varres[resultPosCtr] = allchar[i];
         	resultPosCtr++;
    		}
	}
	varres[resultPosCtr++] = '\0';
        int lstatus = atoi(varres);
    	usleep(5000);
       // printf(" Laser status at %s \n",dt);
        printf("Status Interno  = %d \n",lstatus);
        // and close laser serial
       	usleep(1000);
//        serialClose(m_fd3);
	return lstatus;
     	}
*/
}

int skeleton::laserStart(int freq, float duty, int cycles){
	int uptime = 1000000*(duty/freq);

 	printf ("Odroid test with %d cycles at %d Hz on WiringPi pin %3d  - duty cycle = %5.1f \n",cycles,freq,PULSE,duty) ;
/*
	//Initialize the woromgPi ports
	if (wiringPiSetup () == -1) {
  		printf(" WiringPiSetup error - stop !\n");
  		return 1 ;
  	}


	// set priority of process
 	piHiPri(60) ;
 	//sleep (1) ; //?

 	//set the PULSE pin as output
 	pinMode (PULSE, OUTPUT);
 	pinMode (TTLDAQ, OUTPUT);
	printf(" Print TTLDAQ and OUTPUT %5d %5d \n",TTLDAQ,OUTPUT);


 	// here send once the TTL to DAQ to Set up
 	digitalWrite (TTLDAQ, HIGH) ;
 	delayMicroseconds (2000) ;
	printf(" Print TTLDAQ and High %5d %5d \n",TTLDAQ,HIGH);
 	digitalWrite (TTLDAQ, LOW);
	printf(" Print TTLDAQ and Low %5d %5d \n",TTLDAQ,LOW);

 	usleep (10);

 	// from rate calculate downtime for cycle
	 int totaltime=1000000/freq;
 	int downtime = totaltime - uptime;
 	float factor=0.97;

  	if (freq<=100) {
  	      factor =0.997;
  	}
  	else if (freq>100 && freq<=500) {
     		factor =0.986;
          }
 	else if (freq>500 && freq<=1000) {
 	       factor =0.976;
  	}
  	else if (freq>1000 && freq<=1500) {
  	      factor =0.966;
  	}
  	else if (freq>1500 && freq<=2000) {
        	factor =0.953;
  	}
  	else {
  	   factor =0.94;
 	}

 	// microseconds delay - scale by 3.5% because of large offset
 	int down = downtime*factor;
 	int up = uptime*factor;
 	//printf(" Scaled uptime   = %d usec \n",up);
	 //printf(" Scaled downtime = %d usec \n",down);
 	int ic;
 	int idone=0;
        float valT = m_Weatherboard->getT();
        float valH = m_Weatherboard->getH();
 	for (ic=0;ic<cycles;++ic) {
           if (valH/1000 < 85 && valT < 45){
    		digitalWrite (PULSE, HIGH) ;        // On
    		// microseconds delay
		printf(" Print PULSE and High %5d %5d \n",PULSE,HIGH);
		delayMicroseconds (up) ;
		printf(" Print delay ms  %5d  \n",up);
	    	digitalWrite (PULSE, LOW) ; // Off
		printf(" Print PULSE and low %5d  %5d  \n",PULSE,LOW);
	    //microseconds delay - scaled 
		    delayMicroseconds (down) ;
		printf(" Print dealy ms %5d  \n",down);
		    ++idone;
		printf(" Time for %5d loops execution  \n",idone);
               	}
             else
          printf("DANGER!!! Temperature= %.2f or R.H.=%.2f outside safety boundaries...  \n",valT, valH/1000);
         }  

	printf(" Time for %5d loops execution  \n",idone);
	  // here send once the TTL to DAQ finish
	 digitalWrite (TTLDAQ, HIGH) ;
	printf(" Print TTLDAQ and Low %5d %5d \n",TTLDAQ,HIGH);
 	delayMicroseconds (2000) ;
	 digitalWrite (TTLDAQ, LOW);
	printf(" Print TTLDAQ and Low %5d %5d \n",TTLDAQ,LOW);
        return 0;
*/
};


int skeleton::relay(int flag) {
		//relayon = flag;
		/*
              	printf("DEbug %d",relayon);
		std::string realInstruction = ("\xFF\x01");  
              	serialPuts (m_fd2,realInstruction.c_str());
		if(flag==1){
              		serialPutchar (m_fd2,'\x01');
		relayon = 1;
   		}
                if(flag==0) {
              		serialPutchar (m_fd2,'\x00');
		relayon = 0;
		}*/
                return 0;
 };


int skeleton::cmd(const std::string& chaine, int commandStringAck, std::string& result) {

	int ret = 0;
	char buf[1024];
        printf("\n(Plugin) : skeleton::cmd()2: %s\n",chaine.c_str());
	sprintf(buf,"for test");
        float duty;
        int cycles;
        int freq;

// add 05/04/2017 
// parse the instruction
 if(chaine.find("setWheel") != chaine.npos) {
                // and parse the argument of the instruction 
                std::string value= chaine;
               std::string value1;
               std::string value2;
                unsigned long index =value.find(" ");
               if(index != value.npos ) {
                       value1 = value.substr(index+1,value.size()-index-3);
                        printf("wheel1 position choosen is %s \n",value1.c_str());
}
              if(index != value.npos ) {
                       value2 = value.substr(index+3,value.size()-index);
                        printf("wheel2 position choosen is %s \n",value2.c_str());
}

                    ret= wheel1(atoi(value1.c_str()));
                    ret= wheel2(atoi(value2.c_str()));
                    posw1 = atoi(value1.c_str());
                    posw2 = atoi(value2.c_str());        
}
        // parse the instruction
        if(chaine.find("Configure") != chaine.npos) {

/*               if (wiringPiSetup () == -1) {
                printf(" WiringPiSetup error - stop !\n");
                return 1 ;
               }
*/
	       std::string value= chaine;
               std::string value1;
               std::string value2;
               unsigned long index =value.find(" ");
               if(index != value.npos ) {
                       value1 = value.substr(index+1,value.size()-index-3);
                        printf("wheel1 position choosen is %s \n",value1.c_str());
} 
              if(index != value.npos ) {
                       value2 = value.substr(index+3,value.size()-index);
                        printf("wheel2 position choosen is %s \n",value2.c_str());
}
	        ret =relay(1);
//system("cd /home/odroid/; source relay_on.sh");
//                usleep (5000000);
///system("cd /home/odroid/Desktop/utils/; ./w12_init");
                float valH; // = m_Weatherboard->getH();
                printf("Value RH ok? --> skeleton::get() ValH=%.2f\n",valH/1000);
                float valT; // = m_Weatherboard->getT();
                printf("Value Temp ok? --> skeleton::get() Value T=%.2f\n",valT);
                if (valH/1000 < 85 && valT < 45){
		    if (!ret) ret= laser(1);
//                usleep (10000000);
		    if (!ret) ret= wheel1(atoi(value1.c_str()));
                    if (!ret) ret= wheel2(atoi(value2.c_str()));
                  // //    if (value1+value2=8){
                     //set the SiPM relay on
              ////         printf(" Rel SiPM on ... confgiguration whells %s %s \n",value1,value2);
                       //pinMode (TTLREL, OUTPUT);
                       //printf(" Print TTLREL and OUTPUT %5d \n",OUTPUT);
                     // here send once the TTL to DAQ to Set up
                       //digitalWrite (TTLREL, HIGH) ;
                       //printf(" Print TTLREL and High %5d \n",TTLREL,HIGH);
//                       delay (100000) ;
////                       digitalWrite (TTLREL, LOW);
//                       delay(1000);
///                       printf(" Print TTLREL and Low %5d %5d \n",TTLREL,LOW);
                     // }
              }
                else
             printf("DANGER!!!! STOP Configuration!! --> T=%.2f\n and R.H.=%.2f\n",valT,valH);
        }
        if(chaine.find("Start") != chaine.npos) {
        system("modprobe spicc");
        system("cd /home/odroid/Desktop/spi-adc/mcp3008/; ./OutBinTime2GE2 &");
        ret= laserStart(1000,0.5,10000);
//        ret= adc1(0);
          //   }
       }
       if(chaine.find("RelayOn") != chaine.npos) {
		printf("Debug found Relayon_Instr");	
		ret=relay(1);
               // relayON();
        } 
        if(chaine.find("Relay") != chaine.npos) {
                  printf("%s debug",chaine.c_str());
                               std::string value= chaine;
                unsigned long index =value.find(" ");
                if(index != value.npos ) {
                        std::string value1 = value.substr(index+1,value.size()-index-1);
                        // print the result the value of the argument of the instruction is
                        printf("Set relay cmd choosen is %s \n",value1.c_str());
                        ret = relay(atoi(value1.c_str()));

                }
               // relayON();
        }
        if(chaine.find("RelayOff") != chaine.npos) {
		printf("Debug found RelayOff_Instr");
		ret=relay(0);
//                relayOFF();
	}
        if(chaine.find("LaserArm") != chaine.npos) {
		ret =laser(1);
        }
	if(chaine.find("LaserDisArm") != chaine.npos) {
		m_testThread->cmdStopLaser();
		ret =laser(0);
 		ret =0;
	}
//////
        if(chaine.find("SetLaser") != chaine.npos) {
                std::string value= chaine;
                unsigned long index =value.find(" ");
                if(index != value.npos ) {
                        std::string value1 = value.substr(index+1,value.size()-index-1);
                        // print the result the value of the argument of the instruction is
                        printf("setLaser cmd choosen is %s \n",value1.c_str());
                        ret = laser(atoi(value1.c_str()));

                }

        }
        result = buf;
	return ret;
};


int skeleton::set(const std::string& value,int commandStringAck, std::vector<boost::any>& tabValue) {

        
        int ret = 0;
        char buf[1024];
        std::string valueFinal;
 		
                if (int *pi = boost::any_cast <int> (&tabValue[0]))
                         sprintf(buf,"%d",*pi);
                else if (short *pi = boost::any_cast <short> (&tabValue[0]))
                         sprintf(buf,"%d",*pi);
                else if (long *pi = boost::any_cast <long> (&tabValue[0]))
                         sprintf(buf,"%ld",*pi);
                else if (double *pi = boost::any_cast <double> (&tabValue[0]))
                         sprintf(buf,"%f",*pi);
                else if (float *pi = boost::any_cast <float> (&tabValue[0]))
                         sprintf(buf,"%f",*pi);
                else if (bool *pi = boost::any_cast <bool> (&tabValue[0]))
                         sprintf(buf,"%d",*pi);
                else if(string *pstr = boost::any_cast<string>(&tabValue[0]))
                         sprintf(buf,"%s",pstr->c_str());
/* debug JL yes */ 
       //050416 printf("\n(Plugin) : skeleton::set(): %s\n",buf);
	
	if(value.compare("setL") == 0) {
          printf("\n(Plugin) : skeleton::set(): %s\n",buf);
//        printf(" (Plugin)  : skeleton set() with -> %s\n",buf);
        //050416	m_Laser->setL(atof(buf));
	}
/* end debug JL yes */ 
        return ret;
}

int skeleton::close() {
        printf("\n(Plugin) : skeleton::close() : \n");
	return 0;
};

/*int skeleton::afterStart() {
	int ret=0;
// Mandatory allways need
	PluginsBase::afterStart();
	return ret;
};*/
// Metti tutto cio che serve come inizializzazione..metto le wheels in 4 quando il MOs è lanciato
int skeleton::init(const std::string& chaine) {
// Mandatory allways need
        int ret=0;
	PluginsBase::init(chaine);
	m_Weatherboard=NULL;
        //m_Weatherboard = new Weatherboard();
        m_Laser=NULL;
        //m_Laser = new Laser();
        Setup();
// add 05/04/2017
//initial position of the wheels, Laser status, poweron rlgpio on/off etc 
        posw1 = 4;
        posw2 = 4;
     //   ret = setwheel(posw1,posw2);     
        ret = wheel1(posw1);
        ret = wheel2(posw2);
   // confguration rel_gpio off during the initialization
        //pinMode (TTLREL, OUTPUT);
        //printf(" Print OUTPUT %5d \n",OUTPUT);
        //digitalWrite (TTLREL, LOW );
        //printf(" Print TTLREL %5d and LOW %5d \n",TTLREL,LOW);
 // configuration laser status off during the initialization
//        ret = laser(0);
//aggiungi da programma status hardware check...per il print
        //ret = lasercheckstatus(0);
  //      printf(" Print status %5d \n",ret);
       printf("\n(Plugin) : skeleton::init() : %s\n",chaine.c_str());
        return ret;
};

extern "C" {
skeleton *ptr_Plugin() {
	return new skeleton();
}
}

