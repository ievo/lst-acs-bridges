#ifndef SETASYNCHRONE_H_
#define SETASYNCHRONE_H_

#include <iostream>
#include <boost/any.hpp>

class DataAccessClientOPCUA;
class SetAsynchrone
{
public:
    SetAsynchrone(DataAccessClientOPCUA* ref,const std::string& object, int nameSpace,boost::any element) {
	m_ref = ref;
	m_object = object;
	m_namespace = nameSpace;
	m_value = element; 
	start(NULL);
	};
private:
    DataAccessClientOPCUA* m_ref;
    std::string m_object;
    int m_namespace;
    boost::any m_value;

    void *run(void *params){
               if (int *pi = boost::any_cast <int> (&m_value))
			m_ref->setDatapoint(m_object,m_namespace,*pi);
                else if (short *pi = boost::any_cast <short> (&m_value))
			m_ref->setDatapoint(m_object,m_namespace,*pi);
                else if (long *pi = boost::any_cast <long> (&m_value))
			m_ref->setDatapoint(m_object,m_namespace,*pi);
                else if (double *pi = boost::any_cast <double> (&m_value))
			m_ref->setDatapoint(m_object,m_namespace,*pi);
                else if (float *pi = boost::any_cast <float> (&m_value))
			m_ref->setDatapoint(m_object,m_namespace,*pi);
                else if (bool *pi = boost::any_cast <bool> (&m_value))
			m_ref->setDatapoint(m_object,m_namespace,*pi);
                else if(string *pstr = boost::any_cast<string> (&m_value))
			m_ref->setDatapoint(m_object,m_namespace,*pstr);
	return NULL;
    };
    void start(void * params)
    {
        this->params = params;
        pthread_create(&threadId, 0, &SetAsynchrone::static_run, this);
    }

    static void* static_run(void * void_this)
    {
         SetAsynchrone *thread_this = static_cast<SetAsynchrone*>(void_this);
         return thread_this->run(thread_this->params);
    }
    void wait(){
	pthread_join (threadId, NULL);
	}
    pthread_t threadId;
    //LAPPThread * thread_this;
    void *params;
};



#endif //  LAPPTHREAD_H_

