#ifndef CLIENT_OPCUA_H
#define CLIENT_OPCUA_H
#include "uabase.h"
#include "uaclientsdk.h"
#include "uaargument.h"
#include "callbackPluginManager.h"
#include "callbackPlugin.h"


class CB_changeStatus;

using namespace UaClientSdk;

class Client_opcua : public UaSessionCallback
{
    UA_DISABLE_COPY(Client_opcua);
public:
    Client_opcua(CB_changeStatus* CB_changeStatus);

    virtual ~Client_opcua();

    // UaSessionCallback implementation ----------------------------------------------------
    virtual void connectionStatusChanged(OpcUa_UInt32 clientConnectionId, UaClient::ServerStatus serverStatus);
    // UaSessionCallback implementation ------------------------------------------------------

    // OPC UA service calls
    UaStatus connect(const std::string& serverUrl);
    UaStatus disconnect();
    void subscribe(CallbackPluginManager* g_pCallback,UaObjectArray<UaNodeId>  *g_VariableNodeIds);
    UaStatus callMethod(const std::string& method, int nameSpace, CallIn& callRequest,CallOut& resultCall);
    UaStatus getDatapoint(const std::string& object,int nameSpace, UaVariant& resultElement);
    int getTypeDatapoint(const std::string& object,int nameSpace);
    UaStatus setDatapoint(const std::string& object,int nameSpace, UaVariant element);


private:
    UaSession*              m_pSession;
    UaClient::ServerStatus  m_serverStatus;
    void getMethodArguments(const UaNodeId& methodId, UaArguments& inputArguments, UaArguments& outputArguments);
    int m_ClientHandle;
    CB_changeStatus* m_CB_changeStatus;

};

#endif // NAMESERVEROPCUACLIENT_H

