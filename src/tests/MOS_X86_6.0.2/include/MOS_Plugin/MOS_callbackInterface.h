#ifndef MOSCALLBACKPLGIN_H
#define MOSCALLBACKPLGIN_H


#include <vector>
#include <string>

class MOS_CallbackInterface
{
public:
    MOS_CallbackInterface(){};
    virtual void infoDebug()=0;
    virtual void  dataChange(std::vector<std::string> listElements, std::vector<std::string> listValues)=0;
    virtual void abort()=0;
};

#endif
