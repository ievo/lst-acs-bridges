#ifndef CALLBACKPLGINMANAGER_H
#define CALLBACKPLGINMANAGER_H


#include <vector>
#include <list>
#include <string>

#include "MOS_callbackInterface.h"

#include "uaclientsdk.h"
#include "uasession.h"
using namespace UaClientSdk;



   typedef std::vector<std::string> myiterator;

class CallbackPluginManager:
    public UaSessionCallback,
    public UaSubscriptionCallback
{
  private :
	std::vector<MOS_CallbackInterface*> m_MOS_callback;
	std::vector<myiterator> m_elements;
	//myiterator m_elementsTotal;
	std::vector<std::string> m_elementsTotal;
public:
    CallbackPluginManager() {
    };
	void testJL() {
	}

    ~CallbackPluginManager(){
	for (std::vector<MOS_CallbackInterface*>::iterator it = m_MOS_callback.begin(); it != m_MOS_callback.end();it++) delete (*it);
    };

    int addCallback(std::vector<std::string> l_elements,std::vector<int> l_namespace,MOS_CallbackInterface* MOS_callback) {

	//printf("sonde jl CallbackPluginManager::addCallback() avec \n");
	m_elements.push_back(l_elements);
	m_MOS_callback.push_back(MOS_callback);
	for (std::vector<std::string>::iterator it = l_elements.begin(); it != l_elements.end();it++) {
		int flag=0;
		int i;
        	for (i=0; i<m_elementsTotal.size(); i++) {
			if( m_elementsTotal[i].find(it->c_str())==0) { 
				flag =1;
				//printf("sonde jl ic on trouve deja %s flag=%d\n",it->c_str(),flag);
			}
		}
				//printf("sonde jl ic on trouve fin de boucle flag=%d\n",flag);

		if(flag==0) 
			m_elementsTotal.push_back(it->c_str());
		//printf("sonde jl CallbackPluginManager::addCallback() avec m_elementsTotal=%s\n",it->c_str());
			//	printf("sonde jl ic on trouve ,m_elementsTotal.size()=%d\n",m_elementsTotal.size());
	}
				//printf("sonde jl ic on trouve ,m_elementsTotal.size()=%d fin de boucle addCallback\n",m_elementsTotal.size());
	
    };


    virtual void dataChange(
        OpcUa_UInt32               /*clientSubscriptionHandle*/,
        const UaDataNotifications& dataNotifications,
        const UaDiagnosticInfos&   /*diagnosticInfos*/) {
        OpcUa_UInt32 i = 0;
        std::vector<std::string> listElements;
        std::vector<std::string> listValues;

        for ( i=0; i<dataNotifications.length(); i++ )
        {
		/*printf("sonde jl ici dckPluginManager::dataChange ClientHandle=%d\n",dataNotifications[i].ClientHandle-1);
		printf("sonde jl ici dckPluginManager::dataChange element=%s\n",m_elementsTotal[dataNotifications[i].ClientHandle-1].c_str());*/
           if ( OpcUa_IsGood(dataNotifications[i].Value.StatusCode) )
            {
                UaVariant tempValue = dataNotifications[i].Value.Value;
                if(tempValue.isArray()) {
			int typeInt;
                	UaVariant val2;
			int nbIndex = tempValue.arraySize();
			std::string tabConvertString="{";
			typeInt = atoi(tempValue.dataType().toString().toUtf8());
			if (typeInt == OpcUaId_Double) {
	                        UaDoubleArray valDoubleTab;
                                tempValue.toDoubleArray(valDoubleTab);
                                for (int i = 0; i < nbIndex; i++) {
                                	val2 = valDoubleTab[i];
					tabConvertString  += val2.toString().toUtf8();
					tabConvertString += ",";
                                }
				tabConvertString += "}";
                        }
                        if (typeInt == OpcUaId_Float) {
                                UaFloatArray valFloatTab;
                                tempValue.toFloatArray(valFloatTab);
                                for (int i = 0; i < nbIndex; i++) {
                                        val2 = valFloatTab[i];
                                        tabConvertString  += val2.toString().toUtf8();
                                        tabConvertString += ",";
                                }
                                tabConvertString += "}";
                        }
			if (typeInt == OpcUaId_Int64) {
	                        UaInt64Array valInt64Tab;
                                tempValue.toInt64Array(valInt64Tab);
                                for (int i = 0; i < nbIndex; i++) {
                                	val2 = valInt64Tab[i];
					tabConvertString  += val2.toString().toUtf8();
					tabConvertString += ",";
                                }
				tabConvertString += "}";
                        }
			if (typeInt == OpcUaId_Int32) {
	                        UaInt32Array valInt32Tab;
                                tempValue.toInt32Array(valInt32Tab);
                                for (int i = 0; i < nbIndex; i++) {
                                	val2 = valInt32Tab[i];
					tabConvertString  += val2.toString().toUtf8();
					tabConvertString += ",";
                                }
				tabConvertString += "}";
                        }
			if (typeInt == OpcUaId_Int16) {
	                        UaInt16Array valInt16Tab;
                                tempValue.toInt16Array(valInt16Tab);
                                for (int i = 0; i < nbIndex; i++) {
                                	val2 = valInt16Tab[i];
					tabConvertString  += val2.toString().toUtf8();
					tabConvertString += ",";
                                }
				tabConvertString += "}";
                        }
			if (typeInt == OpcUaId_Byte) {
	                        UaByteArray valByteTab;
                                tempValue.toByteArray(valByteTab);
                                for (int i = 0; i < nbIndex; i++) {
                                	val2 = valByteTab[i];
					tabConvertString  += val2.toString().toUtf8();
					tabConvertString += ",";
                                }
				tabConvertString += "}";
                        }
                        if (typeInt == OpcUaId_Boolean) {
                                UaBooleanArray valBooleanTab;
                                tempValue.toBoolArray(valBooleanTab);
                                for (int i = 0; i < nbIndex; i++) {
                                        val2 = valBooleanTab[i];
                                        tabConvertString  += val2.toString().toUtf8();
                                        tabConvertString += ",";
                                }
                                tabConvertString += "}";
                        }
                        listElements.push_back(m_elementsTotal[dataNotifications[i].ClientHandle-1]);
                        listValues.push_back(tabConvertString.c_str());
                } else {
                        listElements.push_back(m_elementsTotal[dataNotifications[i].ClientHandle-1]);
                        listValues.push_back(tempValue.toString().toUtf8());
                }
            }
            else
            {
                printf("  Variable %d failed!\n", i);
            }
        }



        /*for ( i=0; i<dataNotifications.length(); i++ )
        {
            if ( OpcUa_IsGood(dataNotifications[i].Value.StatusCode) )
            {
                UaVariant tempValue = dataNotifications[i].Value.Value;
                listElements.push_back(m_elementsTotal[dataNotifications[i].ClientHandle-1]);
                listValues.push_back(tempValue.toString().toUtf8());
		//printf("sonde jl CallbackPluginManager::dataChange() %d\n",dataNotifications[i].ClientHandle-1);
		//printf("sonde jl CallbackPluginManager::dataChange() %s-->%s \n",m_elementsTotal[dataNotifications[i].ClientHandle-1].c_str(),tempValue.toString().toUtf8());
            }
            else
            {
                printf("  Variable %d failed!\n", i);
            }
        }*/
//printf("sonde jl dataChange() avant size=%d\n",listValues.size());

        dataChange(listElements, listValues);
    };

	/** Send subscription state change. */
    virtual void subscriptionStatusChanged(
        OpcUa_UInt32      /*clientSubscriptionHandle*/,
        const UaStatus&   /*status*/) {
		//printf("-- Event dataChange ----------------------------------- subscriptionStatusChanged \n");

	};

    /** Send new events. */
    virtual void newEvents(
        OpcUa_UInt32          /*clientSubscriptionHandle*/,
        UaEventFieldLists&    /*eventFieldList*/) {

		//printf("-- Event dataChange ----------------------------------- newEvents \n");
	};

    virtual void connectionStatusChanged(
        OpcUa_UInt32              /*clientConnectionId*/,
        UaClient::ServerStatus /*serverStatus*/){
	};

void dataChange(std::vector<std::string> listElements, std::vector<std::string> listValues){
        std::vector<std::string>listElementsTemp ;
        std::vector<std::string> listValueTemp;
        listElementsTemp.clear();
        listValueTemp.clear();

	std::vector<std::string> m_elements2;
	//std::vector<myiterator>  m_elements2;
	int index=0;
	int index2=0;
	std::string test;
/*				
		for (std::vector<std::string>::iterator it4 = listElements.begin(); it4 != listElements.end();it4++) {
printf("sonde jl ici dckPluginManager::dataChange() liste elements dana data change =%s\n",it4->c_str());
		}
printf("\n\n\n\n\n");
*/
		for (std::vector<MOS_CallbackInterface*>::iterator it = m_MOS_callback.begin(); it != m_MOS_callback.end();it++) {
			std::vector<std::string> m_elements2;
			m_elements2=m_elements[index];
			index++;

/*			printf("sonde jl boucle liste des callback =%d \n",index);
			for (myiterator::iterator it3 = m_elements2.begin(); it3 != m_elements2.end();it3++) {
				printf("sonde jl ici dckPluginManager::dataChange() liste elements ipour ce Caalback =%s\n",it3->c_str());
			}
			printf("\n\n\n\n\n");
*/
			for (myiterator::iterator it3 = m_elements2.begin(); it3 != m_elements2.end();it3++) {
				test= it3->c_str();	
//printf("sonde jl boucle for sur l'ensemble des elements que l'on cherche elementEnCour=%s\n",test.c_str());
				index2=0;
				for (std::vector<std::string>::iterator it4 = listElements.begin(); it4 != listElements.end();it4++) {
//printf("sonde jl liste element du datachange element=%s\n",it4->c_str());
					if(test.compare(it4->c_str()) == 0) {
//printf("sonde jl trouve l'element %s\n",it4->c_str());
						listElementsTemp.push_back(it4->c_str());
						listValueTemp.push_back(listValues[index2]);
						break;
					}
					index2++;
				}
	                	if(listElementsTemp.size()>0) {
                                	(*it)->dataChange(listElementsTemp,listValueTemp);
                                	listElementsTemp.clear();
                                	listValueTemp.clear();
                        	}
			}
	}
//printf("\n\nfin de datachange\n\n");
}
/*
void dataChange(std::vector<std::string> listElements, std::vector<std::string> listValues){
	std::vector<std::string>listElementsTemp ;
	std::vector<std::string> listValueTemp;
	listElementsTemp.clear();
	listValueTemp.clear();

	//printf("--------------------- sonde jl debut dataChange\n");
	std::list<myiterator> m_elements2;
	int index=0;
		//printf("--------------------- sonde jl boucle1 avec %d\n",listElements.size());
	for (std::vector<std::string>::iterator it = listElements.begin(); it != listElements.end();it++) {
		for (std::vector<myiterator>::iterator it2 = m_elements.begin(); it2 != m_elements.end();it2++) {
			int index2=0;
			std::string test;
			for (myiterator::iterator it3 = (*it2).begin(); it3 != (*it2).end();it3++) {
				test= it3->c_str();	
				//printf("--------------------- sonde jl boucle3 avec %s==%s ? \n",test.c_str() , it->c_str());
				if(test.compare(it->c_str()) == 0) {
					//printf("On range %s --> %s\n",it->c_str(),listValues[index].c_str());
					listElementsTemp.push_back(it->c_str());
					//listValueTemp.push_back(listValues[index2]);
					//for (myiterator::iterator it4 = listValues.begin(); it4 != listValues.end();it4++) {
					//	printf("sonde jl list ded elementValue = %s\n",it->c_str());
					//}
					listValueTemp.push_back(listValues[index]);
				}
				index2++;
			}
	/		if(listElementsTemp.size()>0) {
				printf("sonde jl index=%d\n",index);
				printf("sonde jl size1=%d\n",listElementsTemp.size());
				printf("sonde jl size2=%d\n",listValueTemp.size());
				m_MOS_callback[0]->dataChange(listElementsTemp,listValueTemp);
				listElementsTemp.clear();
				listValueTemp.clear();
			}	/
		//	index++;
		}
		index++;
	}
	if(listElementsTemp.size()>0) {
                                //printf("sonde jl index=%d\n",index);
                                //printf("sonde jl size1=%d\n",listElementsTemp.size());
                                //printf("sonde jl size2=%d\n",listValueTemp.size());
                                //m_MOS_callback[0]->dataChange(listElementsTemp,listValueTemp);
				for (std::vector<MOS_CallbackInterface*>::iterator it = m_MOS_callback.begin(); it != m_MOS_callback.end();it++) {
                                	//printf("sonde jl MOS_CallbackInterface\n");
                                	(*it)->dataChange(listElementsTemp,listValueTemp);
				}
                                listElementsTemp.clear();
                                listValueTemp.clear();
                        }

	//printf("--------------------- sonde jl fin dataChange\n");
}
*/
};
#endif
