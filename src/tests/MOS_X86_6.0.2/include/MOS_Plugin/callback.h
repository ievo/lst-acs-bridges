#include "uaclientsdk.h"
#include "uasession.h"
#include <vector>
using namespace UaClientSdk;


class Callback2:
    public UaSessionCallback,
    public UaSubscriptionCallback
{
public:
    Callback2(){printf("sonde jl debug \t\t************************** constructor Callback de libclientOPCUA \n");};
    virtual ~Callback2(){};

    /** Send changed status. */
    virtual void connectionStatusChanged(
        OpcUa_UInt32              clientConnectionId,
        UaClient::ServerStatus serverStatus);

    /** Send changed data. */
    virtual void dataChange(
        OpcUa_UInt32               clientSubscriptionHandle,
        const UaDataNotifications& dataNotifications,
        const UaDiagnosticInfos&   /*diagnosticInfos*/) {
        OpcUa_UInt32 i = 0;
        printf("-- Event dataChange ----------------------------------- dataChange \n");
        printf("clientSubscriptionHandle %d \n", clientSubscriptionHandle);
        for ( i=0; i<dataNotifications.length(); i++ )
        {
            if ( OpcUa_IsGood(dataNotifications[i].Value.StatusCode) )
            {
                UaVariant tempValue = dataNotifications[i].Value.Value;
                if (tempValue.type() == OpcUaType_ExtensionObject)
                {
                    printf("  Variable %d with structure value:\n", dataNotifications[i].ClientHandle);
                    UaExtensionObject extensionObject;
                    tempValue.toExtensionObject(extensionObject);
                    //printExtensionObject(extensionObject);
                }
                else
                {
                    printf("  Variable %d value = %s\n", dataNotifications[i].ClientHandle, tempValue.toString().toUtf8());
                }
            }
            else
            {
                printf("  Variable %d failed!\n", i);
            }
        }
        printf("-------------------------------------------------------\n");
    };

    
	/** Send subscription state change. */
    virtual void subscriptionStatusChanged(
        OpcUa_UInt32      clientSubscriptionHandle,
        const UaStatus&   status) {
		printf("-- Event dataChange ----------------------------------- subscriptionStatusChanged \n");

	};

    /** Send new events. */
    virtual void newEvents(
        OpcUa_UInt32          clientSubscriptionHandle,
        UaEventFieldLists&    eventFieldList) {

		printf("-- Event dataChange ----------------------------------- newEvents \n");
	};
};

