#ifndef CALLBACKPLGIN_H
#define CALLBACKPLGIN_H


#include "uaclientsdk.h"
#include "uasession.h"
#include <vector>
#include <string>
using namespace UaClientSdk;

#include "MOS_callbackInterface.h"
//class MOS_CallbackInterface;

class CallbackPlugin:
    public UaSessionCallback,
    public UaSubscriptionCallback
{
public:
    CallbackPlugin(std::vector<std::string> l_elements,std::vector<int> l_namespace,MOS_CallbackInterface* MOS_callback){
	m_elements = l_elements;
	m_namespace = l_namespace;
	m_MOS_callback = MOS_callback;
	};
    virtual ~CallbackPlugin(){};

    /** Send changed status. */
    virtual void connectionStatusChanged(
        OpcUa_UInt32              /*clientConnectionId*/,
        UaClient::ServerStatus /*serverStatus*/){
	};

    /** Send changed data. */
    virtual void dataChange(
        OpcUa_UInt32               /*clientSubscriptionHandle*/,
        const UaDataNotifications& dataNotifications,
        const UaDiagnosticInfos&   /*diagnosticInfos*/) {
        OpcUa_UInt32 i = 0;
	std::vector<std::string> listElements;
	std::vector<std::string> listValues;
        for ( i=0; i<dataNotifications.length(); i++ )
        {
            if ( OpcUa_IsGood(dataNotifications[i].Value.StatusCode) )
            {
                UaVariant tempValue = dataNotifications[i].Value.Value;
		listElements.push_back(m_elements[dataNotifications[i].ClientHandle-1]);
		listValues.push_back(tempValue.toString().toUtf8());
            }
            else
            {
                printf("  Variable %d failed!\n", i);
            }
        }
	    m_MOS_callback->dataChange(listElements, listValues);
    };

    
	/** Send subscription state change. */
    virtual void subscriptionStatusChanged(
        OpcUa_UInt32      /*clientSubscriptionHandle*/,
        const UaStatus&   /*status*/) {
		printf("-- Event dataChange ----------------------------------- subscriptionStatusChanged \n");

	};

    /** Send new events. */
    virtual void newEvents(
        OpcUa_UInt32          /*clientSubscriptionHandle*/,
        UaEventFieldLists&    /*eventFieldList*/) {

		printf("-- Event dataChange ----------------------------------- newEvents \n");
	};
  private :
	std::vector<std::string> m_elements;
	std::vector<int> m_namespace;
	MOS_CallbackInterface* m_MOS_callback;
};

#endif
