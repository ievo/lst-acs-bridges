/**
 *******************************************************************************
 *
 * @file    log.cpp
 * @authors panazol@lapp.in2p3.fr
 * @date
 * @Modif   12/11/2014
 * @version v1.0.2
 * @brief description :
 *     log class to print in the console and store informations in a log file
 *
 *
 * @details
 *  <full description...>
 *
 *------------------------------------------------------------------------------
 *
 * @copyright Copyright © 2014, LAPP/CNRS
 *
 *
 *******************************************************************************/

#ifndef __LOG_H__
#define __LOG_H__

#include "string"
#include "stdio.h"
//#include "cta_ua_nodemanager.h"
//#include "cta_ua_nodemanagerLevelUser.h"
//#include <libxml/parser.h>
//#include <libxml/xpath.h>

#include "libxml/parser.h"
#include "libxml/xpath.h"

#define MAX_PATH 2048

#define ELOG_TRACE 1
#define ELOG_DEBUG 2

#define ELOG_INFO 4
#define ELOG_WARNING 5
#define ELOG_ERROR 6

class SystemThread;
//class xmlNodePtr;

class Log {
public:
	static Log *singleton;  // pointeur vers le singleton
	Log(std::string logFileName);
	virtual ~Log();

	// Methodes
	static void kill();
	static Log *getInstance(std::string logFileName);
	static Log *getInstance();

	//methodes
	void warning(std::string text);
	void error(std::string text);
	void info(std::string text,std::string color);
	void debug(std::string text);
	void trace(std::string text);
	void updateConformity();
	void setDpRef(std::string ref);
	void updateDpLog();
	void configureElog(std::string elog_port,std::string elog_address,std::string elog_account,std::string elog_pwd,std::string elog_name,std::string elog_type);
	void readConfigfile(std::string elogConfigFile);
	xmlNodePtr readFile(std::string filename,int flagAbsolue_Relatif);
	void parse(xmlNodePtr m_racine);
	bool fileExists(const char* FileName);

private:
	std::string m_logFileName;
	std::string m_log_DP;
	std::string m_temp_log;
	std::string m_elog_port;
	std::string m_elog_address;
	std::string m_elog_account;
	std::string m_elog_pwd;
	std::string m_elog_type;
	std::string m_elog_name;
	std::string m_elog_category;
	int m_elog_level;
	bool m_elog_enable;
	int m_conformity;
	SystemThread *m_systemThread;
	FILE *m_fd;
	const std::string currentDateTime();
};

#endif

