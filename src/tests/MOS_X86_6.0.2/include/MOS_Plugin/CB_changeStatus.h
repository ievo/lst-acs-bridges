#ifndef CB_changeStatus_H
#define CB_changeStatus_H

#include "pluginsBase.h"

class CB_changeStatus {
public:
 CB_changeStatus() {};
 virtual void message(int status)=0;
 virtual void setRefPlugin(PluginsBase *plugin)=0;
};
#endif
