#!/bin/bash


usage() {
        echo "(script) ******************************"
        echo "(script) *   Error parameter missing    "
        echo "(script) *   Usage :                    "
        echo "(script) *   ./createInstanceAMC.sh <InstanceNumber>"
        echo "(script) *   <InstanceNumber>    -->  instance number allow to build the name of the xml file for this instance"
        echo "(script) *******************************"
}

echo "create an instance of the Model : start"
if [ "$1" == "" ]
then
        usage
        exit
fi

m_name=AMC_$1.xml
m_modelName=AMCModel.xml
m_name_server="opc.tcp:\/\/192.168.75.141:4851"

NOW=$(date +"%d-%m-%Y %k:%M")
sed -re "s/@Name@/$m_name/"  $m_modelName > temp.xml
sed -re "s/@Name_Server@/$m_name_server/"  temp.xml  > temp2.xml
sed -re "s/@CCD_1@/$1.AMC_CCD/"  temp2.xml  > temp.xml
sed -re "s/@RootNS@/$1/"  temp.xml  > temp2.xml
sed -re "s/@DateCreation@/$NOW/"  temp2.xml > $m_name
rm -rf temp.xml
rm -rf temp2.xml

echo "create an instance ($m_name) with the Model($m_modelName) : finished"
