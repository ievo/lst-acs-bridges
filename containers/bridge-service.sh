#!/bin/sh
# Based on example from https://docs.docker.com/engine/reference/builder/

# Setting up the environment
# . /alma/ACS-JUN2017/ACSSW/config/.acs/.bash_profile.acs
. /home/drivemgr/.bashrc

# start service in background here
echo "Starting ACS..."
acsStart

acsStartContainer --java LSTDataPublisherContainer &

acsStartContainer --java DriveMasterContainer &

# USE the trap if you need to also do manual cleanup after the service is stopped,
#     or need to start multiple services in the one container
trap acsStop HUP INT QUIT TERM

echo "[hit enter key to exit] or run 'docker stop <container>'"
read

# stop service and clean up here
echo "Stopping ACS"
acsStop

echo "exited $0"
