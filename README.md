# lst-acs-bridges

This repository contains the ACS interface to the LST1 control components, published by the LST1 OPC-UA servers. 

**`src` sub-directory:**

provides the ACS "introot" folder, that should be pointed to via the ACS_INTROOT variable. To get it working, be sure to put the correct server IP and port numbers:

- the "drive" OPC-UA server is set in `INTROOT/config/CDB/alma/DriveControl/DriveControl.xml`.

**`containers` sub-directory:**

configuration file to build the docker container with these bridges. A sample command:

```bash
docker build -f containers/Dockerfile . --tag lst/acs-bridges:latest
```

Once the container is run, the `bridge-service.sh` script from its home folder needs to be invoked to start the ACS environment and the bridges.

